```puml
@startuml

class CommunicationContext {
    openConnection
    openReader
    sendCommand
    --
    isConnected
    isRunning
    EndOfClass
}

class RootReducer {
    pos
    speed
    torque
    current
    voltage
}


CommunicationContext --> RootReducer


@enduml
```