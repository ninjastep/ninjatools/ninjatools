| Command | Response | Alt-response | Min | Max | Comment |
| ------- | -------- | ------------ | --- | --- | ------- |
| get_res | &#60;r:XXX&#62; |  |  |  | Motorns serie resistans |
| set_res XXX | &#60;r-XXX&#62; |  | 2 | 1000 | Motorns serie resistans |
| get_ind | &#60;L:XXX&#62; |  |  |  | Motorns fasinduktans |
| set_ind XXX | &#60;L-XXX&#62; |  | 0 | 1 | Motorns fasinduktans |
| get_km | &#60;km:XXX&#62; |  |  |  | Motorkonstan (km) |
| set_km XXX | &#60;km-XXX&#62; |  | 0 | 1 | Motorkonstan (km) |
| get_n | &#60;n:XXX&#62; |  |  |  | Polantal |
| set_n XXX | &#60;n-XXX&#62; |  | 0 | 100 | Polantal |
| get_imax | &#60;i:XXX&#62; |  |  |  | Max ström |
| set_imax XXX | &#60;i-XXX&#62; |  | 0 | 100 | Max ström |
| get_spd_p | &#60;spd_p:XXX&#62; |  |  |  | P verkan speed PID |
| set_spd_p XXX | &#60;spd_p-XXX&#62; |  | 0 | 1 | P verkan speed PID |
| get_spd_i | &#60;spd_i:XXX&#62; |  |  |  | I verkan speed PID |
| set_spd_i XXX | &#60;spd_i-XXX&#62; |  | 0 | 1 | I verkan speed PID |
| get_spd_d | &#60;spd_d:XXX&#62; |  |  |  | D verkan speed PID |
| set_spd_d XXX | &#60;spd_d-XXX&#62; |  | 0 | 1 | D verkan speed PID |
| get_pos_p | &#60;pos_p:XXX&#62; |  |  |  | P verkan position PID |
| set_pos_p XXX | &#60;pos_p-XXX&#62; |  | 0 | 50 | P verkan position PID |
| get_pos_i | &#60;pos_i:XXX&#62; |  |  |  | I verkan position PID |
| set_pod_i XXX | &#60;pos_i-XXX&#62; |  | 0 | 50 | I verkan position PID |
| get_pos_d | &#60;pos_d:XXX&#62; |  |  |  | D verkan position PID |
| set_pos_d XXX | &#60;pos_d-XXX&#62; |  | 0 | 50 | D verkan position PID |
| get_trq_p | &#60;trq_p:XXX&#62; |  |  |  | P verkan torque PID |
| set_trq_p XXX | &#60;trq_p-XXX&#62; |  | 0 | 1 | P verkan torque PID |
| get_trq_i | &#60;trq_i:XXX&#62; |  |  |  | I verkan torque PID |
| set_trq_i XXX | &#60;trq_i-XXX&#62; |  | 0 | 1 | I verkan torque PID |
| get_trq_d | &#60;trq_d:XXX&#62; |  |  |  | D verkan torque PID |
| set_trq_d XXX | &#60;trq_d-XXX&#62; |  | 0 | 1 | D verkan torque PID |
| get_spd_max | &#60;sm:XXX&#62; |  |  |  | Max speed (enligt flash config) alltså sparar värdet. |
| set_spd_max XXX | &#60;sm-XXX&#62; |  | 0 | 100 | Max speed (enligt flash config) alltså sparar värdet. |
| get_trq_max | &#60;mt:XXX&#62; |  |  |  | Max torque (enligt flash config) alltså sparar värdet. |
| set_trq_max XXX | &#60;tm-XXX&#62; |  | 0 | 1 | Max torque (enligt flash config) alltså sparar värdet. |
| get_vdq | &#60;vdq:XXX&#62; |  |  |  | Voltage Divider Quotient. Används för att kalibrera input spänningsmätning |
| set_vdq XXX | &#60;vdq-XXX&#62; |  | 0 | 0.1 | Voltage Divider Quotient. Används för att kalibrera input spänningsmätning |
| get_spd | &#60;s:XXX&#62; |  |  |  | Frågor motor_controller vad hastighet är och skickar tillbaka |
| set_spd XXX | &#60;s-XXX&#62; |  | -100 | 100 | Ropar på motor_controller_set_speed om vi är i MOTOR_CONTROLLER_PID_SPEED, ropar på motor_controller_set_max_speed om vi är i MOTOR_CONTROLLER_PID_POSITION och skriver <error> om vi är i MOTOR_CONTROLLER_TORQUE. OBS, vi har ingen metod att fråga motorcontroller vilket mode vi är i just nu. Så vänta med denna! Eller snarar gör kommandon men implementera inget i callback |
| get_trq | &#60;t:XXX&#62; |  |  |  | Frågar motor_controller vad torque är och skickar tillbaka |
| set_trq XXX | &#60;t-XXX&#62; |  | -5 | 5 |  |
| get_pos | &#60;p:XXX&#62; |  |  |  | Returnerar nuvarnade positionen i rad |
| set_pos XXX | &#60;p-XXX&#62; |  | -1000 | 1000 |  |
| get_vin | &#60;v:XXX&#62; |  |  |  | Returnerar input spänning (fråga hbryggan) |
| start_cal | &#60;cal:OK/ERR&#62; |  | 0 | 30 | Startar en calibrering. Obs start_cal måste skickas med ett argument. T.ex. start_cal 5 vilket betyder att running spänningen är 5 V, holding spänningen är alltid 80% av running spänningen. Om man skickar fler argument, tex. start_cal 4 5 så är första argumentet spänningen och andra argumentet antal varv.  |
| is_cal | &#60;cal:YES/NO&#62; |  |  |  | Kollar om det finns en kalibrering genom att ropa på TLE5012B_calibration_valid() |
| check_cal | &#60;ccal:XXX&#62; |  |  |  | Returnerar RMS värdet är det tänkt. Samma arguement som start_cal men kör istället en test kalibrering. Just nu ger returvärdet TLE5012B_test_calibration() endast Okej eller Error så för tillfället så får du ge ett dummy värde om det lyckas och skriva -1 om det misslyckas.  |
| set_ctrl_mode XXX | &#60;ctrl_mode-XXX&#62; | &#60;ctrl_mode-ERR&#62; | 0 | 2 | 0 = "torque", 1 = "position", 2 = "speed" |
| get_ctrl_mode | &#60;ctrl_mode:XXX&#62; |  |  |  | 0 = "torque", 1 = "position", 2 = "speed" |
| get_hb | &#60;hb:XXX XXX XXX XXX XXX&#62; |  |  |  | Returnerar flera värden: Pos, speed, torque, current alpha, current beta, volt |
| start_hb_stream XXX | &#60;hb:OK/ERR_INVAL_ARG/ERR&#62; | &#60;hb-XXX&#62; | 0 | 1000 | Startar en hb stream. Behöver som input streaming frekvensen. |
| stop_hb_stream | &#60;hb:STOP&#62; |  |  |  | Avslutar streamen. |
| move | &#60;p-XXX&#62; |  |  |  | Kör motorn XXX relativt till nuvarande position, svarar med absolut position |
| stop | &#60;p-XXX&#62; |  |  |  | Används för att stanna motorn utan att stoppa motorcontrollern med stop_mot |
| init_mot | &#60;motor:OK/ERR&#62; |  |  |  |  |
| start_mot | &#60;motor:START&#62; |  |  |  |  |
| stop_mot | &#60;motor-STOP&#62; |  |  |  |  |
| start_mqtt | &#60;mqtt-OK/ERR&#62; |  |  |  | Startar ninja clienten |
| init_mqtt | &#60;mqtt-OK/ERR/ERR_WIFI/ERR_NVS&#62; |  |  |  | Initierar ninja clienten. Läser ifrån flash och skickar en ninja_client_config till ninja_client_init |
| set_mqtt_name XXX | &#60;mqtt-OK&#62; |  |  |  | Sätter ninja clientens namn |
| set_mqtt_uname XXX | &#60;mqtt-OK&#62; |  |  |  | Sätter ninja clientens broker användarnamn |
| set_mqtt_pw XXX | &#60;mqtt-OK&#62; |  |  |  | Sätter ninja clientens broker lösenord. |
| set_mqtt_url XXX | &#60;mqtt-OK&#62; |  |  |  | Sätter ninja clientens broker url. |
| set_wifi_ssid XXX | &#60;wifi-OK/ERR&#62; |  |  |  | Sätter ninja clientens wifi ssid |
| set_wifi_pw XXX | &#60;wifi-OK/ERR&#62; |  |  |  | Sätter ninja clientens wifi lösenord |
| get_mqtt_name | &#60;mqtt:XXX/ERR&#62; |  |  |  | Returns ninja client's name |
| get_mqtt_uname | &#60;mqtt:XXX/ERR&#62; |  |  |  | Returns the mqtt brokers username |
| get_mqtt_pw | &#60;mqtt:YES/NO/ERR&#62; |  |  |  | Returns if there exists a mqtt broker password |
| get_wifi_ssid | &#60;wifi:XXX/ERR&#62; |  |  |  | Returns the wifi ssid |
| get_wifi_pw | &#60;wifi:YES/NO/ERR&#62; |  |  |  | Returns if there exists a wifi password |
| set_pos_cb XXX XXX XXX XXX | &#60;cbp-XXX&#62; | &#60;cbp:XXX&#62; | -inf | inf | Skickar ett meddelande när man är på positionen som man satt i argumentet. Det första svaret är bara en konfirmation att en cb är satt tillsammans med ID. Som argument skickas val once less abs. OBS! once,less och abs ska skickas in som "true" "false" |
| set_spd_cb XXX XXX XXX XXX | &#60;cbs-XXX&#62; | &#60;cbs:XXX&#62; | 0 | 75 | Skickar ett meddelande när man har uppnått hastigheten dvs speed >= cb_speed. Dock så finns inte "CUSTOM SPEED" som cb än. Som argument skickas val once less abs |
| set_trq_cb XXX XXX XXX XXX | &#60;cbt-XXX&#62; | &#60;cbt:XXX&#62; | 0 | 1 | Skickar ett meddelande när man har en torque som är större än argumentet. Obs, vi tar ej hänsyn till tecken här. Som argument skickas val once less abs. OBS! once,less och abs ska skickas in som "true" "false" |
| rmv_cb XXX | &#60;cbr-OK&#62; |  |  |  | Tar bort en callback. Argumentet är ID |
| get_gpio_dig |  |  |  |  |  |
| set_gpio_dig |  |  |  |  |  |
| set_gpio_dir |  |  |  |  |  |
| set_gpio_pull |  |  |  |  |  |
| set_gpio_cb | cb |  |  |  |  |
| get_es | &#60;es:XXX XXX XXX  XXX XXX&#62; |  |  |  | Returnerar flera värden: res ind km n cal |
| start_pos_stream XXX | &#60;p:OK/ERR_INVAL_ARG/ERR&#62; | &#60;p:XXX&#62; | 0 | 1000 | Startar en pos stream. Behöver som input streaming frekvensen. |
| start_spd_stream XXX | &#60;s:OK/ERR_INVAL_ARG/ERR&#62; | &#60;s:XXX&#62; | 0 | 1000 | Startar en spd stream. Behöver som input streaming frekvensen. |
| start_trq_stream XXX | &#60;t:OK/ERR_INVAL_ARG/ERR&#62; | &#60;t:XXX&#62; | 0 | 1000 | Startar en trq stream. Behöver som input streaming frekvensen. |
| start_iab_stream XXX | &#60;iab:OK/ERR_INVAL_ARG/ERR&#62; | &#60;iab:XXX XXX&#62; | 0 | 1000 | Startar en i stream. Output är alpha current och beta current. Behöver som input streaming frekvensen. |
| start_vin_stream XXX | &#60;v:OK/ERR_INVAL_ARG/ERR&#62; | &#60;v:XXX&#62; | 0 | 1000 | Startar en volt stream. Behöver som input streaming frekvensen. |
| stop_pos_stream | &#60;p:STOP&#62; |  |  |  | Avslutar streamen. |
| stop_spd_stream | &#60;s:STOP&#62; |  |  |  | Avslutar streamen. |
| stop_trq_stream | &#60;t:STOP&#62; |  |  |  | Avslutar streamen. |
| stop_iab_stream | &#60;i:STOP&#62; |  |  |  | Avslutar streamen. |
| stop_vin_stream | &#60;v:STOP&#62; |  |  |  | Avslutar streamen. |
| start_sd | &#60;sd-OK/ERR&#62; |  |  |  | Startar step dir |
| init_sd | &#60;sd-OK/ERR/ERR_WIFI/ERR_NVS&#62; |  |  |  | Initierar step dir |
| stop_sd | &#60;sd-OK/ERR&#62; |  |  |  | Stoppar step dir |
| set_sd_fault XXX XXX XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter fault pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown, micro_steps_to_enable_fault, micro_step_divisor_power |
| set_sd_step XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter step pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_dir XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter dir pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_ms1 XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter ms1 pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_ms2 XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter ms2 pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_ms3 XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter ms3 pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_enable XXX XXX XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter enable pinnen för step dir. Behöver som input gpio_nr, active_low, pulldown. |
| set_sd_dir_active XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter vilket håll motorn ska snurra när dir pin är active. Behöver som input cw eller ccw. |
| set_sd_relative_step XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter relativ steg längd. Behöver som input 0<x<=1 |
| set_sd_active_time XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter hur länge step behöver vara aktiv i us. Behöver som input en siffra 0<x |
| set_sd_step_length XXX | &#60;sd-OK/ERR/ERR_INVAL_ARG&#62; |  |  |  | Sätter hur långt ett step är i rad. Behöver som input en siffra 0<x |
| get_sd_fault | &#60;sd-XXX XXX XXX XXX XXX/ERR&#62; |  |  |  | Returns fault pin settings. |
| get_sd_step | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns step pin settings. |
| get_sd_dir | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns dir pin settings. |
| get_sd_ms1 | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns ms1 pin settings. |
| get_sd_ms2 | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns ms2 pin settings. |
| get_sd_ms3 | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns ms3 pin settings. |
| get_sd_enable | &#60;sd-XXX XXX XXX/ERR&#62; |  |  |  | Returns enable pin settings. |
| get_sd_dir_active  | &#60;sd-CW/CCW/ERR&#62; |  |  |  | Returns the way the motor turns when dir pin is active. Returns cw or ccw |
| get_sd_relative_step  | &#60;sd-XXX/ERR&#62; |  |  |  | Returns the relative step length |
| get_sd_active_time  | &#60;sd-XXX/ERR&#62; |  |  |  | Returns how long the step pin has to active |
| get_sd_step_length  | &#60;sd-XXX/ERR&#62; |  |  |  | Returns how long a base step is in rad. |

