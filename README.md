![LOGO](/readmeImages/NS-logo.png)

# Ninja Tools

## User manual

NS = Ninja Step (the engine)
NT = Ninja Tools (the GUI/Platform/ReactJS App)

### Requirements

- Chrome v.89 (or later) or Microsoft Edge installed

### Description

Ninja Tools(NT) is a responsive WebApp built in ReactJS for communication with the Ninja Step engine(NS). It uses WebSerial technology to allow the website to directly communicate with the engine.

### Getting Started

- Clone GitLab repo
- Install Node.js with npm if not installed: https://www.npmjs.com/get-npm
- Install dependencies for the project (run the command in the cloned folder)
  - `npm install`
- Start the project on your local server (run the command in the cloned folder)
  - `npm start`

## Shortcuts:

### Under the hood:

- [Structure & Context](#structure-context)
  - [useContext-hook](#usecontext-hook)
  - [useReducer-hook](#usereducer-hook)
  - [Architecture](#architecture)
  - [Connecting to NinjaStep](#connecting-to-ninjastep)
  - [Communication](#communication)
  - [Reading input](#reading-input)
  - [Writing output](#writing-output)
  - [Useful functions and variables](#useful-functions-and-variables)

### User guide

- [User guide & Pages of NinjaStep](#user-guide-pages-of-ninjastep)
  - [Home](#home)
  - [Engine Setup](#engine-setup)
  - [Controller Setup](#controller-setup)
  - [Interface Setup](#interface-setup)
  - [Live Graph](#live-graph)
  - [Terminal](#terminal)

## Under the hood:

### Structure & Context

The webapp is built with ReactJS using functional components (with hooks). Variables and functions are shared to all pages (and their subcomponents) through context-components, quote from the official ReactJS documentation:

> In a typical React application, data is passed top-down (parent to child) via props, but such usage can be cumbersome for certain types of props (e.g. locale preference, UI theme) that are required by many components within an application. Context provides a way to share values like these between components without having to explicitly pass a prop through every level of the tree.

#### useContext-hook

Since a lot of information needs to be accessible from many different components we use context-components that wrap the entire app. This allows for variables to be accesible to read and write from the entire application. Originally this was done through one master component called CommunicationContext, but this has been broken down into several more specified context-components. These context-components now specialize on different topics such as SerialPort, Motor, Communication etc.

#### useReducer-hook

However, some variables needs to be updated at a very rapid pase for data streams (such as position, speed, voltage etc). To improve performance we use Redux and rootReducers. The rootReducer only stores information about position,speed, torque, current, voltage and terminal and is used when we need to rapidly update information.

Here is a simplified example showing how pages and subcomponents get access to variables and functions from Context providers and the rootReducer. Note that this is far from all variables, functions and pages but merely an example to illustrate internal communication works.

![COMMUNICATION](/readmeImages/contextproviders.png)

#### Architecture

The architecture of the project is designed using Barrels(see tutorial [here](https://hackernoon.com/react-project-architecture-using-barrels-d086146eb0f6))

Short walkthrough of architecture:

- src folder holds the project
- app.tsx is the root-component
  - Pages are imported through a folder (this is the core idea of Barrels), same with the pages respective components. Each page is represented by a folder that contains the actual page.tsx, a folder with page-components and a index.ts (which dictates what is exported from the folder).

![BARRELS](/readmeImages/barrels.png)

General components that are accesses all throughout the site (like TopBar, SideBar and ContentPanel) are located in `src/components`

#### Connecting to NinjaStep

When the user connects with the NS-engine, the connection is handled in `ConnectionContext.tsx`. A reader and writer stream are created with the `openSerialPort()` function. If successfull, the function `readPort()` gets called. The `readPort()` is a continous process, its actively listening to the port until `closeConnection()` is called.

#### Communication

Almost all communication is handled in the `CommunicationContext`-component, functions like sendCommand and readPort. readPort is a recursive-loop which continues running while there is a connection to a serialport, which reads values from the `SerialPort`-class with the `read` function then passes those values to `interpretValue` that translates different types of text to different commands and actions.

#### Reading input

The `interpretValue` functions is very important. It scans the input and calls different functions depending on input. If the input for example contains `<p:` it calls on `readPos`. So depending on the input from the stream a series of different functions that parses info are called upon. Thus `interpretValue` is vital for NT.

`interpretValue` passes the value to `handleValue` in the `ValueInterpreter`-class and uses regex to check if it's a valid value before calling a certain function with the help of a Javascript Object instead of if-statements.
![interpretvalue](/readmeImages/interpretvalue.png)
![interpretvalue2](/readmeImages/interpretvalue2.png)

For rapidly updating variables we update a state in `rootReducer` which uses Redux technology. Variables like position, heartbeat(which includes position, speed, torque etc) are often updated frequently and thus it was more efficient to use a state and redux than useContext-hook that CommunicationContext uses.
So in the example above, `readPos` parses the input and calls upon the `rootReducer` to update the state variable containg position.

![readpos](/readmeImages/readpos.png)

![ROOTREDUCER](/readmeImages/rootreducer1.png)

#### Writing output

Communication from NT to NS is mainly sent through `sendCommand` and a few variations of this. When we send many commands rapidly (for example when asking for several settings in the Engine or Controller pages) we use `chainCommands` to make sure every command gets sent. All commands are ultimately sent to NS via the `writeToPort` function. This function opens and closes a writer-stream with every message, it is not an asyncronous function.

### Useful functions and variables

| Function / Variable                    | Description                                                                    |
| -------------------------------------- | ------------------------------------------------------------------------------ |
| CommunicationContext                   |
| `sendCommand(string)`                  | Send command to NinjaStep ([List of commands](/CommandList.md))                |
| `chainCommands(array)`                 | Send multiple commands to NinjaStep                                            |
| `safeSendCommand(string, backOffTime)` | Checks if NinjaStep is ready to receive data before sending command            |
| `radiansToDegrees(radians)`            | Converts radians to degrees (useful since get_pos returns position in radians) |
| `degreesToRadians(degrees)`            | Converts degrees to radians                                                    |
| ConnectionContext                      |
| `connected`                            | Shows wheter or not SerialPort is connected                                    |
| `controller`                           | Returns which controller-mode NinjaStep has                                    |
| `createConnection()`                   | Starts the connection process                                                  |
| `closeConnection()`                    | Begin closing connection to SerialPort                                         |
| `softReset()`                          | Make NinjaStep "soft reset"                                                    |
| MotorContext                           |
| `running`                              | Gets wheter or not the motor is running (start_mot/stop_mot commands)          |
| `setRunning`                           | Sets wheter or not motor is running                                            |
| SerialPort                             |
| `read()`                               | Read bytearray from the serialports byte buffer                                |
| `write(command)`                       | Writes bytearray to the serialport                                             |
| `hardReset()`                          | Begins a "hard reset" on the serialport                                        |

## User guide & Pages of NinjaStep

If the user is not using Chrome (v89 or later) or Microsoft Edge, a prompt is displayed asking the user to download chrome or microsoft edge.

![MODAL](/readmeImages/modal.png)

With Chrome installed and experimental-web-platform-features enabled the home-screen is presented.

![HOME](/readmeImages/home.png)

1. The Light switch and the 4 color lets you switch to dark mode and customize colors.
2. Enable demo version to see the interface without connecting a NinjaStep engine.
3. Select baudrate and click the connect button to launch a prompt and select device. **Welcome to Ninja Tools!**
4. This button lets you update the firmware on your Ninja Tools engine, however you already need to have the firmware files on your computer. If Ninjastep CI is updated, this could be automated. So that Ninja Tools automatically gets the latest firmware from the Gitlab pages.

### Home

![HOME2](/readmeImages/home2.png)

1. Start and stop the NS engine. The dropdown to the left allows the user to change controller mode on the engine. Cascade Position (formerly position), Position(formerly torque) and Speed.
2. Position is displayed on the slider, it's not interactable and only displays information. Use the position wheel and joystick to directly control the position of NS. The joystick increases or decreases position. When sticky mode is activated the joystick is stuck in positive or negative movement. The position wheel and joystick are not available when the controller is in Speed mode. 
3. Information about the NS engine is displayed here
4. Speed and torque are displayed in the Performance section.
5. Send commands to NS.

### Motor Setup

![MOTOR](/readmeImages/motor.png)

1. Displays motor info and allows the user to customize configuration. The current info is automatically collected from the engine.
2. Calibrate the angle. Running voltage is necessary while number of rotations is not. NT automatically suggests a running voltage and number of revolutions.

### Controller Setup

![CONTROLLER1](/readmeImages/controller1.png)

1. Choose which controller to set up and to do a step response with, you can see and edit current PID-variables here as well
2. Displays the current PID-controller.

![CONTROLLER2](/readmeImages/controller2.png) 
3. This panel is for the actual Step Response, customize the length of the step, how long you want to track data and frequency (how many updates per second, currently 50 is the max value). These input values change acordingly to the controller the user has selected. 
4. Here the data from the step response will show up. First the data is collected and afterwards the data will show in the chart. The chart is zoomable and you can export the data to CSV.

### Interface Setup

![INTERFACE](/readmeImages/interface.png)
Customize the different pins, this page currently needs improvement. We gather data in a JSON-object but it doesnt communicate to the Ninja Step engine yet. 

### Live Graph

![LIVE](/readmeImages/live.png)

1. Select which variables to track and untrack, also control whether you track or not.
2. Simple input to change the Ninja Steps position
3. Depending on which variables you've chosen to track the data will display here.

### Terminal

![TERMINAL](/readmeImages/terminal.png)
This is an integrated CLI that is the original line of communication with the engine. The integrations is currently a bit primitive and could use improvement, like auto-complete with tab etc.
