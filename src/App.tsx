//Author : Teo

import React, { useContext, useEffect } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import { Layout } from "antd";
import {
  Home,
  ControllerSetup,
  MotorSetup,
  InterfaceSetup,
  LiveGraph,
  Terminal,
} from "./pages"
import "antd/dist/antd.css";
import SideBar from "./components/SideBar";
import TopBar from "./components/TopBar";
import {
  CommunicationContextProvider,
  CompatabilityContextProvider,
  MotorContextProvider,
  SerialPortContextProvider,
  ConnectionContextProvider
} from './context/index'

const { Content } = Layout;


//  This shows the structure of the website, CommunicationContextProvider wraps all children and delivers variables from the Ninja Step motor
//  After the communication context the BrowserRouter wraps everything with routing for the website  f
const App = () => {
  return (
    <SerialPortContextProvider>
      <MotorContextProvider>
        <CommunicationContextProvider>
          <ConnectionContextProvider>
            <CompatabilityContextProvider>
              <BrowserRouter basename={process.env.PUBLIC_URL}>
                <Layout>
                  <Layout>
                    <SideBar />
                    <Layout>
                      <Content>
                        <TopBar />
                        <div className="content-wrapper">
                          <Route
                            path="/"
                            exact
                            component={(routing: any) => <Home {...routing} />}
                          />
                          <Route
                            path="/motor-setup"
                            exact
                            component={(routing: any) => <MotorSetup {...routing} />}
                          />
                          <Route
                            path="/controller-setup"
                            exact
                            component={(routing: any) => (
                              <ControllerSetup {...routing} />
                            )}
                          />
                          <Route
                            path="/interface-setup"
                            exact
                            component={(routing: any) => (
                              <InterfaceSetup {...routing} />
                            )}
                          />
                          <Route
                            path="/live-graph"
                            exact
                            component={(routing: any) => <LiveGraph {...routing} />}
                          />
                          <Route
                            path="/terminal"
                            exact
                            component={(routing: any) => <Terminal {...routing} />}
                          />
                        </div>
                      </Content>
                    </Layout>
                  </Layout>
                </Layout>
              </BrowserRouter>
            </CompatabilityContextProvider>
          </ConnectionContextProvider>
        </CommunicationContextProvider>
      </MotorContextProvider>
    </SerialPortContextProvider>
  );
};

export default App;
