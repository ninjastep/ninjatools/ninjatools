import { useContext } from 'react'
import { CommunicationContext } from './CommunicationContext'
import { MotorContext, ConnectionContext } from './index'


class ValueInterpreter {
  // Using regex to match what element to pick in readValue object
  static regex = /NINJA|motor:START|motor-STOP|<ctrl_mode-|<(p|s|hb|f|r|L|km|n|i|sm|tm|cal|pos_p|pos_i|pos_d|spd_p|spd_i|spd_d|trq_p|trq_i|trq_d|ctrl_mode):/g
  static getControllerMode = {
    0: "Torque",
    1: "Position",
    2: "Speed",
  }
  static readValue: object
  static states

  static init = (states) => {
    const {
      setResistance,
      setRunning,
      setInductance,
      setMotorConstant,
      setPoleNumber,
      setMaxCurrent,
      setMaxSpeed,
      setMaxTorque,
      setCalibrated,
      setPosKP,
      setPosKI,
      setPosKD,
      setSpdKP,
      setSpdKI,
      setSpdKD,
      setTrqKP,
      setTrqKI,
      setTrqKD,
    } = useContext(MotorContext)
    
    const {
      setIsLoading,
      dispatch,
      setReadyToSend
    } = useContext(CommunicationContext)

    const {
      setConnected,
      setKnockResp,
      setController,
      warningModal,
      handleCalibrationStage,
      handleReset,
    } = states
    
    ValueInterpreter.states = states;
    ValueInterpreter.states.warningModal = warningModal
    ValueInterpreter.states.handleReset = handleReset
    ValueInterpreter.states.handleCalibrationStage = handleCalibrationStage
    ValueInterpreter.states.setReadyToSend = setReadyToSend
    ValueInterpreter.states.dispatch = dispatch

    // Replaces normal if-statements
    ValueInterpreter.readValue = {
      'NINJA': () => { setConnected(true); setKnockResp(true); setIsLoading(false); },
      '<p:': (val) => { ValueInterpreter.readPos(val) },
      '<s:': (val) => { ValueInterpreter.readSpd(val) },
      '<hb:': (val) => { ValueInterpreter.readHB(val) },
      '<f:': (val) => { ValueInterpreter.readFloat(val, states.setFloat) },
      '<r:': (val) => { setResistance(+ValueInterpreter.extractValue(val, "r:")) },
      'motor:START': () => { setRunning(true) },
      'motor-STOP': () => { setRunning(false) },
      '<L:': (val) => { setInductance(+ValueInterpreter.extractValue(val, "L:")) },
      '<km:': (val) => { setMotorConstant(ValueInterpreter.extractValue(val, "km:")) },
      '<n:': (val) => { setPoleNumber(+ValueInterpreter.extractValue(val, "n:")) },
      '<i:': (val) => { setMaxCurrent(+ValueInterpreter.extractValue(val, "i:")) },
      '<sm:': (val) => { setMaxSpeed(+ValueInterpreter.extractValue(val, "sm:")) },
      '<tm:': (val) => { setMaxTorque(+ValueInterpreter.extractValue(val, "tm:")) },
      '<cal:': (val) => { ValueInterpreter.readIsCal(val, setCalibrated) },
      '<pos_p:': (val) => { setPosKP(ValueInterpreter.extractValue(val, "pos_p:")) },
      '<pos_i:': (val) => { setPosKI(ValueInterpreter.extractValue(val, "pos_i:")) },
      '<pos_d:': (val) => { setPosKD(ValueInterpreter.extractValue(val, "pos_d:")) },
      '<spd_p:': (val) => { setSpdKP(ValueInterpreter.extractValue(val, "spd_p:")) },
      '<spd_i:': (val) => { setSpdKI(ValueInterpreter.extractValue(val, "spd_i:")) },
      '<spd_d:': (val) => { setSpdKD(ValueInterpreter.extractValue(val, "spd_d:")) },
      '<trq_p:': (val) => { setTrqKP(ValueInterpreter.extractValue(val, "trq_p:")) },
      '<trq_i:': (val) => { setTrqKI(ValueInterpreter.extractValue(val, "trq_i:")) },
      '<trq_d:': (val) => { setTrqKD(ValueInterpreter.extractValue(val, "trq_d:")) },
      '<ctrl_mode:': (val) => { setController(ValueInterpreter.getControllerMode[+ValueInterpreter.extractValue(val, "ctrl_mode:")]) },
      '<ctrl_mode-': (val) => {
        if (val.includes("ERR")) {
          warningModal("Cal-Error", "Please Try again");
        }
      },
    }
  }

  // Updates different states depending on the value received
  static handleValue = (value) => {
    ValueInterpreter.states.handleReset(value);
    ValueInterpreter.states.handleCalibrationStage(value);

    if (value.includes('<') && value.includes('>')) {
      ValueInterpreter.states.setReadyToSend(true);
      let matchedValue = value.match(ValueInterpreter.regex);

      if (matchedValue != null)
        ValueInterpreter.readValue[matchedValue[0]](value)
    }
  }

  static extractValue = (value, separator) => {
    let splitValue: any = value.split(separator)[1];
    splitValue = splitValue.split(">")[0];
    return splitValue;
  }

  static readPos = (pos) => {
    if (pos.includes('OK')) {
      console.log('starting stream')
    } else if (pos.includes('ERR_INVAL_ARG')) {
      ValueInterpreter.states.warningModal("Stream Failed", "Position Stream Invalid Argument")
      console.log('pos stream invalid argument')
    } else if (pos.includes('ERR')) {
      ValueInterpreter.states.warningModal("Stream Error", "Position Stream encountered an error")
      console.log('pos stream error')
    } else if (pos.includes('STOP')) {
      console.log('pos stream stopped')
    } else {
      ValueInterpreter.states.dispatch({ type: 'UPDATE_POSITION', value: ValueInterpreter.extractValue(pos, "p:") })
    }
  }

  static readSpd = (spd) => {
    if (spd.includes('OK')) {
      console.log('starting stream')
    } else if (spd.includes('ERR_INVAL_ARG')) {
      ValueInterpreter.states.warningModal("Stream Failed", "Speed Stream Invalid Argument")
      console.log('spd stream invalid argument')
    } else if (spd.includes('ERR')) {
      ValueInterpreter.states.warningModal("Stream Error", "Speed Stream encountered an error")
      console.log('spd stream error')
    } else if (spd.includes('STOP')) {
      console.log('spd stream stopped')
    } else {
      ValueInterpreter.states.dispatch({ type: 'UPDATE_SPEED', value: ValueInterpreter.extractValue(spd, "s:") })
    }
  }

  static readHB = (value) => {
    if (value.includes('OK')) {
      return
    } else if (value.includes('ERR')) {
      console.log("here")
      ValueInterpreter.states.warningModal("Stream Error", "HB Stream encountered an error")
      console.log('HB ERROR')
    } else if (value.includes('STOP')) {
      console.log('HB STOPPED')
    } else {
      let hb: any = value.split('hb:')
      hb = hb[1]
      hb = hb.split('>')
      hb = hb[0]
      hb = hb.split(' ')
      ValueInterpreter.states.dispatch({ type: 'UPDATE_HEARTBEAT', value: ValueInterpreter.extractValue(value, "hb:").split(' ') })
    }
  }

  static readFloat = (value, state) => {
    let float = ValueInterpreter.extractValue(value, "f:");

    let output = []
    for (let i = 0; i < float.length; i++) {
      output.push(float[i].charCodeAt().toString(2) + ' ')
    }

    console.log('ReadFLoat: float is now: ' + float)
    console.log('Float2Bin: ' + output)

    console.log('ReadFLoat: float is now: ' + float)

    state(float);
  }

  static readIsCal = (value, setCalibrated) => {
    let isCal = ValueInterpreter.extractValue(value, "cal:");
    console.log('isCal is: ' + isCal)
    if (isCal.includes("ERR")) {
      ValueInterpreter.states.warningModal("Calibration Failed", "Check your input values and try again")
    }
    setCalibrated(isCal)
  }
}

export default ValueInterpreter
