import React, { createContext } from 'react'

export const CompatabilityContext = createContext<any>(undefined)

export default ({ children }) => {

    const checkBrowser = () => {

        const userBrowser = navigator.userAgent

        if(!(userBrowser.indexOf("Chrome") > -1)){
            return false
        } else {
            const raw = userBrowser.match(/Chrom(e|ium)\/([0-9]+)\./)

            const chromeVersion = parseInt(raw[2], 10)
    
            if (chromeVersion >= 89) {
                return true
            } else {
                return false
            }
        }
    }

    const checkOS = () => navigator.appVersion.indexOf('Linux') != -1

    return (
        <CompatabilityContext.Provider value={{
            checkBrowser,
            checkOS,
        }}>
            {children}
        </CompatabilityContext.Provider>
    )
}