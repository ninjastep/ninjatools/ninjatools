class SerialPort {
  isAvailable: boolean
  baudRate: number
  private reader
  private writer
  isOpen: boolean
  port
  private inputDone
  private inputStream

  constructor(baudRate) {
    this.isAvailable = this.checkSerial()
    this.baudRate = baudRate
  }

  // @ts-ignore
  checkSerial = () => (navigator.serial ? true : false)

  requestPort = async () => {
    // @ts-ignore
    this.port = await navigator.serial.requestPort()
  }

  openPort = async () => {
    await this.port.open({ baudRate: this.baudRate })
      .catch(async(error) => {
        await this.port.close();
        throw error;
      })
    this.isOpen = true
  }

  closePort = async () => {
    await this.port.close()
    this.isOpen = false
  }

  openLineBreakStream = () => {
    console.log('opening LineBreak Stream');
    let decoder = new TextDecoderStream();
    this.inputDone = this.port.readable.pipeTo(decoder.writable);
    this.inputStream = decoder.readable.pipeThrough(
      new TransformStream(new LineBreakTransformer()),
    )
    this.reader = this.inputStream.getReader();
  }

  closeStream = async () => {
    await this.reader.cancel();
    await this.reader.releaseLock();
    await this.inputDone.catch(() => {})
  }
  
  read = async () => await this.reader.read()
  

  write = async (command) => await this.writer.write(command);

  initialize = async () => {
    this.writer = await this.port.writable.getWriter();
  }

  disconnect = async () => {
    await this.closeStream();
    await this.writer.releaseLock();
    await this.closePort()
  }

  hardReset = async () => {
    await this.port.setSignals({ dataTerminalReady: false} )
    await this.port.setSignals({ requestToSend: true} )
    setTimeout(async () => {
      await this.port.setSignals({ dataTerminalReady: true })
      await this.port.setSignals({ requestToSend: false })
    }, 500)
    setTimeout(async () => {
      await this.port.setSignals({ dataTerminalReady: true })
      await this.port.setSignals({ requestToSend: true })
    }, 500)
    return;
  }
}

// The LinebreakTransformer is used to break up the incoming stream from the NS-motor by linebreaks
class LineBreakTransformer {
  constructor() {
    // A container for holding stream data until a new line.
    // @ts-ignore
    this.container = ''
  }

  transform(chunk, controller) {
    // @ts-ignore
    this.container += chunk.toString()
    // @ts-ignore
    const lines = this.container.split('\n')
    // @ts-ignore
    this.container = lines.pop()
    lines.forEach((line) => controller.enqueue(line))
  }

  flush(controller) {
    // @ts-ignore
    controller.enqueue(this.container)
  }
}

export default SerialPort
