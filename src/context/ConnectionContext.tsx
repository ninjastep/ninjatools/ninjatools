import React, { createContext, useState, useContext } from 'react'
import { SerialPortContext, CommunicationContext, MotorContext } from './index'
import { Modal, Menu } from 'antd'
import {
    RedoOutlined,
    CompassOutlined,
    DashboardOutlined,
} from '@ant-design/icons'
import ValueInterpreter from './ValueInterpreter'

export const ConnectionContext = createContext(undefined)

let knockResp = false
let commandTries = 0

export default ({ children }) => {
    const [connectionType, setConnectionType] = useState('serial')
    const [connected, setConnected] = useState(false)
    const [controller, setController] = useState('')
    const [resetStatus, setResetStatus] = useState('finished')

    const {
        sendCommand,
        setIsLoading,
        readPort,
        setPidTuneData,
        demoMode
    } = useContext(CommunicationContext)

    const {
        setCalibrationStage,
        setRunning,
        running,
    } = useContext(MotorContext)

    const { serialPort } = useContext(SerialPortContext)

    // GENERAL METHODS

    const setKnockResp = (value) => (knockResp = value)

    // Calls on the openSerialPort method and then double checks that we have a valid connection with the checkConnection (which is called after a 0.5 second delay)
    const createConnection = async () => {
        //  checks that connection type is serial, for now we only support connection via web serial
        if (!demoMode) {
            if (connectionType === 'serial') {
                const response = await requestSerialPort();
                // knockRespond is a variable for checkConnection and will be set to true if the reader stream receives <NINJA>
                knockResp = false

                if (response !== -1) {
                    await openSerialPort()
                    setTimeout(() => {
                        serialPort.isOpen && sendCommand('knock_knock')
                        checkConnection()
                    }, 500)
                }
            }
        }
        else {
            setConnected(true)
            readPort()
        }
    }

    // Eventlistener that detects when serial gets unplugged and redirects to initial page
    const handleDisconnect = (e) => {
        e.preventDefault()
        console.log('disconnected')
        setRunning(false)
        setConnected(false)
    }
    // @ts-ignore
    navigator.serial && navigator.serial.addEventListener('disconnect', handleDisconnect)

    // Uses closeSerialPort to close the connection with the motor.
    const closeConnection = async () => {
        if (connectionType === 'serial') {
            await closeSerialPort()
        }
    }

    // Method to check that a connection has been established
    const checkConnection = async () => {
        if (knockResp) {
            // knockResp = false;
            commandTries = 0
            return
        } else if (commandTries > 4) {
            setConnected(false)
            setIsLoading(false)
            serialPort.isOpen && (await closeConnection())
            commandTries = 0
            warningModal(
                'Connection Failed',
                'Device is not responding, check your device permissions or if your device is connected somewhere else',
            )
            console.log('Connection Failed')
            return
        } else {
            serialPort.isOpen && sendCommand('exit')

            setTimeout(() => {
                serialPort.isOpen && sendCommand('knock_knock')
            }, 500)
            console.log('command try: ' + commandTries)
            commandTries++
            setTimeout(() => {
                checkConnection()
            }, 3000)
        }
    }

    //SERIAL COMMUNICATION

    //  Use filter to only allow Silicon labs products thus identifying the NS-motor
    //  currently not used.
    const siliconLabsFilter = {
        usbVendorId: 0x10c4,
    }

    const requestSerialPort = async () => {
        // Requests port from user
        return await serialPort.requestPort().catch((error) => {
            console.log('openSerialPort error:')
            console.log(error)
            setIsLoading(false)
            return -1
        })
    }

    const openSerialPort = async () => {
        // Opens Port to NS-motor
        await serialPort
            .openPort()
            .then(async () => {
                setIsLoading(true)
                await serialPort.initialize()
                await serialPort.hardReset()
                serialPort.openLineBreakStream();
                readPort();
            })
            .catch((error) => {
                console.log(error)
                permissionModal()
            })
    }

    // closeSerialPort is used to close the connection with the NS-motor
    const closeSerialPort = async () => {
        if (running) {
            sendCommand('stop_mot')
        }
        await serialPort.disconnect()
        setRunning(false)
        setConnected(false)
    }

    const checkResetStatus = () => {
        return new Promise((resolve) => {
            let isDone = false
            let checkStatus = setInterval(() => {
                setResetStatus((state) => {
                    isDone = state === 'finished'
                    return state
                })
                if (isDone) {
                    clearInterval(checkStatus)
                    resolve('reset done')
                }
            }, 50)
        })
    }

    const softReset = async () => {
        setResetStatus('running')
        setRunning(false)
        sendCommand('exit')
        sendCommand('rst')
        await checkResetStatus().then((value) => {
            value === 'reset done' && sendCommand('knock_knock')
            return
        })
    }

    const handleReset = (value) => {
        // Had to do this because I could not access the updated state without doing it this way.
        setResetStatus((state) => {
            let temp = state
            if (state === 'running') {
                if (
                    value.includes(
                        'flash_config: Calibration file successfully read to flash',
                    )
                ) {
                    console.log('reset done')
                    temp = 'finished'
                }
            }
            return temp
        })
    }

    const handleCalibrationStage = (value) => {
        setCalibrationStage((state) => {
            let temp = state
            if (value.includes(`Step ${state + 1}:`)) {
                temp = state + 1
            }
            return temp
        })
    }

    const permissionModal = () => {
        Modal.error({
            title: 'Permission Denied',
            content: (
                <>
                    Could not access device.
                    <br />
          Check your device permissions or if your device is connected somewhere
          else.
                </>
            ),
        })
    }

    const warningModal = (title, content) => {
        Modal.error({
            title: title,
            content: content,
        })
    }

    ValueInterpreter.init({
        setConnected,
        setKnockResp,
        setController,
        warningModal,
        handleCalibrationStage,
        handleReset,
    })


    //Since we can change controller from both home and controller setup we export this function from here
    const selectController = (option) => {
        switch (option) {
            case 0:
                sendCommand('set_ctrl_mode 0')
                setIsLoading(true)
                setTimeout(() => {
                    serialPort.hardReset()
                }, 500)
                setTimeout(() => {
                    sendCommand('knock_knock')
                }, 2500)
                setTimeout(() => {
                    sendCommand('get_ctrl_mode')
                }, 3000)
                break
            case 1:
                sendCommand('set_ctrl_mode 1')
                setIsLoading(true)
                setTimeout(() => {
                    serialPort.hardReset()
                }, 500)
                setTimeout(() => {
                    sendCommand('knock_knock')
                }, 2500)
                setTimeout(() => {
                    sendCommand('get_ctrl_mode')
                }, 3000)
                break
            case 2:
                sendCommand('set_ctrl_mode 2')
                setIsLoading(true)
                setTimeout(() => {
                    serialPort.hardReset()
                }, 500)
                setTimeout(() => {
                    sendCommand('knock_knock')
                }, 2500)
                setTimeout(() => {
                    sendCommand('get_ctrl_mode')
                }, 3000)
                break
        }
        setPidTuneData(
            [
                {
                    position: 0,
                    reference: 0,
                    time: 0,
                },
            ]
        )
        setRunning(false)
    }

    const controllerDropDownMenu = (
        <Menu>
            <Menu.Item
                key="1"
                icon={<RedoOutlined />}
                onClick={() => selectController(0)}
            >
                Position
          </Menu.Item>
            <Menu.Item
                key="2"
                icon={<CompassOutlined />}
                onClick={() => selectController(1)}
            >
                Cascade Position
          </Menu.Item>
            <Menu.Item
                key="3"
                icon={<DashboardOutlined />}
                onClick={() => selectController(2)}
            >
                Speed
          </Menu.Item>
        </Menu>
    )

    const dropdownPreview = () => {
        if (controller === 'Position') {
            return <>Cascade Position</>
        } else if (controller === 'Speed') {
            return <>Speed</>
        } else {
            return <>Position</>
        }
    }

    const controllerIcon = () => {
        if (controller === 'Position') {
            return <CompassOutlined />
        } else if (controller === 'Speed') {
            return <DashboardOutlined />
        } else {
            return <RedoOutlined />
        }
    }

    return (
        <ConnectionContext.Provider value={{
            connectionType,
            connected,
            setConnected,
            controller,
            handleCalibrationStage,
            handleReset,
            setKnockResp,
            setConnectionType,
            createConnection,
            closeConnection,
            setController,
            softReset,
            selectController,
            controllerDropDownMenu,
            dropdownPreview,
            controllerIcon,
            requestSerialPort,
        }}>
            {children}
        </ConnectionContext.Provider>
    )
}