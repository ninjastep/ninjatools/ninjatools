import React, { createContext, useState, useContext } from 'react'
import { useDispatch } from 'react-redux'
import './styles/clipboard.css'
import ValueInterpreter from './ValueInterpreter'
import { MotorContext, SerialPortContext, ConnectionContext } from './index'
import { MockSerialPort } from '../components/helpers/MockSerialPort'

/*
Communication Context is Ninja Tools most important component. 
*/

// These variables and functions are exported to all children (see structure in App.tsx)
export const CommunicationContext = createContext<any>({
  // connectionType: 'serial',
  // baudrateState: 115200,
  // connected: false,
  // controller: '',
  pidRef: 0.0,
  pidTuneActive: false,
  liveTrackPos: false,
  liveTrackSpd: false,
  liveTrackTrq: false,
  liveTrack: false,
  controllerDropDownMenu: 0,
  // terminalOutput: "",
  // stepLength: 0,
  // stepTime: 0,
  pidTuneData: [],
  // pidHz: 20,
  pidChartStartValue: 0,
  isLoading: false,
  csvData: [],
  // setConnectionType: () => { },
  // createConnection: () => { },
  // closeConnection: () => { },
  // setBaudrateState: () => { },
  sendCommand: () => { },
  // setTerminalActive: () => {},
  degreesToRadians: () => { },
  radiansToDegrees: () => { },
  safeSendCommand: () => { },
  // setController: () => { },
  chainCommands: () => { },
  setPIDRef: () => { },
  setLiveTrackPos: () => { },
  setLiveTrackSpd: () => { },
  setLiveTrackTrq: () => { },
  setLiveTrack: () => { },
  // setStepLength: () => {},
  // setStepTime: () => {},
  // setPidHz: () => {},
  setPidChartStartValue: () => { },
  setPidtuneActive: () => { },
  setIsLoading: () => { },
  setCsvData: () => { },
  // softReset: async () => { },
  // hardReset: () => { },
  pidIsNegative: false,
  setPidIsNegative: () => { },
  // selectController: () => { },
  // dropdownPreview: () => { },
  // controllerIcon: () => { },
  // serialPort: undefined,
  // requestSerialPort: async () => { },
})

// These variables are used to communicate with the NS-motor. These are declared outside the main function so that they don't rerender.
const pi = Math.PI
let readyToSend = true

// let decoder = new TextDecoder();
let encoder = new TextEncoder()

const CommunicationProvider = (props: any) => {
  // Dispatch is used to call functions from the Redux, specifically when updating the heartbeat values (and other variables that updates rapidly)
  const dispatch = useDispatch()
  const setReadyToSend = (value) => {
    readyToSend = value
  }

  //Hook variables are exported and set all over the website via React Context
  const { serialPort } = useContext(SerialPortContext)
  const [demoMode, setDemoMode] = useState(false)
  const mockSerialPort = new MockSerialPort()

  //PID Tuning variables
  const [pidRef, setPIDRef] = useState(0.0)
  const [liveTrackPos, setLiveTrackPos] = useState(false)
  const [liveTrackSpd, setLiveTrackSpd] = useState(false)
  const [liveTrackTrq, setLiveTrackTrq] = useState(false)
  const [liveTrack, setLiveTrack] = useState(false)
  const [pidTuneActive, setPidTuneActive] = useState(false)
  // const [stepLength, setStepLength] = useState(0);
  // const [stepTime, setStepTime] = useState(0);
  const [pidTuneData, setPidTuneData] = useState([
    {
      position: 0,
      reference: 0,
      time: 0,
    },
  ])
  const [pidChartStartValue, setPidChartStartValue] = useState(0)
  const [isLoading, setIsLoading] = useState(false)
  const [csvData, setCsvData] = useState([])
  const [pidIsNegative, setPidIsNegative] = useState(false)
  // GENERAL METHODS

  // The portReader is always active in async mode so it doesnt interupt the flow, however unlike the writeToPort this does not close
  // until we close the connection with NS-motor.
  const readPort = async () => {
    console.log('Reading Port')

    // Recursive reading loop for SerialPorts ReadableStream
    if (!demoMode) {
      const loop = async () => {
        await serialPort.read()
          .then(({ value, done }) => {
            value && (() => {
              interpretValue(value);
              dispatch({ type: 'UPDATE_TERMINAL', value: value })
            })()
            // If 'done' is true, loop will not be called again
            done ? console.log('[readLoop] DONE', done) : loop();
          })
          .catch((error) => {
            // Reset stream if buffer is full
            error.name === 'BufferOverrunError' &&
              (() => {
                serialPort.openLineBreakStream()
                loop()
              })()
          })
      }
      loop();
      console.log('releasing portReader lock')
    } else {
      const loop = () => {
        let value = mockSerialPort.read()
        value && (() => {
          interpretValue(value);
          dispatch({ type: 'UPDATE_TERMINAL', value: value })
        })()
        // If 'done' is true, loop will not be called again
        !value ? console.log('[readLoop] DONE', true) : loop();
      }
      loop();
    }
  }

  // writeToPort sends commands to the NS-motor, this stream is closed and reopened with each message unlike the reader which is constantly open.
  const writeToPort = async (command) => {
    if (command.split(' ')[1] == 'NaN') {
      return
    }
    console.log('writingToPort ' + command)

    const encodedCommand = encoder.encode(command + '\n')
    await serialPort.write(encodedCommand)
  }

  //  The original function to send command to the Ninja Step motor through the stream
  const sendCommand = async (command) => {
    // console.log("sendCommand " + command);
    //ORIGINAL
    // console.log("serial is true");
    // writeToPort(command)
    if (!demoMode )
      await writeToPort(command)
    else
      mockSerialPort.write(command)
      readPort()
  }

  //  chainCommands & safeSendCommand are to make sure commands get through the stream and is often
  //  called upon when we want to send many commands rapidly
  const chainCommands = (commandArray) => {
    for (let i = 0; i < commandArray.length; i++) {
      safeSendCommand(commandArray[i])
    }
  }

  const safeSendCommand = (command, backoffTime = 1) => {
    if (readyToSend) {
      // console.log("READY")
      // console.log("send command: " + command);
      sendCommand(command)
    } else {
      setTimeout(() => {
        safeSendCommand(command, backoffTime * 2)
      }, backoffTime * 5)
    }
  }

  // Sharing states with ValueInterpreter
  // const setStates = {
  //   setIsLoading,
  //   dispatch,
  //   setReadyToSend
  // }

  //  This function is very important, it reads the stream from the Ninja Step motor and calls on different functions
  //  depending on the contents of the stream
  // const valueInterpreter = new ValueInterpreter(setStates)
  const interpretValue = (value) => {
    console.log(value)
    ValueInterpreter.handleValue(value)
  }

  // These methods are used in the position slider on the home page
  const degreesToRadians = (degrees) => {
    const radians = degrees * (pi / 180)
    const roundedRadians = Math.round(radians * 100) / 100
    return roundedRadians
  }

  const radiansToDegrees = (radians) => {
    const degrees = radians * (180 / pi)
    const roundedDegrees = Math.round(degrees * 100) / 100
    return roundedDegrees
  }

  return (
    <CommunicationContext.Provider
      value={{
        pidRef,
        pidTuneActive,
        liveTrackPos,
        liveTrackSpd,
        liveTrackTrq,
        liveTrack,
        // stepLength,
        // stepTime,
        pidTuneData,
        // pidChartStartValue,
        isLoading,
        csvData,
        sendCommand,
        degreesToRadians,
        radiansToDegrees,
        safeSendCommand,
        chainCommands,
        setPIDRef,
        setPidTuneActive,
        setLiveTrackPos,
        setLiveTrackSpd,
        setLiveTrackTrq,
        setLiveTrack,
        closeStream: serialPort.closeStream,
        // setStepLength,
        // setStepTime,
        setPidTuneData,
        setPidChartStartValue,
        setIsLoading,
        setCsvData,
        hardReset: serialPort.hardReset,
        pidIsNegative,
        setPidIsNegative,
        readPort,
        setReadyToSend,
        readyToSend,
        dispatch,
        demoMode,
        setDemoMode,
      }}
    >
      {props.children}
    </CommunicationContext.Provider>
  )
}

export default CommunicationProvider
