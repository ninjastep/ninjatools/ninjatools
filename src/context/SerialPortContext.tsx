import React, { createContext, useState } from 'react'
import SerialPort from './SerialPort'

export const SerialPortContext = createContext (undefined)

export default ({ children }) => {
    const [baudrateState, setBaudrateState] = useState(115200)
    const [serialPort] = useState(new SerialPort(baudrateState))

    return (
        <SerialPortContext.Provider value={{
            serialPort,
            baudrateState,
            setBaudrateState
        }}>
            {children}
        </SerialPortContext.Provider>
    )
}