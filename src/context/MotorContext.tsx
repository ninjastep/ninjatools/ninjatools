import React, { createContext, useState } from 'react'

export const MotorContext = createContext(undefined)

export default ({ children }) => {
    const [running, setRunning] = useState(false)
    const [float, setFloat] = useState(0.0)
    const [resistance, setResistance] = useState(0.0)
    const [inductance, setInductance] = useState(0.0)
    const [motorConstant, setMotorConstant] = useState(0.0)
    const [poleNumber, setPoleNumber] = useState(0.0)
    const [maxCurrent, setMaxCurrent] = useState(0.0)
    const [maxSpeed, setMaxSpeed] = useState(0.0)
    const [maxTorque, setMaxTorque] = useState(0.0)
    const [calibrated, setCalibrated] = useState('NO')
    const [calibrationStage, setCalibrationStage] = useState(0)

    const [pos, setPos] = useState(0.0)
    const [posKP, setPosKP] = useState(0.0)
    const [posKI, setPosKI] = useState(0.0)
    const [posKD, setPosKD] = useState(0.0)
    const [spdKP, setSpdKP] = useState(0.0)
    const [spdKI, setSpdKI] = useState(0.0)
    const [spdKD, setSpdKD] = useState(0.0)
    const [trqKP, setTrqKP] = useState(0.0)
    const [trqKI, setTrqKI] = useState(0.0)
    const [trqKD, setTrqKD] = useState(0.0)

    return (
        <MotorContext.Provider value={{
            running, setRunning,
            float, setFloat,
            resistance, setResistance,
            inductance, setInductance,
            motorConstant, setMotorConstant,
            poleNumber, setPoleNumber,
            maxCurrent, setMaxCurrent,
            maxSpeed, setMaxSpeed,
            maxTorque, setMaxTorque,
            calibrated, setCalibrated,
            calibrationStage, setCalibrationStage,
            pos, setPos,
            posKP, setPosKP,
            posKI, setPosKI,
            posKD, setPosKD,
            spdKP, setSpdKP,
            spdKI, setSpdKI,
            spdKD, setSpdKD,
            trqKP, setTrqKP,
            trqKI, setTrqKI,
            trqKD, setTrqKD,
        }}>
            {children}
        </MotorContext.Provider>
    )
}