import React, { useContext } from "react";
import { CommunicationContext } from "../context/CommunicationContext";
import {useSelector, useDispatch} from 'react-redux';


const MinEnv = () => {

  const position = useSelector( (state) => state.position );
  const speed = useSelector( (state) => state.speed );
  const torque = useSelector( (state) => state.torque );
  const current = useSelector( (state) => state.current );
  const voltage = useSelector( (state) => state.voltage );
  const dispatch = useDispatch();

  const {
  sendCommand,
  } = useContext(CommunicationContext);

console.log("MinEv rendering");

  return (
    <>
      {" "}
          <div>
            pos: {position}
            <br />
            volt: {voltage}
            <br />
            current: {current}
            <br />
            speed: {speed}
            <br />
            torque: {torque}
            {/* <br />
            <button onClick={() => openSerialPort()}>Connect</button> */}
            <br />
            <button onClick={() => sendCommand("knock_knock")}>Knock</button>
            <br />
            <button onClick={() => sendCommand("start_mot")}>Start Mot</button>
            <br />
            <button onClick={() => sendCommand("start_hb_stream 0.1")}>
              HB 1/s
            </button>
            <br />
            <button onClick={() => sendCommand("start_hb_stream 1")}>
              HB 10/s
            </button>
            <br />
            <button onClick={() => sendCommand("stop_hb_stream")}>
              Stop HB
            </button>
          </div>
    </>
  );
};

export default React.memo(MinEnv);
