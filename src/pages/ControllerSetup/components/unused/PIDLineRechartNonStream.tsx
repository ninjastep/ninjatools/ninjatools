import React, { useContext, useRef } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Brush,
} from "recharts";
import { CommunicationContext } from "../../../../context/CommunicationContext";
import { CSVLink } from "react-csv";
import { Spin, Space, Button } from "antd";

const headers = [
  { label: "Value", key: "value" },
  { label: "mSec", key: "mSec" },
  { label: "Position", key: "position" },
];

const PIDLineRechartNonStream = () => {
  const linkRef = useRef(null);
  const {
    pidTuneData,
    isLoading,
    csvData,
    controller,
  } = useContext(CommunicationContext);

  // console.log("LineChart loading:")
  // console.log(isLoading)
  // console.log("pidTuneData: ")
  // console.log(pidTuneData)


  // if (isLoading) {
  //   return <Spin size="large" />;
  // } else {
    return (
      <>
        {/* <ResponsiveContainer width="100%" height={300}> */}
          <LineChart data={pidTuneData}
          width={600}
          height={300}
          >
            <XAxis dataKey="time" />
            <YAxis domain={["pidChartStartValue", "auto"]} />

            <Legend />
            <Line
              type="monotone"
              dataKey="position"
              stroke="#8884d8"
              dot={{ r: 0 }}
              activeDot={{ r: 3 }}
              isAnimationActive={true}
            />
            {/* <Brush dataKey="time" height={30} stroke="#8884d8" /> */}
            <Line
              type="monotone"
              dataKey="reference"
              stroke="#82ca9d"
              dot={{ r: 0 }}
              activeDot={{ r: 3 }}
              isAnimationActive={true}
            />
            <Tooltip />
          </LineChart>
        {/* </ResponsiveContainer> */}
        <Space>
          <Button onClick={() => (linkRef as any).current.link.click()}>
            Download CSV
          </Button>
          <CSVLink
            ref={linkRef}
            style={{ display: "none" }}
            data={csvData}
            headers={headers}
            filename={controller + "_PID.csv"}
          >
            Export CSV
          </CSVLink>
        </Space>
      </>
    );
  // }
};

export default PIDLineRechartNonStream;
