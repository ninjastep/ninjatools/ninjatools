import React, { useContext, useEffect, useRef, useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  ReferenceArea,
  CartesianGrid,
} from "recharts";
import * as _ from "lodash"
import { CommunicationContext, ConnectionContext } from "../../../context/index";
import { CSVLink } from "react-csv";
import { Spin, Space, Button } from "antd";

const headers = [
  { label: "Value", key: "value" },
  { label: "mSec", key: "mSec" },
  { label: "Speed", key: "speed" },
];

const PIDSpdChart = () => {
  const linkRef = useRef(null);
  const {
    pidTuneData,
    setPidTuneData,
    isLoading,
    csvData,
    pidIsNegative,
  } = useContext(CommunicationContext);

  const { controller } = useContext(ConnectionContext)
  
  useEffect(() => {
      let max = !pidIsNegative ? Math.max.apply(Math, pidTuneData.map(o => o.speed)) * 0.25 :
      (Math.min.apply(Math, pidTuneData.map(o => o.speed)) * 0.25) * - 1;
      setTop("dataMax+"+max);
      setBottom("dataMin-"+max)

  }, [pidTuneData])

  const [left, setLeft] = useState("dataMin");
  const [right, setRight] = useState("dataMax");
  const [refAreaLeft, setRefAreaLeft] = useState("");
  const [refAreaRight, setRefAreaRight] = useState("");
  const [top, setTop] = useState("dataMax+" + 5);
  const [bottom, setBottom] = useState("dataMin-" + 5);

  const getAxisYDomain = (from, to, offset) => {

    const refData = _.filter(pidTuneData, function (obj) { return (obj.time >= from && obj.time <= to); });

    const top = _.maxBy(refData, function (o) { return o.speed; }).speed;
    const bottom = _.minBy(refData, function (o) { return o.speed; }).speed;

    return [(bottom | 0) - offset, (top | 0) + offset];
  };

  const zoom = (e) => {

    if (refAreaLeft === refAreaRight || refAreaRight === "") {
      setRefAreaLeft("");
      setRefAreaRight("");

      return;
    }

    let newAreaLeft;
    let newAreaRight;

    // xAxis domain
    if (refAreaLeft > refAreaRight) {
      const tempArea = refAreaLeft;
      newAreaLeft = refAreaRight;
      newAreaRight = tempArea;
    }

    const [newBottom, newTop] = getAxisYDomain(newAreaLeft || refAreaLeft, newAreaRight || refAreaRight, 1);

    setBottom(newBottom);
    setTop(newTop);
    setRefAreaLeft(newAreaLeft || "");
    setRefAreaRight(newAreaRight || "");
    setLeft(newAreaLeft || refAreaLeft);
    setRight(newAreaRight || refAreaRight);
    // setPidTuneData(pidTuneData.slice());
  };

  const zoomOut = () => {
    setPidTuneData(pidTuneData.slice());
    setRefAreaLeft("");
    setRefAreaRight("");
    setLeft("dataMin");
    setRight("dataMax");
    setTop("dataMax+1");
    setBottom("dataMin-1");
  };

  const formatter = (value) => `${value.toFixed(2)}`;

  if (isLoading) {
    return <Spin size="large" />;
  } else {
    return (
      <>
        <ResponsiveContainer width="99%" height={320}>
          <LineChart
            data={pidTuneData}
            height={300}
            onMouseDown={(e) => e ? setRefAreaLeft(e.activeLabel) : null}
            onMouseMove={(e) => e ? setRefAreaRight(e.activeLabel) : null}
            onMouseUp={(e) => e ? zoom(e) : null}
          >
            <CartesianGrid strokeDasharray="3 3" />

            <XAxis
              allowDataOverflow
              dataKey="time"
              domain={[left, right]}
              type="number"
            />
            <YAxis
              allowDataOverflow
              domain={[bottom, top]}
              type="number"
              tickFormatter={(formatter)}
              reversed={pidIsNegative}
            />
            <Legend layout="horizontal" verticalAlign="top" align="center" />
            <Line
              type="monotone"
              dataKey="speed"
              stroke="#8884d8"
              dot={{ r: 0 }}
              activeDot={{ r: 3 }}
              isAnimationActive={true}
            />
            <Line
              type="monotone"
              dataKey="reference"
              stroke="#82ca9d"
              dot={{ r: 0 }}
              activeDot={{ r: 3 }}
              isAnimationActive={true}
            />
            <Tooltip />
            {refAreaLeft && refAreaRight ? (
              <ReferenceArea
                x1={refAreaLeft}
                x2={refAreaRight}
                strokeOpacity={0.3}
              />
            ) : null}
          </LineChart>
        </ResponsiveContainer>
        <Space>
          <Button onClick={() => (linkRef as any).current.link.click()}>
            Download CSV
          </Button>
          <CSVLink
            ref={linkRef}
            style={{ display: "none" }}
            data={csvData}
            headers={headers}
            filename={controller + "_PID.csv"}
          >
            Export CSV
          </CSVLink>
          <Button onClick={() => zoomOut()}>
            Zoom Out
          </Button>
        </Space>
      </>
    );
  }
};

export default PIDSpdChart;
