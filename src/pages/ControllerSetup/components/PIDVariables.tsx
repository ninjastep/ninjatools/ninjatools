import React, { useContext, useEffect, useRef } from 'react'
import {
  Row,
  Col,
  Typography,
  Button,
  Dropdown,
  Form,
  Spin,
} from 'antd'
import {
  DownOutlined,
} from '@ant-design/icons'
import { CommunicationContext, MotorContext, ConnectionContext } from '../../../context/index'
import ContentPanel from '../../../components/ContentPanel'
import CustomInput from '../../../components/CustomInput'

const PIDVariables = () => {
  const {
    maxSpeed,
    maxTorque,
    posKP,
    posKI,
    posKD,
    spdKP,
    spdKI,
    spdKD,
    trqKP,
    trqKI,
    trqKD,
    setPosKP,
    setPosKI,
    setPosKD,
    setSpdKP,
    setSpdKI,
    setSpdKD,
    setTrqKP,
    setTrqKI,
    setTrqKD,
    setMaxSpeed,
    setMaxTorque,
  } = useContext(MotorContext)

  const {
    sendCommand,
    chainCommands,
    isLoading,
  } = useContext(CommunicationContext)

  const {
    connected,
    controller,
    dropdownPreview,
    controllerDropDownMenu,
    controllerIcon,
  } = useContext(ConnectionContext)

  const [form] = Form.useForm()

  const getPIDSettings = () => {
    chainCommands([
      'get_ctrl_mode',
      'get_pos_p',
      'get_pos_i',
      'get_pos_d',
      'get_spd_p',
      'get_spd_i',
      'get_spd_d',
      'get_trq_p',
      'get_trq_i',
      'get_trq_d',
      'get_trq_max',
      'get_spd_max',
    ])
  }

  useEffect(() => {
    if (connected) {
      getPIDSettings()
    }
  }, [])

  // References to autoswich focus from inputfields on enterpress

  // Refs for Cascade Position (position) controller
  const refPosPkp = useRef(null)
  const refPosPki = useRef(null)
  const refPosPkd = useRef(null)
  const refPosMspd = useRef(null)
  const refPosSkp = useRef(null)
  const refPosSki = useRef(null)
  const refPosSkd = useRef(null)
  const refPosMtrq = useRef(null)

  // Refs for Speed (speed) controller
  const refSpdKp = useRef(null)
  const refSpdKi = useRef(null)
  const refSpdKd = useRef(null)
  const refSpdMspd = useRef(null)
  const refSpdMtrq = useRef(null)

  // Refs for Position (torque) controller
  const refTrqKp = useRef(null)
  const refTrqKi = useRef(null)
  const refTrqKd = useRef(null)
  const refTrqMtrq = useRef(null)

  const switchFocus = (e) => {
    // Using IIFE and Object to choose what to focus
    ; ({
      // Switching focus on Cascade Position (Position) Inputs
      pidForm_pos_pkp: refPosPki,
      pidForm_pos_pki: refPosPkd,
      pidForm_pos_pkd: refPosMspd,
      pidForm_pos_mspd: refPosSkp,
      pidForm_pos_skp: refPosSki,
      pidForm_pos_ski: refPosSkd,
      pidForm_pos_skd: refPosMtrq,
      pidForm_pos_mtrq: refPosPkp,

      // Switching focus on Speed (Speed) Inputs
      pidForm_spd_kp: refSpdKi,
      pidForm_spd_ki: refSpdKd,
      pidForm_spd_kd: refSpdMtrq,
      pidForm_spd_mtrq: refSpdMspd,
      pidForm_spd_mspd: refSpdKp,

      // Switching focus on Position (Torque) Inputs
      pidForm_trq_kp: refTrqKi,
      pidForm_trq_ki: refTrqKd,
      pidForm_trq_kd: refTrqMtrq,
      pidForm_trq_mtrq: refTrqKp,
    }[e.target.id].current.focus())
  }

  const sendCommandAndUpdate = (set, value, get) => {
    sendCommand(set + ' ' + value)
    setTimeout(() => {
      sendCommand(get)
    }, 500)
  }

  const pidCustomization = () => {
    if (controller === 'Position') {
      return (
        <Form name="pidForm" form={form}>
          <Row gutter={[24, 16]}>
            <Col span={12}>
              <Typography.Title level={4}>Position</Typography.Title>
              <Form.Item name="pos_pkp">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosPkp}
                  onChange={(e) => setPosKP(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_pos_p', posKP, 'get_pos_p')
                    switchFocus(e)
                  }}
                  addonBefore="kP"
                  form={form}
                  state={{ pos_pkp: posKP }}
                />
              </Form.Item>
              <Form.Item name="pos_pki">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosPki}
                  onChange={(e) => setPosKI(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_pos_i', posKI, 'get_pos_i')
                    switchFocus(e)
                  }}
                  addonBefore="kI"
                  form={form}
                  state={{ pos_pki: posKI }}
                />
              </Form.Item>
              <Form.Item name="pos_pkd">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosPkd}
                  onChange={(e) => setPosKD(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_pos_d', posKD, 'get_pos_d')
                    switchFocus(e)
                  }}
                  addonBefore="kD"
                  form={form}
                  state={{ pos_pkd: posKD }}
                />
              </Form.Item>
              <Form.Item name="pos_mspd">
                <CustomInput
                  type="number"
                  min={0}
                  max={100}
                  refs={refPosMspd}
                  onChange={(e) => setMaxSpeed(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_max', maxSpeed, 'get_spd_max')
                    switchFocus(e)
                  }}
                  addonBefore="mSpd"
                  form={form}
                  state={{ pos_mspd: maxSpeed }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Typography.Title level={4}>Speed</Typography.Title>
              <Form.Item name="pos_skp">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosSkp}
                  onChange={(e) => setSpdKP(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_p', spdKP, 'get_spd_p')
                    switchFocus(e)
                  }}
                  addonBefore="kP"
                  form={form}
                  state={{ pos_skp: spdKP }}
                />
              </Form.Item>
              <Form.Item name="pos_ski">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosSki}
                  onChange={(e) => setSpdKI(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_i', spdKI, 'get_spd_i')
                    switchFocus(e)
                  }}
                  addonBefore="kI"
                  form={form}
                  state={{ pos_ski: spdKI }}
                />
              </Form.Item>
              <Form.Item name="pos_skd">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refPosSkd}
                  onChange={(e) => setSpdKD(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_d', spdKD, 'get_spd_d')
                    switchFocus(e)
                  }}
                  addonBefore="kD"
                  form={form}
                  state={{ pos_skd: spdKD }}
                />
              </Form.Item>
              <Form.Item name="pos_mtrq">
                <CustomInput
                  type="number"
                  min={0}
                  max={1}
                  refs={refPosMtrq}
                  onChange={(e) => setMaxTorque(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate(
                      'set_trq_max',
                      maxTorque,
                      'get_trq_max',
                    )
                    switchFocus(e)
                  }}
                  addonBefore="mTrq"
                  form={form}
                  state={{ pos_mtrq: maxTorque }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      )
    } else if (controller === 'Speed') {
      return (
        <Form name="pidForm" form={form}>
          <Typography.Title level={4}>Speed</Typography.Title>
          <Row gutter={[24, 16]}>
            <Col span={12}>
              <Form.Item name="spd_kp">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refSpdKp}
                  onChange={(e) => setSpdKP(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_p', spdKP, 'get_spd_p')
                    switchFocus(e)
                  }}
                  addonBefore="kP"
                  form={form}
                  state={{ spd_kp: spdKP }}
                />
              </Form.Item>
              <Form.Item name="spd_ki">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refSpdKi}
                  onChange={(e) => setSpdKI(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_i', spdKI, 'get_spd_i')
                    switchFocus(e)
                  }}
                  addonBefore="kI"
                  form={form}
                  state={{ spd_ki: spdKI }}
                />
              </Form.Item>
              <Form.Item name="spd_kd">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refSpdKd}
                  onChange={(e) => setSpdKD(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_d', spdKD, 'get_spd_d')
                    switchFocus(e)
                  }}
                  addonBefore="kD"
                  form={form}
                  state={{ spd_kd: spdKD }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="spd_mtrq">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refSpdMtrq}
                  onChange={(e) => setMaxTorque(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate(
                      'set_trq_max',
                      maxTorque,
                      'get_trq_max',
                    )
                    switchFocus(e)
                  }}
                  addonBefore="mTrq"
                  form={form}
                  state={{ spd_mtrq: maxTorque }}
                />
              </Form.Item>
              <Form.Item name="spd_mspd">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refSpdMspd}
                  onChange={(e) => setMaxSpeed(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_spd_max', maxSpeed, 'get_spd_max')
                    switchFocus(e)
                  }}
                  addonBefore="mSpd"
                  form={form}
                  state={{ spd_mspd: maxSpeed }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      )
    } else {
      return (
        <Form name="pidForm" form={form}>
          <Typography.Title level={4}>Position</Typography.Title>
          <Row gutter={[24, 16]}>
            <Col span={12}>
              <Form.Item name="trq_kp">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refTrqKp}
                  onChange={(e) => setTrqKP(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_trq_p', trqKP, 'get_trq_p')
                    switchFocus(e)
                  }}
                  addonBefore="kP"
                  form={form}
                  state={{ trq_kp: trqKP }}
                />
              </Form.Item>
              <Form.Item name="trq_ki">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refTrqKi}
                  onChange={(e) => setTrqKI(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_trq_i', trqKI, 'get_trq_i')
                    switchFocus(e)
                  }}
                  addonBefore="kI"
                  form={form}
                  state={{ trq_ki: trqKI }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="trq_kd">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refTrqKd}
                  onChange={(e) => setTrqKD(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate('set_trq_d', trqKD, 'get_trq_d')
                    switchFocus(e)
                  }}
                  addonBefore="kD"
                  form={form}
                  state={{ trq_kd: trqKD }}
                />
              </Form.Item>
              <Form.Item name="trq_mtrq">
                <CustomInput
                  type="number"
                  min={0}
                  max={50}
                  refs={refTrqMtrq}
                  onChange={(e) => setMaxTorque(e.target.value)}
                  onPressEnter={(e) => {
                    sendCommandAndUpdate(
                      'set_trq_max',
                      maxTorque,
                      'get_trq_max',
                    )
                    switchFocus(e)
                  }}
                  addonBefore="mTrq"
                  form={form}
                  state={{ trq_mtrq: maxTorque }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      )
    }
  }

  const mainContent = () => {
    if (isLoading) {
      return <Spin size="large" />
    } else {
      return (
        <>
          <ContentPanel
            title="PID Variables"
            extra={<Button onClick={getPIDSettings}>Refresh</Button>}
          >
            <Row gutter={[8, 24]}>
              <Dropdown overlay={controllerDropDownMenu}>
                <Button>
                  {controllerIcon()} {dropdownPreview()} <DownOutlined />
                </Button>
              </Dropdown>
            </Row>
            <Row>{pidCustomization()}</Row>
          </ContentPanel>
        </>
      )
    }
  }

  return <>{mainContent()}</>
}

export default PIDVariables
