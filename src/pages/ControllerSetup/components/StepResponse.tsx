import React, { useContext, useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Row, Col, Input, Button, Form, Space, Dropdown, Menu } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { CommunicationContext, MotorContext, ConnectionContext } from "../../../context/index";
import ContentPanel from "../../../components/ContentPanel";
import inputStep from "../../../components/helpers/inputStep";

let csvData,
  pidData = [];
let refPos,
  startTime,
  timeOffset,
  startPos,
  refSpeed;

const StepResponse = () => {
  const position = useSelector((state) => state.position);
  const speed = useSelector((state) => state.speed);

  const [selectedKeys, setSelectedKeys] = useState([
    sessionStorage.getItem("lengthUnit") || "rad",
  ]);
  const [form] = Form.useForm();

  const {
    running,
  } = useContext(MotorContext);
  
  const {
    sendCommand,
    setPidTuneActive,
    setCsvData,
    pidTuneActive,
    setPidTuneData,
    setIsLoading,
    setPidIsNegative,
    degreesToRadians,
    radiansToDegrees,
  } = useContext(CommunicationContext)

  const { controller } = useContext(ConnectionContext)

  //Here we actually listen to the data from NS and update values to display on the charts
  useEffect(() => {
    if (pidTuneActive) {
      const currentTime = new Date().getTime();
      const time = currentTime - startTime;

      if (controller === "Position" || controller === "Torque") {
        let isDegrees = selectedKeys[0] === "°";
        let updateValues = {
          pidPosition: position - startPos,
          pidReference: refPos - startPos,
          csvPosition: position,
          csvReference: refPos,
        }
        
        if (isDegrees) {
          for( let key in updateValues ) 
            updateValues[key] = radiansToDegrees(updateValues[key]);
        }
        
        updatePidData("position", updateValues.pidPosition, updateValues.pidReference, time)
        updateCsvData("position", updateValues.csvPosition, updateValues.csvReference, time)
        
      } else if (controller === "Speed") {
        // console.log("Speed:", speed)
        // console.log("ref:", refSpeed)
        updatePidData("speed", speed, refSpeed, time)
        updateCsvData("speed", speed, refSpeed, time)
      }
    } 
  }, [position, speed]);

  const updatePidData = (controller, value, reference, time) => {
    pidData.push({
      [controller]: value,
      reference,
      time: time - timeOffset,
    })
  }

  const updateCsvData = (controller, controllerValue, refValue, time) => {
    csvData.push({
      value: "reference",
      mSec: time - timeOffset,
      [controller]: refValue,
    },
    {
      value: 'actual',
      mSec: time,
      [controller]: controllerValue,
    })
  }

  // After PID-data has been sampled in array we send it to a global variable so that the chart can display it
  const displayPIDChart = () => {
    setPidTuneData(pidData);
    setCsvData(csvData);
  };

  const convertToNumbers = (obj) => {
    for(let key in obj)
      obj[key] = +obj[key];
  }

  const performPidStep = async (values) => {
    setIsLoading(true);
    saveDefaultValues(values);
    pidData = [];
    csvData = [];
    convertToNumbers(values)
    timeOffset = values.time * 100;
    const duration = values.time * 1000;

    startTime = new Date().getTime();

    if (!running) {
      const startUp = 3000;
      const waitForRef = 1000;
      sendCommand("start_mot")
      await new Promise(resolve => setTimeout(resolve, startUp))
      sendCommand("get_pos")
      sendCommand("get_spd")
      await new Promise(resolve => setTimeout(resolve, waitForRef))
    }

    if (controller === "Speed") {
      spdStep(values.startSpeed, values.stepSpeed, values.freq, timeOffset, duration);
    } else {
      posStep(values.length, values.freq, timeOffset, duration);
    }

  };

  const posStep = (stepLength, freq, offset, duration) => {
    if (selectedKeys[0] === "°") {
      stepLength = degreesToRadians(stepLength);
    }

    startPos = refPos = position;
    console.log("startPos:", startPos)
    console.log("refPos", refPos)
    console.log("position", position)

    sendCommand("start_pos_stream " + freq);

    setPidTuneActive(true);
    setPidIsNegative(stepLength < 0);

    setTimeout(() => {
      refPos = stepLength + startPos;
      sendCommand("set_pos " + refPos);
    }, offset);

    // Time until PID-tuning is stopped
    setTimeout(() => {
      setPidTuneActive(false);
      sendCommand("stop_pos_stream");
      displayPIDChart();
      setIsLoading(false);
    }, duration);
  };

  const spdStep = (startSpeed, stepSpeed, freq, offset, duration) => {
    setPidIsNegative(stepSpeed < 0);
    setPidTuneActive(true);
    refSpeed = startSpeed
   
    if (startSpeed === 0) {
      console.log("No start speed");
      sendCommand("start_spd_stream " + freq);

      setTimeout(() => {
        refSpeed = stepSpeed;
        sendCommand("set_spd " + stepSpeed);
      }, offset);

      setTimeout(() => {
        setPidTuneActive(false);
        sendCommand("stop_spd_stream");
        sendCommand("set_spd " + 0);
        displayPIDChart();
        setIsLoading(false);
      }, duration);
    } else {
      console.log("start speed");
      
      const warmUp = 1000;
      // refSpeed = startSpeed
      sendCommand("set_spd " + startSpeed);

      setTimeout(() => {
        sendCommand("start_spd_stream " + freq);
      }, warmUp);

      setTimeout(() => {
        refSpeed = stepSpeed
        sendCommand("set_spd " + stepSpeed);
      }, offset + warmUp);

      setTimeout(() => {
        setPidTuneActive(false);
        sendCommand("stop_spd_stream");
        sendCommand("set_spd " + 0);
        displayPIDChart();
        setIsLoading(false);
      }, duration + warmUp);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const saveDefaultValues = (values) => {
    sessionStorage.setItem("length", values.length);
    sessionStorage.setItem("time", values.time);
    sessionStorage.setItem("freq", values.freq);
    sessionStorage.setItem("lengthUnit", selectedKeys[0]);
    sessionStorage.setItem("startSpeed", values.startSpeed);
    sessionStorage.setItem("stepSpeed", values.stepSpeed);
  };

  const getDefaultValue = (value) => {
    // console.log(value + ":" + sessionStorage.getItem(value))
    if(value === "startSpeed"){
      return sessionStorage.getItem(value) || 0;    //DOESNT WORK, returns undefined instead of || value
    } else if(value === "stepSpeed"){
      return sessionStorage.getItem(value) || 5
    } else if(value === "length")  {
      return sessionStorage.getItem(value) || 1.6
    }
    return sessionStorage.getItem(value);
  };

  const abortStep = () => {
    setPidTuneActive(false);
    switch (controller) {
      case "Position":
        sendCommand("stop_pos_stream");
        break;
      case "Speed":
        sendCommand("stop_spd_stream");
        break;
      default:
        console.log("controller not identified");
        break;
    }

    setIsLoading(false);
  };

  const handleMenuClick = (e) => {
    // Selects unit and handles conversion
    setSelectedKeys((prev) => {
      if (prev[0] !== e.key) {
        if (prev[0] === "rad") {
          form.setFieldsValue({
            length: radiansToDegrees(form.getFieldValue("length")),
          });
        } else {
          form.setFieldsValue({
            length: degreesToRadians(form.getFieldValue("length")),
          });
        }
      }
      return [e.key];
    });
  };

  const menu = (
    <Menu onClick={handleMenuClick} selectedKeys={selectedKeys}>
      <Menu.Item key="rad">rad</Menu.Item>
      <Menu.Item key="°">°</Menu.Item>
    </Menu>
  );

  const StepResponseController = () => {
    if (controller === "Speed") {
      return (
        <Row>
          <Form
            name="speedTuneForm"
            initialValues={{ remember: true }}
            onFinish={performPidStep}
            onFinishFailed={onFinishFailed}
            form={form}
          >
            <Row gutter={[24, 16]}>
              <Col span={24}>
                <Form.Item
                  name="startSpeed"
                  initialValue={
                    // getDefaultValue("startSpeed") || 0
                    0
                  }
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={-25}
                    max={25}
                    step={inputStep(getDefaultValue("startSpeed") || 1)}
                    addonBefore="Start Speed"
                    addonAfter="rads/s"
                  />
                </Form.Item>
                <Form.Item
                  name="stepSpeed"
                  initialValue={
                    // getDefaultValue("stepSpeed") || 5
                    5
                  }
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={-25}
                    max={25}
                    step={inputStep(getDefaultValue("stepSpeed") || 1)}
                    addonBefore="Step Speed"
                    addonAfter="rads/s"
                  />
                </Form.Item>
                <Form.Item
                  name="time"
                  initialValue={getDefaultValue("time") || 1}
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={0}
                    max={50}
                    step={inputStep(getDefaultValue("time") || 3)}
                    addonBefore="Sampling Time"
                    addonAfter="s"
                  />
                </Form.Item>

                <Form.Item
                  name="freq"
                  initialValue={getDefaultValue("freq") || 50}
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={1}
                    max={1000}
                    step={inputStep(getDefaultValue("freq") || 50)}
                    addonBefore="Sampling Rate"
                    addonAfter="samples/s"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Space>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Start
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" danger onClick={abortStep}>
                    Abort
                  </Button>
                </Form.Item>
              </Space>
            </Row>
          </Form>
        </Row>
      );
    } else {
      return (
        <Row>
          <Form
            name="posTuneForm"
            initialValues={{ remember: true }}
            onFinish={performPidStep}
            onFinishFailed={onFinishFailed}
            form={form}
          >
            <Row gutter={[24, 16]}>
              <Col span={24}>
                <Form.Item
                  name="length"
                  initialValue={
                    // getDefaultValue("length") || 1.6
                    1.6
                }
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={-1000}
                    max={1000}
                    step={inputStep(
                      // getDefaultValue("length") || 0.1
                      1
                    )}
                    addonBefore="Length"
                    addonAfter={
                      <Dropdown overlay={menu} placement="bottomCenter">
                        <span>
                          {selectedKeys} {<DownOutlined />}
                        </span>
                      </Dropdown>
                    }
                  />
                </Form.Item>
                <Form.Item
                  name="time"
                  initialValue={getDefaultValue("time") || 1}
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={0}
                    max={50}
                    step={inputStep(getDefaultValue("time") || 3)}
                    addonBefore="Sampling Time"
                    addonAfter="s"
                  />
                </Form.Item>

                <Form.Item
                  name="freq"
                  initialValue={getDefaultValue("freq") || 50}
                  rules={[{ required: true, message: "Please input a value" }]}
                >
                  <Input
                    type="number"
                    min={1}
                    max={1000}
                    step={inputStep(getDefaultValue("freq") || 50)}
                    addonBefore="Sampling Rate"
                    addonAfter="samples/s"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Space>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Start
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" danger onClick={abortStep}>
                    Abort
                  </Button>
                </Form.Item>
              </Space>
            </Row>
          </Form>
        </Row>
      );
    }
  };

  return (
    <>
      <ContentPanel title="Step Response">
        {StepResponseController()}
      </ContentPanel>
    </>
  );
};

export default StepResponse;
