import React, { useContext, useEffect } from "react";
import { Row, Col, Divider } from "antd";
import { ConnectionContext, CommunicationContext } from "../../context/index";
import TorqueBlock from "../../assets/torqueBlock.svg";
import SpeedBlock from "../../assets/speedBlock.svg";
import PositionBlock from "../../assets/positionBlock.svg";
import { PIDVariables, StepResponse, PIDPosChart, PIDSpdChart } from "./components"
import ContentPanel from "../../components/ContentPanel";
import { useHistory } from "react-router-dom";

const ControllerSetup: React.FC<any> = () => {
  const { controller } = useContext(ConnectionContext);

  const history = useHistory();
  const { connected, running } = useContext(ConnectionContext);
  const { sendCommand } = useContext(CommunicationContext);

  useEffect(() => {
    if(!connected) {
      history.push("/")
    }
  },[connected])

  useEffect(() => {
    if (running)
      sendCommand("stop_hb_stream") // For demoMode
  }, [])

  const controllerImage = () => {
    if (controller === "Position") {
      return <img src={PositionBlock} style={{ width: "100%" }} />;
    } else if (controller === "Speed") {
      return <img src={SpeedBlock} style={{ width: "100%" }} />;
    } else {
      return <img src={TorqueBlock} style={{ width: "100%" }} />;
    }
  };

  const pidChart = () => {
    if(controller === "Speed") {
      return(
        <PIDSpdChart />
      );
    } else {
      return(
        <PIDPosChart />
      )
    }
  }

  return (
    <>
      <Row gutter={[24, 24]}>
        <Col span={8}>
          <Row>
            <PIDVariables />
          </Row>

          <Divider />

          <Row>
            <StepResponse />
          </Row>
        </Col>

        <Col span={16}>
          <Row>
            <ContentPanel>{controllerImage()}</ContentPanel>
          </Row>

          <Divider />

          <Row>
            <ContentPanel title="PID-tuning">
              {pidChart()}
            </ContentPanel>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default ControllerSetup;
