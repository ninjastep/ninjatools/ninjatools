import React from "react";
import { useSelector } from "react-redux";
import SpeedGaugeNivo from "../components/SpeedGaugeNivo";
import TorqueGaugeNivo from "../components/TorqueGaugeNivo";
import { Layout, Row, Col, Divider } from "antd";
import ContentPanel from "../../../components/ContentPanel";

const { Content } = Layout;

const Performance = () => {
  const speed = useSelector((state) => state.speed);
  const torque = useSelector((state) => state.torque);

  return (
    <>
      <ContentPanel title="Performance">
        <Row>
          <Col span={24}>
            <Divider orientation="left" plain>
              Speed {speed}
            </Divider>
            <Row>
              <Content
                className="site-layout-background"
                style={{
                  padding: 24,
                  margin: 0,
                  height: 180,
                  minWidth: 100,
                }}
              >
                <SpeedGaugeNivo />
              </Content>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Divider orientation="left" plain>
              Torque {torque}
            </Divider>
            <Row>
              <Content
                className="site-layout-background"
                style={{
                  padding: 24,
                  margin: 0,
                  height: 180,
                  minWidth: 100,
                }}
              >
                <TorqueGaugeNivo />
              </Content>
            </Row>
          </Col>
        </Row>
      </ContentPanel>
    </>
  );
};

export default Performance;
