import React, { useEffect, useContext } from "react";
import { useSelector } from "react-redux";
import { CommunicationContext, MotorContext } from "../../../context/index";
import { ResponsiveBar } from "@nivo/bar";

const SpeedGaugeNivo = () => {
  const { maxSpeed } = useContext(MotorContext)
  const { sendCommand } = useContext(CommunicationContext);
  const speed = useSelector((state) => state.speed);

  useEffect(() => {
    sendCommand("get_spd_max");
  }, []);

  const data = [
    {
      indexValue: "number",
      category: "speed",
      "rad/s": parseFloat(speed),
      radsColor: "hsl(130, 70%, 50%)",
    },
  ];

  return (
    <ResponsiveBar
      data={data}
      keys={["rad/s"]}
      indexBy="category"
      margin={{ top: 10, right: 60, bottom: 20, left: 60 }}
      padding={0.5}
      layout="horizontal"
      minValue={-1 * maxSpeed}
      maxValue={maxSpeed}
      colors="#80C3F3"
      borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
      axisTop={null}
      axisRight={null}
      axisLeft={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: 10,
      }}
      labelSkipWidth={1}
      labelSkipHeight={0}
      labelTextColor="black"
      // labelFormat={(e: any) => Math.round(e*100) / 100}
      labelFormat={(e) => Math.round((e as number) * 100) / 100}
      animate={true}
      motionStiffness={150}
      motionDamping={15}
    />
  );
};

export default SpeedGaugeNivo;
