import React from "react";
import { useSelector } from "react-redux";
import { Slider } from "antd";

/*
  The position slider shows the current position of the ninja steg motor, at this point it is not interactable and only displays
  the current position

*/
const PositionSlider = () => {

  const position = useSelector((state) => state.position);

  return (
    <Slider
      style={{flex:1}}
      defaultValue={position}
      value={position}
      disabled={true}
      min={-1000}
      max={1000}
      marks={{
        "-1000": "-1000",
        "-500": "-500",
        0: "0",
        500: "500",
        1000: "1000",
      }}
    />
  );
};

export default PositionSlider;
