import React, { useContext, useEffect, useRef } from 'react'
import { Descriptions } from 'antd'
import { CommunicationContext, MotorContext, ConnectionContext } from '../../../context/index'
import { useSelector } from 'react-redux'
import { useState } from 'react'

const DescriptionForm = () => {
  /*
    These variables are not rapidly updated and thus shared via useContext
  */
  const { sendCommand } = useContext(CommunicationContext)
  const { connectionType } = useContext(ConnectionContext)
  const { resistance, running } = useContext(MotorContext)

  /*  
    These "heartbeat-variables" are very rapidly updated and because of that, we import them via a Redux store instead of useContext

  */
  const position = useSelector((state) => state.position)
  const currentAlpha = useSelector((state) => state.currentAlpha)
  const currentBeta = useSelector((state) => state.currentBeta)
  const voltage = useSelector((state) => state.voltage)
  const current = useRef(0);
  const [dispCurr, setDispCurr] = useState("0");
  const [dispPow, setDispPow] = useState("0");

  useEffect(() => {
    current.current = Math.sqrt(currentAlpha ** 2 + currentBeta ** 2);
  }, [currentAlpha, currentBeta])

  useEffect(() => {
    setInterval(() => {
      updateVariables()
    }, 1000)

  }, [running])

  const updateVariables = () => {
    setDispCurr((current.current * 1000).toFixed(2))
    setDispPow(((current.current ** 2 * resistance) * 1000).toFixed(2))
  }

  useEffect(() => {
    sendCommand("get_res");
  }, [])

  return (
    // All items have css-class to right align them
    <Descriptions size="small" column={2} bordered>
      {/* here I just wrote a version number to have a value */}
      <Descriptions.Item
        label="Version"
        className="description-item-content-right-align"
      >
        1.01
      </Descriptions.Item>
      <Descriptions.Item
        label="Connection"
        className="description-item-content-right-align"
      >
        {connectionType}
      </Descriptions.Item>
      <Descriptions.Item
        label="Current"
        className="description-item-content-right-align"
      >
        {/* All number variables are rounded to two decimals */}
        {/* The current are multiplied with 1000 to show milliAmpere */}
        {/* {(current.current * 1000).toFixed(2)} mA */}
        {dispCurr} mA
      </Descriptions.Item>
      <Descriptions.Item
        label="Power"
        className="description-item-content-right-align"
      >
        {/* {((current.current ** 2 * resistance) * 1000).toFixed(2)} mW */}
        {dispPow} mW
      </Descriptions.Item>
      {/* adding a +-sign to input casting from string to number */}
      <Descriptions.Item
        label="Voltage"
        className="description-item-content-right-align"
      >
        {(+voltage).toFixed(2)} V
      </Descriptions.Item>
      <Descriptions.Item
        label="Position"
        className="description-item-content-right-align"
      >
        {(+position).toFixed(2)} rads
      </Descriptions.Item>
    </Descriptions>
  )
}

export default DescriptionForm
