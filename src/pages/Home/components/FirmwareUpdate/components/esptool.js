import struct from './struct.mjs';
import SparkMD5 from 'spark-md5'

const MAX_UINT32 = 0xffffffff
const MAX_UINT24 = 0xffffff

const DEFAULT_TIMEOUT = 3                   // timeout for most flash operations
const START_FLASH_TIMEOUT = 20              // timeout for starting flash (may perform erase)
const CHIP_ERASE_TIMEOUT = 120              // timeout for full chip erase
const MAX_TIMEOUT = CHIP_ERASE_TIMEOUT * 2  // longest any command can run
const SYNC_TIMEOUT = 0.1                    // timeout for syncing with bootloader
const MD5_TIMEOUT_PER_MB = 8                // timeout (per megabyte) for calculating md5sum
const ERASE_REGION_TIMEOUT_PER_MB = 30      // timeout (per megabyte) for erasing a region
const MEM_END_ROM_TIMEOUT = 0.05            // special short timeout for ESP_MEM_END, as it may never respond
const DEFAULT_SERIAL_WRITE_TIMEOUT = 10     // timeout for serial port write

const main = async (port, files) => {
    let esp = await ESPLoader.detect_chip(port, 115200)
    esp = await esp.run_stub()
    console.log("Configuring flash size...")
    let flash_size = await detect_flash_size(esp)
    await esp.flash_set_parameters(flash_size_bytes(flash_size))
    let args = {
        flash_size,
        after: "hard_reset",
        flash_freq: '40m',
        flash_mode: 'dio',
        operation: 'write_flash',
        addr_filename: files,
    }
    await write_flash(esp, args)
    console.log("Done")
    await esp.reader.releaseLock();
    await esp._port.close();
}

const DETECTED_FLASH_SIZES = {
    0x12: '256KB', 0x13: '512KB', 0x14: '1MB',
    0x15: '2MB', 0x16: '4MB', 0x17: '8MB', 0x18: '16MB'
}

class ESPLoader {
    static CHIP_NAME = "Espressif device"
    static IS_STUB = false

    static DEFAULT_PORT = "/dev/ttyUSB0"

    // Commands supported by ESP8266 ROM bootloader
    static ESP_FLASH_BEGIN = 0x02
    static ESP_FLASH_DATA = 0x03
    static ESP_FLASH_END = 0x04
    static ESP_MEM_BEGIN = 0x05
    static ESP_MEM_END = 0x06
    static ESP_MEM_DATA = 0x07
    static ESP_SYNC = 0x08
    static ESP_WRITE_REG = 0x09
    static ESP_READ_REG = 0x0a

    // Some comands supported by ESP32 ROM bootloader (or -8266 w/ stub)
    static ESP_SPI_SET_PARAMS = 0x0B
    static ESP_SPI_ATTACH = 0x0D
    static ESP_CHANGE_BAUDRATE = 0x0F
    static ESP_FLASH_DEFL_BEGIN = 0x10
    static ESP_FLASH_DEFL_DATA = 0x11
    static ESP_FLASH_DEFL_END = 0x12
    static ESP_SPI_FLASH_MD5 = 0x13

    // Some commands supported by stub only
    static ESP_ERASE_FLASH = 0xD0
    static ESP_ERASE_REGION = 0xD1
    static ESP_READ_FLASH = 0xD2
    static ESP_RUN_USER_CODE = 0xD3

    // Flash encryption debug more command
    static ESP_FLASH_ENCRYPT_DATA = 0xD4

    // Maximum block sized for RAM and Flash writes, respectively.
    static ESP_RAM_BLOCK = 0x1800

    static FLASH_WRITE_SIZE = 0x400

    // Default baudrate. The ROM auto-bauds, so we can use more or less whatever we want.
    static ESP_ROM_BAUD = 115200

    // First byte of the application image
    static ESP_IMAGE_MAGIC = 0xe9

    // Initial state for the checksum routine
    static ESP_CHECKSUM_MAGIC = 0xef

    // Flash sector size, minimum unit of erase.
    static FLASH_SECTOR_SIZE = 0x1000

    // This register happens to exist on both ESP8266 & ESP32
    static UART_DATA_REG_ADDR = 0x60000078

    static UART_CLKDIV_MASK = 0xFFFFF

    // Memory addresses
    static IROM_MAP_START = 0x40200000
    static IROM_MAP_END = 0x40300000

    // The number of bytes in the UART response that signify command status
    static STATUS_BYTES_LENGTH = 2

    isOpen = false

    constructor(port = this.DEFAULT_PORT, baud = this.ESP_ROM_BAUD, trace_enabled = false, reader) {
        this._port = port;
        this._trace_enabled = trace_enabled;
        this.reader = reader;
        if(this.reader)
            this._slip_reader = slip_reader(this._port, this.reader)
    }
    
    init = async (baud) => {
        await this._set_port_baudrate(baud)
        this._port.timeout = 3;
        try {
            this._port.write_timeout = DEFAULT_SERIAL_WRITE_TIMEOUT;
        }
        catch (e) {
            this._port.write_timeout = null;
        }
        return;
    }
    
    _set_port_baudrate = async (baud) => {
        try {
            await this._port.open({ baudRate: baud })
            this.isOpen = true;
            // this.openLineBreakStream();
            this.reader = await this._port.readable.getReader();
            this._slip_reader = slip_reader(this._port, this.reader)
        } catch (e) {
            if(e.name != "InvalidStateError")
                throw Error("Failed to set baud rate . The driver may not support this rate." + e)
        }
    }

    close = async () => {
        await this.reader.cancel()
        await this.reader.releaseLock();
        await this._port.close();
        this.reader = null;
    }

    static detect_chip = async (port = this.DEFAULT_PORT, baud = this.ESP_ROM_BAUD, connect_mode = 'default_reset', trace_enabled = false) => {
        let detect_port = new ESPLoader(port, baud, trace_enabled = trace_enabled)
        await detect_port.init(baud);
        await detect_port.connect(connect_mode)
        try {
            console.log('Detecting chip type...');
            let date_reg = await detect_port.read_reg(ESPLoader.UART_DATA_REG_ADDR)
            if (date_reg == ESP32ROM.DATE_REG_VALUE) {
                let inst = new ESP32ROM(detect_port._port, 115200, detect_port.reader);
                console.log(inst.CHIP_NAME)
                return inst
            }

        } finally {
            console.log("")
        }
        throw new Error("Unexpected UARD date code")
    }
    flush = () => {
        this._slip_reader = slip_reader(this._port, this.reader)
    }

    sync = async () => {
        await this.command(ESPLoader.ESP_SYNC, '\x07\x07\x12\x20' + 'UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU', undefined, undefined, SYNC_TIMEOUT);
        for (let i = 0; i < 7; i++) {
            await this.command();
        }
        return
    }

    _setDTR = async (state) => {
        await this._port.setSignals({ dataTerminalReady: state })
        this.dtr = state;
        return;
    }

    _setRTS = async (state) => {
        await this._port.setSignals({ requestToSend: state })
        return;
    }

    hard_reset = async () => {
        await this._setRTS(true)
        await new Promise(resolve => setTimeout(resolve, 100));
        await this._setRTS(false)
    }

    _connect_attempt = async (mode = 'default_reset', esp32r0_delay = false) => {
        let last_error = null

        if (mode == "no_reset_no_sync") {
            return last_error
        }

        if (mode != 'no_reset') {
            await this._setDTR(false)
            await this._setRTS(true)
            await new Promise(resolve => setTimeout(resolve, 100));
            if (esp32r0_delay) {
                await new Promise(resolve => setTimeout(resolve, 1200));
            }
            await this._setDTR(true);
            await this._setRTS(false);
            if (esp32r0_delay) {
                await new Promise(resolve => setTimeout(resolve, 400));
            }
            await new Promise(resolve => setTimeout(resolve, 50));
            await this._setDTR(false);
        }

        for (let index = 0; index < 5; index++) {
            try {
                this.flush();
                await this.sync()
                return null;
            } catch (error) {
                if (esp32r0_delay) {
                    console.log('_')
                }
                else {
                    console.log('_');
                }
                await new Promise((resolve) => setTimeout(resolve, 50))
                last_error = error;
            }
        }
        return last_error;
    }

    connect = async (mode = 'default_reset') => {
        console.log('Connecting...');
        let last_error = null;

        try {
            for (let i = 0; i < 7; i++) {
                last_error = await this._connect_attempt(mode = mode, false);
                if (last_error == null) {
                    return
                }
                last_error = await this._connect_attempt(mode = mode, true);
                if (last_error == null) {
                    return
                }
            }
        } finally {
            console.log('');
        }
        throw new Error('Failed to connect');
    }

    read_reg = async (addr) => {
        let s = struct("<I")
        let { val, data } = await this.command(ESPLoader.ESP_READ_REG, [...new Uint8Array(s.pack(addr))])
        if (data[0] != 0) {
            throw new Error("failed to read register addres " + addr + " " + data)
        }
        return val
    }

    write_reg = async (addr, value, mask = 0xFFFFFFFF, delay_us = 0) => {
        //  Write to memory address in target
        // Note: mask option is not supported by stub loaders, use update_reg() function.
        let s = struct('<IIII')
        return await this.check_command("write target memory", ESPLoader.ESP_WRITE_REG, new Uint8Array(s.pack(addr, value, mask, delay_us)))

    }

    static checksum = (data, state = ESPLoader.ESP_CHECKSUM_MAGIC) => {
        for (let b of data) {
            state ^= b;
        }
        return state;
    }

    command = async (op = null, data = "", chk = 0, wait_response = true, timeout = DEFAULT_TIMEOUT) => {

        try {
            if (op != null) {
                let s = struct("<BBHI")
                let pkt = new Uint8Array(s.pack(0x00, op, data.length, chk));
                await this.write([...pkt, Array.from(data)]);
            }

            if (!wait_response) {
                return;
            }

            for (let i = 0; i < 100; i++) {
                let { value, done } = await this.read()
                let p = value;
                if (p.length < 8) {
                    continue
                }
                let s = struct("<BBHI");
                let t = Uint8Array.from(p.slice(0, 8))
                let [resp, op_ret, len_ret, val] = s.unpack(t.buffer)
                if (resp != 1) {
                    continue;
                }
                data = p.slice(8, p.length);
                if (op == null || op_ret == op) {
                    return { val, data };
                }
            }
        } finally {
            // if (new_timeout != saved_timeout)
            //     self._port.timeout = saved_timeout
        }

        throw new Error("Response doesn't match request")
    }

    check_command = async (op_description, op = null, dat = "", chk = 0, timeout = DEFAULT_TIMEOUT) => {

        // Execute a command with 'command', check the result code and throw an appropriate
        // FatalError if it fails.

        // Returns the "result" of a successful command.
        let { val, data } = await this.command(op, dat, chk, undefined, timeout)
        if (data.length < this.STATUS_BYTES_LENGTH) {
            throw new Error(`Failed to ${op_description}. Only got ${data.length} byte status response.`)
        }
        let status_bytes = data.slice(-this.STATUS_BYTES_LENGTH);
        if (status_bytes[0] != 0) {
            throw new Error(`Failed to ${op_description} ${status_bytes}`)
        }

        if (data.length > this.STATUS_BYTES_LENGTH)
            return data.slice(0, -this.STATUS_BYTES_LENGTH);
        else
            return val
    }

    read = async () => {
        let next = await this._slip_reader.next();
        return next;
    }

    write = async (packet) => {
        let length = packet.length;
        if (Array.isArray((packet[length - 1]))) {
            let toBeSpread = packet.pop();
            packet = [...packet, ...toBeSpread]
        }
        let newPacket = byteFormat(packet);
        let buf = ['\xc0', ...(newPacket.map((value) => {
            return value === '\xdb' || value === '\xdb'.charCodeAt(0) ? '\xdb\xdd' : value === '\xc0' || value === '\xc0'.charCodeAt(0) ? '\xdb\xdc' : value;
        })), '\xc0']
        let arr = Uint8Array.from(byteFormat(buf))
        await this.send(arr)
    }

    send = async (packet) => {
        let writer = this._port.writable.getWriter();
        writer.write(packet);
        await writer.releaseLock();
        return
    }

    mem_begin = async (size, blocks, blocksize, offset) => {
        if (this.IS_STUB) {
            let stub = this.STUB_CODE;
            let load_start = offset;
            let load_end = offset + size;
            for (let item of [(stub["data_start"], stub["data_start"] + stub["data"].length), (stub["text_start"], stub["text_start"] + stub["text"].length)]) {
                let [start, end] = item;
                if (load_start < end && load_end > start) {
                    throw new Error((`Software loader is resident at ${start}-${end}.  
                                      Can't load binary at overlapping address range ${load_start}-${load_end}. 
                                      Either change binary loading address, or use the --no-stub 
                                      option to disable the software loader.`))
                }
            }
        }
        let s = struct('<IIII')
        return await this.check_command("enter RAM download mode", ESPLoader.ESP_MEM_BEGIN, new Uint8Array(s.pack(size, blocks, blocksize, offset)));
    }

    mem_block = async (data, seq) => {
        let s = struct('<IIII')
        let dat = [...new Uint8Array(s.pack(data.length, seq, 0, 0)), ...data]
        return await this.check_command("write to target RAM", ESPLoader.ESP_MEM_DATA, dat, ESPLoader.checksum(data));
    }

    mem_finish = async (entrypoint = 0) => {
        // Sending ESP_MEM_END usually sends a correct response back, however sometimes
        // (with ROM loader) the executed code may reset the UART or change the baud rate
        // before the transmit FIFO is empty. So in these cases we set a short timeout and
        // ignore errors.
        let timeout = this.IS_STUB ? DEFAULT_TIMEOUT : MEM_END_ROM_TIMEOUT;
        let s = struct('<II')
        let data = new Uint8Array(s.pack((entrypoint == 0 ? 1 : 0), entrypoint))
        try {
            return await this.check_command("leave RAM download mode", ESPLoader.ESP_MEM_END, data, undefined, timeout)
        }
        catch (e) {
            if (this.IS_STUB)
                throw e
        }
    }

    run_stub = async (stub = null) => {
        if (stub == null) {
            if (this.IS_STUB) {
                throw new Error("Not possible for a stub to load another stub (memory likely to overlap.)")
            }
            stub = this.STUB_CODE
        }

        // Upload
        console.log("Uploading stub...")
        for (let field of ["text", "data"]) {
            if (stub[field]) {
                let offs = stub[field + "_start"];
                let length = stub[field].length;
                let blocks = Math.floor((length + ESPLoader.ESP_RAM_BLOCK - 1) / ESPLoader.ESP_RAM_BLOCK);
                await this.mem_begin(length, blocks, ESPLoader.ESP_RAM_BLOCK, offs)
                for (let seq = 0; seq < blocks; seq++) {
                    let from_offs = seq * ESPLoader.ESP_RAM_BLOCK;
                    let to_offs = from_offs + ESPLoader.ESP_RAM_BLOCK;
                    await this.mem_block(stub[field].slice(from_offs, to_offs), seq);
                }
            }
        }
        console.log("Running stub...");
        await this.mem_finish(stub['entry']);

        let p = await this.read();
        if (p.value.join() != "79,72,65,73")
            throw Error(`Failed to start stub. Unexpected response: ${p}`)
        console.log("Stub running...");
        return new ESP32StubLoader(this);
    }

    flash_id = async () => {
        const SPIFLASH_RDID = 0x9F
        return await this.run_spiflash_command(SPIFLASH_RDID, [], 24)
    }

    parse_flash_size_arg = (arg) => {
        return this.FLASH_SIZES[arg]
    }

    flash_set_parameters = async (size) => {
        // Tell the ESP bootloader the parameters of the chip

        // Corresponds to the "flashchip" data structure that the ROM
        // has in RAM.

        // 'size' is in bytes.

        // All other flash parameters are currently hardcoded (on ESP8266
        // these are mostly ignored by ROM code, on ESP32 I'm not sure.)
        let fl_id = 0
        let total_size = size
        let block_size = 64 * 1024
        let sector_size = 4 * 1024
        let page_size = 256
        let status_mask = 0xffff
        let s = struct('<IIIIII')
        await this.check_command("set SPI params", ESP32ROM.ESP_SPI_SET_PARAMS,
            new Uint8Array(s.pack(fl_id, total_size, block_size, sector_size, page_size, status_mask)))

    }

    flash_begin = async (size, offset) => {
        let num_blocks = Math.floor((size + this.FLASH_WRITE_SIZE - 1) / this.FLASH_WRITE_SIZE);
        let erase_size = this.get_erase_size(offset, size)
        let s = struct('<IIII')
        await this.check_command("enter Flash download mode", ESPLoader.ESP_FLASH_BEGIN, new Uint8Array(s.pack(erase_size, num_blocks, this.FLASH_WRITE_SIZE, offset)));

        return num_blocks;
    }

    flash_block = async (data, seq) => {
        let s = struct("<IIII")
        await this.check_command(`write to target Flash after seq ${seq}`, ESPLoader.ESP_FLASH_DATA, [...new Uint8Array(s.pack(data.length, seq, 0, 0)), ...data], ESPLoader.checksum(data))
    }

    flash_finish = async (reboot) => {
        let s = struct("<I");
        let pkt = new Uint8Array(s.pack(!reboot ? 1 : 0))
        // stub sends a reply to this command
        await this.check_command("leave Flash mode", ESPLoader.ESP_FLASH_END, pkt)
    }

    flash_md5sum = async (addr, size) => {
        let s = struct("<IIII")
        let res = await this.check_command('calculate md5sum', ESPLoader.ESP_SPI_FLASH_MD5, new Uint8Array(s.pack(addr, size, 0, 0)))
        let decoder = new TextDecoder()
        if(res.length == 32)
            return decoder.decode(new Uint8Array(res))
        else if(res.length == 16)
            return res.map((num) => {
                let hexed = num.toString(16);
                return hexed.length == 1 ? "0" + hexed : hexed
            }).join("")
        else
            throw Error("MD5Sum command returned unexpected result")
    }

    run_spiflash_command = async (spiflash_command, data = [], read_bits = 0) => {
        // Run an arbitrary SPI flash command.

        // This function uses the "USR_COMMAND" functionality in the ESP
        // SPI hardware, rather than the precanned commands supported by
        // hardware. So the value of spiflash_command is an actual command
        // byte, sent over the wire.

        // After writing command byte, writes 'data' to MOSI and then
        // reads back 'read_bits' of reply on MISO. Result is a number.

        // SPI_USR register flags
        const SPI_USR_COMMAND = (1 << 31)
        const SPI_USR_MISO = (1 << 28)
        const SPI_USR_MOSI = (1 << 27)

        // SPI registers, base address differs ESP32 vs 8266
        let base = this.SPI_REG_BASE
        const SPI_CMD_REG = base + 0x00
        const SPI_USR_REG = base + 0x1C
        const SPI_USR1_REG = base + 0x20
        const SPI_USR2_REG = base + 0x24
        const SPI_W0_REG = base + this.SPI_W0_OFFS

        let set_data_lengths;

        // following two registers are ESP32 only
        if (this.SPI_HAS_MOSI_DLEN_REG) {
            // ESP32 has a more sophisticated wayto set up "user" commands
            set_data_lengths = async (mosi_bits, miso_bits) => {
                const SPI_MOSI_DLEN_REG = base + 0x28
                const SPI_MISO_DLEN_REG = base + 0x2C
                if (mosi_bits > 0)
                    await this.write_reg(SPI_MOSI_DLEN_REG, mosi_bits - 1)
                if (miso_bits > 0)
                    await this.write_reg(SPI_MISO_DLEN_REG, miso_bits - 1)
            }
        }
        else {

            set_data_lengths = async (mosi_bits, miso_bits) => {
                const SPI_DATA_LEN_REG = SPI_USR1_REG
                const SPI_MOSI_BITLEN_S = 17
                const SPI_MISO_BITLEN_S = 8
                let mosi_mask = (mosi_bits == 0) ? 0 : (mosi_bits - 1)
                let miso_mask = (miso_bits == 0) ? 0 : (miso_bits - 1)
                await this.write_reg(SPI_DATA_LEN_REG,
                    (miso_mask << SPI_MISO_BITLEN_S) | (
                        mosi_mask << SPI_MOSI_BITLEN_S))
            }
        }
        // SPI peripheral "command" bitmasks for SPI_CMD_REG
        const SPI_CMD_USR = (1 << 18)

        // shift values
        const SPI_USR2_DLEN_SHIFT = 28

        if (read_bits > 32)
            throw Error("Reading more than 32 bits back from a SPI flash operation is unsupported")
        if (data.length > 64)
            throw Error("Writing more than 64 bytes of data with one SPI command is unsupported")

        let data_bits = data.length * 8
        let old_spi_usr = await this.read_reg(SPI_USR_REG)
        let old_spi_usr2 = await this.read_reg(SPI_USR2_REG)
        let flags = SPI_USR_COMMAND
        if (read_bits > 0)
            flags |= SPI_USR_MISO
        if (data_bits > 0)
            flags |= SPI_USR_MOSI
        await set_data_lengths(data_bits, read_bits)
        await this.write_reg(SPI_USR_REG, flags)
        await this.write_reg(SPI_USR2_REG,
            (7 << SPI_USR2_DLEN_SHIFT) | spiflash_command)
        if (data_bits == 0)
            await this.write_reg(SPI_W0_REG, 0)  // clear data register before we read it
        else {
            data = pad_to(data, 4, 0)  // pad to 32-bit multiple
            let tempString = ""
            for (let i = 0; i < Math.floor(data.length / 4); i++) { tempString += "I" }
            let s = struct(tempString)
            let words = s.unpack(data)
            let next_reg = SPI_W0_REG
            for (let word of words)
                await this.write_reg(next_reg, word)
            next_reg += 4
        }
        await this.write_reg(SPI_CMD_REG, SPI_CMD_USR)

        const wait_done = async () => {
            for (let i = 0; i < 10; i++) {
                let reg = await this.read_reg(SPI_CMD_REG)
                if ((reg & SPI_CMD_USR) == 0) {
                    return
                }
            }
            throw Error("SPI command did not complete in time")
        }
        await wait_done()

        let status = await this.read_reg(SPI_W0_REG)
        // restore some SPI controller registers
        await this.write_reg(SPI_USR_REG, old_spi_usr)
        await this.write_reg(SPI_USR2_REG, old_spi_usr2)
        return status

    }
}

const byteFormat = (str, arr = true) => {
    let newArr = [];
    for (const el of str) {
        if (el === ',') { continue; }
        if (!isNaN(+el)) {
            newArr.push(String.fromCharCode(el).charCodeAt(0))
        }
        else {
            for (let i = 0; i < el.length; i++) {
                newArr.push(el[i].charCodeAt(0))
            }
        }
    }

    return newArr;
}

const detect_flash_size = async (esp) => {
    let flash_id = await esp.flash_id()
    let size_id = flash_id >> 16
    let flash_size;
    flash_size = DETECTED_FLASH_SIZES[size_id];
    if (!flash_size) {
        console.log(`Warning: Could not auto-detext Flash size (FlashID=0x${flash_id}, SizeID=0x${size_id}), defaulting to 4MB`)
        flash_size = '4MB'
    }
    else
        console.log('Auto-detected Flash size:', flash_size)

    return flash_size
}

const pad_to = (data, alignment, pad_character = 255) => {
    let pad_mod = data.length % alignment;
    if (pad_mod != 0) {
        for (let i = 0; i < (alignment - pad_mod); i++)
            data.push(pad_character);
    }
    return data;
}

const flash_size_bytes = (size) => {
    //  Given a flash size of the type passed in args.flash_size
    // (ie 512KB or 1MB) then return the size in bytes.
    if (size.includes("MB"))
        return +size.slice(0, size.indexOf("MB")) * 1024 * 1024
    else if (size.includes("KB"))
        return +size.slice(0, size.indexOf("KB")) * 1024
    else
        throw Error("Unknown size " + size)

}

const _update_image_flash_params = (esp, address, args, image) => {
    if (image.length < 8)
        return image; // not long enough to be a bootloader image

    // unpack the (potential) image header
    let s = struct("BBBB")
    let [magic, _, flash_mode, flash_size_freq] = s.unpack(new Uint8Array(image.slice(0, 4)).buffer)
    if (address != esp.BOOTLOADER_FLASH_OFFSET)
        return image // not flashing bootloader offset, so dont't modify this

    if (magic != ESPLoader.ESP_IMAGE_MAGIC) {
        console.log(`Warning: Image file at 0x${address} doesn't look like an image file, so not changing any flash settings`)
        return image;
    }

    // make sure this really is an image, and not just data that
    // starts with esp.ESP_IMAGE_MAGIC (mostly a problem for encrypted
    // images that happen to start with a magic byte)

    flash_mode = 2;
    let flash_freq = 0;
    let flash_size = esp.parse_flash_size_arg(args.flash_size)

    s = struct("BB")
    let flash_params = Array.from(new Uint8Array(s.pack(flash_mode, flash_size + flash_freq)))
    if (flash_params.join() != image.slice(2, 4).join()) {
        console.log('Flash params set')
        image = [...image.slice(0, 2), ...flash_params, ...image.slice(4)]
    }

    return image;
}

const write_flash = async (esp, args) => {
    for (let files of args.addr_filename) {
        let { address, argfile } = files;
        let bytes = await readFile(argfile);
        let image = pad_to(bytes, 4);

        if (image.length == 0) {
            console.log(`Warning: File ${argfile.name} is empty`);
            continue;
        }
        image = _update_image_flash_params(esp, address, args, image)
        let calcmd5 = SparkMD5.ArrayBuffer.hash((new Uint8Array(image)))
        let uncsize = image.length

        let ratio = 1.0
        let blocks = await esp.flash_begin(uncsize, address)
        let seq = 0;
        let written = 0;
        while(image.length > 0){
            console.log(`Writing at 0x${(address + seq * esp.FLASH_WRITE_SIZE).toString(16)}... (${Math.floor(100 * (seq + 1) / blocks)} %)`)
            let block = image.slice(0, esp.FLASH_WRITE_SIZE)
            let ffArr = []
            for(let i = 0; i < esp.FLASH_WRITE_SIZE - block.length; i++) {ffArr.push(255)}
            block = [...block, ...ffArr];
            await esp.flash_block(block, seq)
            
            image = image.slice(esp.FLASH_WRITE_SIZE)
            seq += 1
            written += block.length
        }

        if(!args.encrypt){
            try{
                let res = await esp.flash_md5sum(address, uncsize);
                if(res != calcmd5)
                    throw Error("MD5 of file does not match data in flash!");
                else
                    console.log("Hash of data verified")
            } catch(e){console.log(e)}

        }

        if(esp.IS_STUB){
            await esp.flash_begin(0, 0);
            await esp.flash_finish(false)
        }

        console.log("\nLeaving...");
    }
}

const readFile = async (file) => {
    let read = await file.arrayBuffer();
    return Array.from(new Uint8Array(read))
}

async function* slip_reader(port, reader) {
    let partial_packet = null;
    let in_escape = false;
    let decoder = new TextDecoder();
    while (true) {
        let { value, done } = await reader.read();
        let read_bytes = value;
        if (read_bytes == undefined) {
            throw new Error("Timed out waiting for packet");
        }

        for (const b of read_bytes) {
            if (partial_packet == null) {
                if (b == '\xc0'.charCodeAt(0)) {
                    partial_packet = []
                }
                else {
                    throw new Error("'Invalid head of packet");
                }
            }
            else if (in_escape) {
                in_escape = false;
                if (b == '\xdc'.charCodeAt(0)) {
                    partial_packet.push('\xc0'.charCodeAt(0))
                }
                else if (b == '\xdd'.charCodeAt(0)) {
                    partial_packet.push('\xdb'.charCodeAt(0))
                }
                else {
                    throw new Error('Invalid SLIP escape')
                }
            }
            else if (b == '\xdb'.charCodeAt(0)) {
                in_escape = true
            }
            else if (b == '\xc0'.charCodeAt(0)) {
                yield partial_packet
                partial_packet = null
            }
            else {
                partial_packet.push(b)
            }
        }
    }
}

class ESP32ROM extends ESPLoader {
    ///Access class for ESP32 ROM bootloader

    CHIP_NAME = "ESP32"
    IMAGE_CHIP_ID = 0
    IS_STUB = false

    static DATE_REG_VALUE = 0x15122500

    IROM_MAP_START = 0x400d0000
    IROM_MAP_END = 0x40400000
    DROM_MAP_START = 0x3F400000
    DROM_MAP_END = 0x3F800000

    // ESP32 uses a 4 byte status reply
    STATUS_BYTES_LENGTH = 4

    SPI_REG_BASE = 0x60002000
    EFUSE_REG_BASE = 0x6001a000

    DR_REG_SYSCON_BASE = 0x3ff66000

    SPI_W0_OFFS = 0x80
    SPI_HAS_MOSI_DLEN_REG = true

    UART_CLKDIV_REG = 0x3ff40014

    XTAL_CLK_DIVIDER = 1

    FLASH_SIZES = {
        '1MB': 0x00,
        '2MB': 0x10,
        '4MB': 0x20,
        '8MB': 0x30,
        '16MB': 0x40
    }

    BOOTLOADER_FLASH_OFFSET = 0x1000

    OVERRIDE_VDDSDIO_CHOICES = ["1.8V", "1.9V", "OFF"]

    MEMORY_MAP = [[0x3F400000, 0x3F800000, "DROM"],
    [0x3F800000, 0x3FC00000, "EXTRAM_DATA"],
    [0x3FF80000, 0x3FF82000, "RTC_DRAM"],
    [0x3FF90000, 0x40000000, "BYTE_ACCESSIBLE"],
    [0x3FFAE000, 0x40000000, "DRAM"],
    [0x3FFAE000, 0x40000000, "DMA"],
    [0x3FFE0000, 0x3FFFFFFC, "DIRAM_DRAM"],
    [0x40000000, 0x40070000, "IROM"],
    [0x40070000, 0x40078000, "CACHE_PRO"],
    [0x40078000, 0x40080000, "CACHE_APP"],
    [0x40080000, 0x400A0000, "IRAM"],
    [0x400A0000, 0x400BFFFC, "DIRAM_IRAM"],
    [0x400C0000, 0x400C2000, "RTC_IRAM"],
    [0x400D0000, 0x40400000, "IROM"],
    [0x50000000, 0x50002000, "RTC_DATA"]]

    STUB_CODE = {
        "text": [248, 32, 244, 63, 248, 48, 244, 63, 54, 65, 0, 145, 253, 255, 192, 32, 0, 136, 9, 128, 128, 36, 86, 72, 255, 145, 250, 255, 192, 32, 0, 136, 9, 128, 128, 36, 86, 72, 255, 29, 240, 0, 0, 0, 16, 32, 244, 63, 0, 32, 244, 63, 0, 0, 0, 8, 54, 65, 0, 229, 252, 255, 129, 251, 255, 12, 2, 192, 32, 0, 41, 8, 145, 250, 255, 33, 250, 255, 192, 32, 0, 34, 105, 0, 192, 32, 0, 40, 9, 86, 114, 255, 192, 32, 0, 136, 8, 12, 18, 128, 128, 4, 32, 40, 48, 29, 240, 0, 0, 0, 0, 64, 54, 65, 0, 101, 252, 255, 22, 154, 255, 129, 237, 255, 145, 252, 255, 192, 32, 0, 153, 8, 192, 32, 0, 152, 8, 86, 121, 255, 29, 240, 0, 0, 0, 0, 0, 1, 0, 0, 128, 0, 152, 192, 255, 63, 255, 255, 255, 0, 4, 32, 244, 63, 54, 65, 0, 33, 252, 255, 50, 34, 4, 22, 67, 5, 101, 248, 255, 22, 234, 4, 165, 251, 255, 88, 66, 12, 248, 12, 19, 65, 244, 255, 87, 168, 11, 88, 34, 128, 85, 16, 204, 53, 65, 242, 255, 28, 3, 136, 34, 64, 88, 17, 37, 243, 255, 129, 240, 255, 128, 133, 16, 81, 240, 255, 192, 32, 0, 137, 5, 129, 210, 255, 192, 32, 0, 66, 104, 0, 192, 32, 0, 72, 8, 86, 116, 255, 136, 66, 72, 34, 48, 136, 192, 58, 52, 137, 66, 57, 34, 29, 240, 0, 8, 0, 244, 63, 28, 0, 244, 63, 0, 0, 244, 63, 144, 192, 255, 63, 8, 64, 255, 63, 128, 128, 0, 0, 132, 128, 0, 0, 64, 64, 0, 0, 72, 128, 255, 63, 16, 0, 244, 63, 148, 192, 255, 63, 54, 65, 0, 33, 244, 255, 49, 247, 255, 65, 247, 255, 192, 32, 0, 88, 2, 74, 67, 97, 241, 255, 192, 32, 0, 40, 6, 32, 32, 116, 22, 226, 9, 198, 35, 0, 97, 238, 255, 177, 238, 255, 192, 32, 0, 162, 38, 0, 160, 160, 116, 37, 176, 0, 150, 202, 4, 145, 234, 255, 129, 235, 255, 177, 235, 255, 128, 137, 128, 176, 185, 128, 192, 32, 0, 200, 8, 146, 27, 0, 160, 160, 116, 144, 128, 244, 27, 152, 144, 144, 244, 192, 32, 0, 146, 91, 0, 138, 140, 192, 32, 0, 162, 72, 0, 130, 27, 0, 145, 225, 255, 128, 128, 244, 151, 152, 62, 192, 32, 0, 168, 4, 129, 224, 255, 145, 221, 255, 55, 154, 25, 70, 2, 0, 124, 232, 135, 26, 233, 70, 9, 0, 0, 0, 192, 32, 0, 57, 8, 192, 32, 0, 153, 4, 70, 2, 0, 192, 32, 0, 153, 8, 192, 32, 0, 57, 4, 129, 209, 255, 12, 9, 138, 131, 192, 32, 0, 146, 88, 0, 11, 34, 38, 2, 2, 198, 217, 255, 198, 212, 255, 0, 33, 206, 255, 192, 32, 0, 89, 2, 29, 240, 0, 0, 80, 45, 6, 64, 54, 65, 0, 65, 164, 255, 88, 52, 48, 53, 99, 22, 227, 3, 88, 20, 90, 83, 80, 92, 65, 134, 0, 0, 101, 232, 255, 136, 68, 166, 24, 4, 136, 36, 135, 165, 242, 229, 224, 255, 22, 154, 255, 168, 20, 48, 195, 32, 32, 178, 32, 129, 242, 255, 224, 8, 0, 140, 58, 34, 160, 196, 41, 84, 88, 20, 58, 85, 89, 20, 88, 52, 48, 53, 192, 57, 52, 29, 240, 0, 8, 32, 244, 63, 0, 0, 64, 0, 112, 226, 250, 63, 72, 36, 6, 64, 240, 34, 6, 64, 54, 97, 0, 229, 217, 255, 173, 1, 129, 252, 255, 224, 8, 0, 61, 10, 12, 18, 236, 234, 152, 1, 130, 162, 0, 128, 137, 16, 137, 1, 165, 222, 255, 145, 242, 255, 129, 243, 255, 192, 32, 0, 168, 9, 128, 138, 32, 192, 32, 0, 130, 105, 0, 178, 33, 0, 161, 239, 255, 129, 240, 255, 224, 8, 0, 160, 35, 131, 29, 240, 0, 0, 255, 15, 0, 0, 54, 65, 0, 161, 120, 255, 145, 253, 255, 130, 160, 1, 130, 74, 0, 50, 106, 1, 48, 140, 65, 34, 106, 3, 48, 48, 180, 154, 34, 137, 42, 42, 131, 128, 140, 65, 12, 2, 137, 74, 41, 90, 165, 248, 255, 45, 10, 50, 160, 197, 160, 35, 147, 29, 240, 0, 44, 146, 0, 64, 54, 65, 0, 130, 160, 192, 173, 2, 135, 146, 14, 162, 160, 219, 129, 251, 255, 224, 8, 0, 162, 160, 220, 134, 3, 0, 130, 160, 219, 135, 146, 8, 129, 247, 255, 224, 8, 0, 162, 160, 221, 129, 244, 255, 224, 8, 0, 29, 240, 0, 0, 0, 54, 65, 0, 58, 50, 6, 2, 0, 0, 162, 2, 0, 27, 34, 229, 251, 255, 55, 146, 244, 29, 240, 0, 0, 0, 16, 0, 0, 88, 16, 0, 0, 124, 218, 5, 64, 216, 46, 6, 64, 156, 218, 5, 64, 28, 219, 5, 64, 54, 33, 33, 162, 209, 16, 129, 250, 255, 224, 8, 0, 134, 9, 0, 0, 81, 246, 255, 189, 1, 80, 67, 99, 205, 4, 173, 2, 129, 246, 255, 224, 8, 0, 236, 250, 205, 4, 189, 1, 162, 209, 16, 129, 243, 255, 224, 8, 0, 74, 34, 64, 51, 192, 86, 99, 253, 161, 236, 255, 178, 209, 16, 26, 170, 129, 238, 255, 224, 8, 0, 161, 233, 255, 28, 11, 26, 170, 37, 248, 255, 45, 3, 29, 240, 34, 160, 99, 29, 240, 0, 0, 54, 65, 0, 162, 160, 192, 129, 205, 255, 224, 8, 0, 29, 240, 0, 0, 104, 16, 0, 0, 112, 16, 0, 0, 116, 16, 0, 0, 120, 16, 0, 0, 252, 103, 0, 64, 164, 146, 0, 64, 8, 104, 0, 64, 54, 65, 33, 97, 249, 255, 129, 249, 255, 26, 102, 26, 136, 73, 6, 114, 209, 16, 12, 6, 44, 10, 89, 8, 98, 103, 26, 129, 246, 255, 224, 8, 0, 129, 207, 255, 71, 184, 2, 70, 54, 0, 173, 7, 129, 207, 255, 224, 8, 0, 77, 6, 81, 239, 255, 97, 203, 255, 26, 85, 106, 129, 137, 5, 6, 45, 0, 0, 129, 233, 255, 64, 99, 192, 26, 136, 136, 8, 189, 1, 96, 104, 99, 205, 6, 32, 162, 32, 129, 197, 255, 224, 8, 0, 140, 234, 145, 225, 255, 12, 5, 82, 103, 22, 109, 5, 154, 81, 70, 13, 0, 0, 37, 246, 255, 96, 182, 32, 173, 1, 229, 236, 255, 165, 245, 255, 205, 6, 16, 177, 32, 112, 167, 32, 129, 186, 255, 224, 8, 0, 106, 34, 106, 68, 55, 180, 204, 129, 214, 255, 80, 100, 192, 26, 136, 136, 8, 135, 54, 163, 6, 239, 255, 0, 0, 129, 212, 255, 224, 8, 0, 129, 209, 255, 16, 136, 128, 178, 40, 0, 101, 125, 0, 247, 234, 13, 246, 70, 10, 96, 181, 128, 162, 75, 0, 27, 102, 6, 247, 255, 124, 235, 183, 154, 215, 38, 70, 39, 161, 165, 255, 112, 183, 32, 26, 170, 129, 167, 255, 224, 8, 0, 101, 239, 255, 161, 160, 255, 28, 11, 26, 170, 229, 229, 255, 165, 238, 255, 44, 10, 129, 195, 255, 224, 8, 0, 29, 240, 82, 39, 26, 55, 181, 209, 87, 180, 142, 198, 242, 255, 29, 240, 0, 0, 0, 192, 254, 63, 79, 72, 65, 73, 164, 235, 255, 63, 52, 241, 9, 64, 12, 0, 244, 63, 56, 64, 244, 63, 255, 255, 0, 0, 0, 0, 1, 0, 140, 128, 0, 0, 16, 64, 0, 0, 0, 64, 0, 0, 0, 192, 254, 63, 4, 192, 254, 63, 16, 39, 0, 0, 20, 0, 244, 63, 255, 255, 15, 0, 164, 235, 255, 63, 8, 192, 254, 63, 176, 192, 255, 63, 124, 104, 0, 64, 236, 103, 0, 64, 88, 134, 0, 64, 108, 42, 6, 64, 56, 50, 6, 64, 204, 44, 6, 64, 76, 44, 6, 64, 52, 133, 0, 64, 204, 144, 0, 64, 120, 46, 6, 64, 48, 239, 5, 64, 88, 146, 0, 64, 76, 130, 0, 64, 20, 44, 6, 64, 54, 193, 0, 33, 223, 255, 12, 10, 34, 97, 8, 66, 160, 0, 129, 238, 255, 224, 8, 0, 33, 218, 255, 49, 219, 255, 6, 1, 0, 66, 98, 0, 75, 34, 55, 50, 247, 37, 226, 255, 12, 75, 162, 193, 32, 229, 216, 255, 101, 225, 255, 65, 223, 254, 33, 223, 254, 177, 211, 255, 12, 12, 42, 36, 12, 90, 192, 32, 0, 73, 2, 129, 225, 255, 224, 8, 0, 49, 207, 255, 34, 161, 1, 192, 32, 0, 88, 3, 44, 10, 32, 37, 32, 192, 32, 0, 41, 3, 129, 131, 255, 224, 8, 0, 129, 218, 255, 224, 8, 0, 33, 200, 255, 192, 32, 0, 56, 2, 204, 186, 28, 194, 32, 35, 16, 34, 194, 248, 12, 19, 32, 163, 131, 12, 11, 129, 211, 255, 224, 8, 0, 241, 193, 255, 209, 81, 255, 193, 193, 255, 177, 164, 254, 226, 161, 0, 12, 10, 129, 206, 255, 224, 8, 0, 49, 163, 254, 82, 211, 43, 65, 193, 254, 97, 187, 255, 106, 36, 192, 32, 0, 104, 2, 22, 118, 255, 192, 32, 0, 120, 2, 12, 6, 192, 32, 0, 105, 2, 12, 18, 34, 65, 16, 34, 7, 1, 12, 40, 34, 65, 17, 130, 81, 9, 105, 81, 38, 146, 8, 28, 54, 103, 18, 31, 70, 8, 0, 0, 34, 7, 3, 98, 7, 2, 128, 34, 17, 96, 34, 32, 102, 66, 17, 40, 39, 192, 32, 0, 40, 2, 41, 81, 70, 1, 0, 0, 28, 34, 34, 81, 9, 37, 212, 255, 12, 139, 162, 193, 16, 229, 202, 255, 34, 7, 3, 130, 7, 2, 128, 34, 17, 128, 130, 32, 33, 161, 255, 135, 178, 17, 162, 160, 192, 37, 198, 255, 162, 160, 238, 229, 197, 255, 165, 209, 255, 70, 221, 255, 98, 7, 1, 12, 210, 39, 150, 2, 6, 147, 0, 103, 50, 78, 102, 102, 2, 198, 177, 0, 246, 118, 32, 102, 54, 2, 198, 103, 0, 246, 70, 8, 102, 38, 2, 198, 77, 0, 70, 176, 0, 102, 70, 2, 6, 125, 0, 102, 86, 2, 134, 144, 0, 134, 172, 0, 12, 146, 39, 150, 2, 6, 136, 0, 103, 50, 8, 102, 118, 2, 198, 141, 0, 134, 167, 0, 102, 150, 2, 198, 133, 0, 12, 178, 39, 150, 2, 70, 123, 0, 70, 163, 0, 28, 50, 39, 150, 2, 198, 59, 0, 103, 50, 40, 102, 182, 2, 198, 69, 0, 28, 2, 103, 50, 10, 12, 242, 39, 150, 2, 6, 48, 0, 6, 155, 0, 28, 18, 39, 150, 2, 134, 77, 0, 28, 34, 39, 150, 2, 70, 101, 0, 70, 150, 0, 34, 160, 210, 39, 150, 2, 70, 44, 0, 103, 50, 14, 34, 160, 208, 39, 22, 29, 34, 160, 209, 39, 22, 35, 134, 143, 0, 34, 160, 211, 39, 150, 2, 134, 68, 1, 34, 160, 212, 39, 150, 2, 134, 63, 0, 70, 138, 0, 12, 18, 22, 88, 69, 173, 2, 45, 10, 134, 133, 0, 38, 136, 2, 134, 131, 0, 198, 21, 1, 0, 0, 37, 175, 255, 96, 70, 32, 96, 34, 128, 22, 42, 1, 134, 126, 0, 0, 160, 172, 65, 129, 111, 255, 224, 8, 0, 86, 218, 30, 66, 212, 240, 64, 162, 192, 204, 36, 134, 30, 1, 0, 160, 96, 244, 86, 22, 254, 97, 87, 255, 134, 3, 0, 160, 160, 245, 129, 103, 255, 224, 8, 0, 86, 154, 28, 96, 68, 192, 64, 162, 192, 71, 54, 234, 134, 3, 0, 160, 172, 65, 129, 96, 255, 224, 8, 0, 86, 26, 27, 66, 212, 240, 64, 162, 192, 86, 164, 254, 70, 15, 1, 12, 6, 34, 160, 192, 38, 136, 2, 70, 106, 0, 134, 15, 1, 0, 0, 102, 184, 2, 134, 13, 1, 134, 70, 0, 102, 184, 2, 134, 248, 0, 198, 96, 0, 34, 160, 1, 38, 184, 2, 6, 95, 0, 146, 39, 4, 129, 65, 255, 98, 160, 0, 34, 160, 194, 135, 25, 2, 198, 93, 0, 184, 87, 168, 39, 165, 169, 255, 198, 240, 0, 0, 0, 0, 12, 20, 102, 184, 44, 168, 71, 129, 56, 255, 12, 6, 34, 160, 194, 135, 26, 2, 70, 85, 0, 136, 55, 184, 87, 168, 39, 32, 136, 17, 137, 193, 37, 167, 255, 33, 20, 254, 136, 193, 105, 98, 34, 210, 43, 137, 34, 160, 70, 131, 45, 4, 134, 73, 0, 145, 15, 254, 12, 6, 162, 9, 0, 34, 160, 198, 103, 154, 2, 134, 72, 0, 104, 39, 40, 89, 130, 200, 240, 128, 102, 192, 146, 160, 192, 96, 41, 147, 98, 199, 24, 157, 6, 178, 160, 239, 198, 1, 0, 162, 9, 0, 27, 153, 160, 187, 48, 96, 169, 192, 135, 42, 241, 130, 7, 5, 162, 7, 4, 98, 7, 6, 128, 136, 17, 160, 152, 32, 0, 102, 17, 144, 134, 32, 98, 7, 7, 146, 160, 193, 128, 102, 1, 128, 102, 32, 96, 139, 192, 128, 41, 147, 12, 6, 70, 51, 0, 0, 129, 246, 253, 12, 6, 146, 8, 0, 34, 160, 198, 103, 153, 2, 198, 46, 0, 152, 56, 34, 160, 200, 103, 25, 2, 6, 44, 0, 98, 72, 0, 40, 88, 6, 42, 0, 28, 130, 39, 152, 2, 70, 192, 0, 12, 6, 12, 18, 70, 38, 0, 0, 102, 72, 2, 70, 196, 0, 6, 32, 0, 102, 184, 2, 6, 198, 0, 70, 1, 0, 102, 72, 2, 6, 198, 0, 12, 6, 34, 160, 192, 6, 30, 0, 102, 184, 2, 70, 196, 0, 6, 24, 0, 193, 252, 254, 12, 6, 168, 12, 12, 18, 130, 200, 240, 157, 6, 160, 146, 131, 128, 38, 131, 32, 153, 16, 34, 160, 198, 103, 153, 82, 177, 246, 254, 109, 9, 216, 11, 34, 160, 201, 135, 61, 69, 128, 224, 20, 12, 6, 34, 160, 192, 103, 158, 58, 98, 199, 24, 45, 14, 198, 2, 0, 42, 150, 152, 9, 75, 34, 153, 10, 75, 170, 12, 25, 32, 237, 192, 135, 50, 237, 22, 89, 43, 169, 12, 233, 11, 134, 171, 0, 0, 0, 102, 136, 2, 134, 175, 0, 12, 18, 12, 6, 198, 1, 0, 0, 0, 98, 160, 0, 34, 160, 255, 32, 160, 116, 229, 149, 255, 96, 160, 116, 165, 149, 255, 101, 161, 255, 86, 34, 199, 98, 7, 1, 12, 248, 135, 22, 64, 103, 56, 21, 102, 70, 2, 6, 115, 0, 102, 102, 2, 198, 120, 0, 38, 54, 2, 70, 21, 255, 6, 30, 0, 0, 28, 34, 39, 150, 2, 6, 109, 0, 103, 50, 12, 28, 18, 39, 150, 2, 6, 57, 0, 198, 14, 255, 0, 0, 34, 160, 210, 39, 22, 71, 34, 160, 212, 39, 22, 110, 134, 10, 255, 0, 161, 204, 254, 129, 216, 254, 224, 8, 0, 97, 202, 254, 129, 203, 254, 192, 32, 0, 104, 6, 184, 55, 128, 134, 16, 192, 136, 17, 96, 100, 53, 138, 102, 176, 102, 130, 184, 39, 173, 2, 176, 182, 194, 129, 207, 254, 224, 8, 0, 162, 163, 232, 129, 204, 254, 224, 8, 0, 6, 251, 254, 0, 210, 39, 5, 194, 39, 4, 178, 39, 3, 168, 39, 165, 154, 255, 134, 246, 254, 0, 178, 7, 3, 34, 7, 2, 128, 187, 17, 32, 187, 32, 178, 203, 240, 162, 199, 24, 165, 123, 255, 70, 240, 254, 0, 0, 0, 98, 7, 3, 34, 7, 2, 128, 102, 17, 32, 102, 32, 33, 142, 253, 98, 198, 240, 136, 50, 157, 2, 128, 102, 99, 22, 38, 186, 136, 18, 138, 134, 128, 76, 65, 6, 2, 0, 146, 97, 13, 101, 98, 255, 146, 33, 13, 162, 41, 4, 166, 26, 4, 168, 41, 167, 164, 235, 165, 90, 255, 22, 154, 255, 168, 18, 96, 198, 32, 178, 199, 24, 129, 174, 254, 224, 8, 0, 140, 58, 114, 160, 196, 121, 82, 120, 18, 106, 119, 121, 18, 120, 50, 96, 103, 192, 105, 50, 70, 214, 254, 98, 7, 3, 130, 7, 2, 128, 102, 17, 128, 102, 32, 34, 199, 24, 98, 198, 240, 12, 25, 70, 30, 0, 0, 65, 149, 254, 113, 145, 253, 226, 36, 0, 98, 97, 7, 224, 119, 192, 114, 97, 6, 120, 37, 12, 57, 119, 54, 1, 12, 25, 153, 209, 233, 193, 101, 91, 255, 152, 209, 113, 141, 254, 232, 193, 161, 140, 254, 189, 2, 153, 1, 242, 193, 24, 221, 7, 194, 193, 28, 129, 148, 254, 224, 8, 0, 157, 10, 184, 37, 168, 113, 160, 187, 192, 185, 37, 160, 102, 192, 184, 4, 170, 34, 168, 97, 170, 187, 11, 169, 160, 169, 32, 185, 4, 160, 175, 5, 112, 187, 192, 204, 154, 194, 219, 128, 12, 29, 192, 173, 131, 22, 170, 0, 173, 7, 153, 209, 37, 108, 255, 152, 209, 121, 4, 140, 182, 120, 51, 140, 119, 144, 127, 49, 144, 119, 192, 150, 119, 247, 214, 137, 0, 34, 160, 199, 41, 83, 6, 62, 0, 0, 86, 73, 15, 40, 51, 22, 82, 170, 34, 160, 200, 6, 1, 0, 0, 0, 34, 160, 201, 41, 83, 70, 165, 254, 0, 168, 39, 86, 250, 168, 129, 118, 254, 224, 8, 0, 161, 100, 254, 129, 112, 254, 224, 8, 0, 129, 115, 254, 224, 8, 0, 198, 157, 254, 104, 55, 22, 38, 167, 32, 162, 32, 129, 110, 254, 224, 8, 0, 162, 163, 232, 129, 104, 254, 224, 8, 0, 224, 6, 0, 134, 150, 254, 0, 137, 193, 129, 106, 254, 224, 8, 0, 136, 193, 160, 130, 147, 173, 8, 70, 230, 254, 40, 39, 104, 55, 96, 130, 32, 128, 128, 180, 22, 200, 185, 198, 104, 255, 178, 39, 3, 162, 39, 2, 37, 119, 255, 34, 160, 0, 12, 22, 160, 38, 147, 70, 100, 255, 248, 119, 232, 103, 216, 87, 200, 71, 184, 55, 168, 39, 12, 18, 129, 81, 254, 224, 8, 0, 109, 10, 12, 10, 96, 42, 131, 6, 96, 255, 168, 39, 12, 11, 129, 75, 254, 224, 8, 0, 12, 2, 6, 89, 255, 0, 40, 39, 104, 55, 192, 32, 0, 105, 2, 12, 6, 45, 6, 6, 88, 255, 33, 57, 254, 136, 87, 104, 39, 137, 2, 33, 55, 254, 105, 2, 6, 246, 255, 145, 53, 254, 12, 8, 104, 9, 34, 160, 200, 96, 40, 131, 109, 2, 33, 49, 254, 137, 9, 137, 2, 12, 18, 96, 40, 131, 6, 73, 255, 0, 40, 51, 22, 34, 241, 70, 107, 254, 29, 240, 0, 0, 54, 65, 0, 157, 2, 130, 160, 192, 40, 3, 135, 153, 14, 204, 50, 12, 18, 198, 6, 0, 12, 2, 41, 3, 124, 226, 29, 240, 38, 18, 5, 38, 34, 18, 6, 12, 0, 130, 160, 219, 128, 41, 35, 135, 153, 41, 12, 34, 41, 3, 6, 8, 0, 34, 160, 220, 39, 153, 8, 12, 18, 41, 3, 45, 8, 29, 240, 0, 130, 160, 221, 124, 242, 135, 153, 11, 12, 18, 41, 3, 34, 160, 219, 29, 240, 0, 124, 242, 29, 240, 0, 0],
        "text_start": 1074393088,
        "entry": 1074394472,
        "data": [8, 192, 254, 63],
        "data_start": 1073736612,
    }

    constructor(port, baud, reader) {
        super(port, baud, undefined, reader);
    }

    get_erase_size = (offset, size) => {
        return size;
    }
}

class ESP32StubLoader extends ESP32ROM {
    /// Access class for ESP32 stub loader, runs on top of ROM.
    FLASH_WRITE_SIZE = 0x4000  // matches MAX_WRITE_BLOCK in stub_loader.c
    STATUS_BYTES_LENGTH = 2  // same as ESP8266, different to ESP32 ROM
    IS_STUB = true

    constructor(rom_loader) {
        super(rom_loader._port, 115200, rom_loader.reader);
        rom_loader.flush();
    }
}

ESP32ROM.STUB_CLASS = ESP32StubLoader

export {main as flash}