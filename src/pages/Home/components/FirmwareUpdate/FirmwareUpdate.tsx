import React, { useState, useContext } from 'react'
import { SerialPortContext } from '../../../../context/index'
import { Upload, Button, Modal, Spin } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import { flash } from './components/esptool'

const FirmwareUpdate: React.FC<any> = ({ modalVisible, setModalVisible }) => {
  const [fileList, setFileList] = useState([])
  const [isFlashing, setIsFlashing] = useState(false);

  const {
    serialPort,
  } = useContext(SerialPortContext)

  const dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess('ok')
    }, 0)
  }

  const getAddresses = {
    'bootloader.bin': 0x1000,
    'partition-table.bin': 0x8000,
    'ninjastep-driver.bin': 0x10000,
  }

  const props = {
    name: 'file',
    action: '',
    accept: '.bin',
    multiple: true,
    onChange: ({ fileList }) => {
      setFileList(fileList)
    },
    customRequest: dummyRequest,
  }

  const startFlash = async (e) => {
    e.preventDefault()

    let files = []

    for (const file of fileList) {
      files.push({
        argfile: file.originFileObj,
        address: getAddresses[file.name],
      })
    }

    await serialPort.requestPort();
    setIsFlashing(true);
    await flash(serialPort.port, files)
    setIsFlashing(false);
    setFileList([])
  }

  const handleOk = () => setModalVisible(false);

  const handleCancel = () => setModalVisible(false);

  return (
    <Modal visible={modalVisible} onOk={handleOk} onCancel={handleCancel}>
      <Upload {...props} fileList={fileList}>
        <Button icon={<UploadOutlined />} disabled={fileList.length >= 3}>
          Select file
        </Button>
          {"  "}{isFlashing && <Spin />}
      </Upload>
      <br />
      <Button
        onClick={async (e) => await startFlash(e)}
        disabled={fileList.length < 3}
      >
        Start update
      </Button>
    </Modal>
  )
}

export default FirmwareUpdate
