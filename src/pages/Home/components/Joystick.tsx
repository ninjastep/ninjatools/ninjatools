import React, { useEffect, useContext, useState } from 'react'
import { useSelector } from 'react-redux';
import { Switch } from 'antd';
import ReactNipple from 'react-nipple'
import 'react-nipple/lib/styles.css'
import './styles/joystick.css'
import { CommunicationContext, MotorContext, ConnectionContext } from '../../../context/index'
import ReactInterval from 'react-interval';

const Joystick = () => {
  const { sendCommand } = useContext(CommunicationContext);
  const { controller } = useContext(ConnectionContext)
  const { maxSpeed } = useContext(MotorContext)
  const position = useSelector((state) => state.position);
  const [speed, setSpeed] = useState(0);
  const [move, setMove] = useState(false);
  const [checked, setChecked] = useState(false);

  const handleEnd = () => {
    if (!checked) {
      console.log("joystick released")
      setSpeed(0);
      changePos();
      setTimeout(() => {
        setMove(false);
      }, 100)
    }
  }

  useEffect(() => {
    handleEnd();
  }, [checked])

  const handleStart = () => {
    setMove(true)
  }

  const handleMove = (evt, data) => {
    //setSpeed(evt.distance/50)
    let speedPercentage = data.distance / 50;
    if (data.direction) {
      if (data.direction.x === 'left') {
        speedPercentage = -speedPercentage;
      }
    }

    setSpeed(speedPercentage);
  }

  const changePos = () => {
    console.log(maxSpeed * speed);
    controller == 'Position' || 'Torque' ?
     sendCommand("set_pos " + (position + speed)) :
     controller == 'Speed' && 
     sendCommand("set_spd " + maxSpeed * speed);
  }

  const handleSwitch = (checked) => {
    setChecked(checked);
  }

  return (
    <div style={{ textAlign: "center" }}>
      <ReactInterval timeout={10} enabled={move} callback={changePos} />
      <ReactNipple
        key={checked} // Sticky mode doesn't work without this
        // supports all nipplejs options
        // see https://github.com/yoannmoinet/nipplejs#options
        options={{
          mode: 'static',
          lockX: true,
          color: '#DDDEFB',
          position: { top: '50%', left: '50%' },
          restOpacity: 1,
          shape: 'square',
          restJoystick: !checked,
        }}
        // any unknown props will be passed to the container element, e.g. 'title', 'style' etc
        style={{
          width: 150,
          height: 150,
          margin: "auto",
          // if you pass position: 'relative', you don't need to import the stylesheet
        }}
        // all events supported by nipplejs are available as callbacks
        // see https://github.com/yoannmoinet/nipplejs#start
        onMove={(evt, data) => handleMove(evt, data)}
        onEnd={handleEnd}
        onStart={handleStart}
      />
      <Switch
        onChange={handleSwitch}
        
        style={{
          marginBottom: "5%",
        }}
        checkedChildren="Sticky"
        unCheckedChildren="Sticky"
      />
    </div>
  )
}

export default Joystick
