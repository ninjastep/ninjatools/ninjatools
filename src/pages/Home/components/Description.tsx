import React, { useContext, useEffect } from "react";
import { CommunicationContext, MotorContext, ConnectionContext } from "../../../context/index";
import { Button, Row, Col, Space, Dropdown } from "antd";
import { PoweroffOutlined, DownOutlined } from "@ant-design/icons";
import PositionSlider from "./PositionSlider";
import CircularAngleSlider from "../components/CircularAngleSlider";
import DescriptionForm from "../components/DescriptionForm";
import ContentPanel from "../../../components/ContentPanel";
import Joystick from './Joystick';

/* This components displays a description of the motor and it's heartbeat variables. It uses A position slider and CircularAngeSlider
  to visualize position and angle of the motor

*/

const Description = () => {
  /*
    These variables and functions are not rapidly updated and thus shared via useContext
  */
  const {
    chainCommands,
    sendCommand,
    isLoading
  } = useContext(CommunicationContext);

  const {
    controllerDropDownMenu,
    controllerIcon,
    dropdownPreview,
    controller,
  } = useContext(ConnectionContext)

  const { running } = useContext(MotorContext)

  useEffect(() => {
    if (!isLoading) {
      sendCommand("get_ctrl_mode");
    }
  }, [])

  const handleMotorButtonClick = () => {
    if (running) {
      chainCommands(["stop_hb_stream", "stop_mot"]);
    } else {
      chainCommands(["start_mot", "start_hb_stream 5"]);
    }
  };

  const buttonText = () => {
    if (running) {
      return "Stop motor";
    } else {
      return "Start motor";
    }
  };

  const controllerContent = () => {
    if (controller === "Speed") {
      return (
        <>
          <Row>
            <Col span={16} offset={4}>
              <Joystick />
            </Col>
          </Row>
        </>
      );
    } else {
      return (
        <>
          <Row>
            <Col span={16} offset={4}>
              <PositionSlider />
            </Col>
          </Row>
          <Row style={{ justifyContent: "center" }}>
            <div className="ns-motor-container">
              <CircularAngleSlider />
            </div>
          </Row>
          <Row>
            <Col span={16} offset={4}>
              <Joystick />
            </Col>
          </Row>
        </>
      );
    }
  };

  return (
    <>
      <ContentPanel
        title="Description"
        showConnected={true}
        extra={
          <Space>
            <Dropdown overlay={controllerDropDownMenu}>
              <Button>
                {controllerIcon()} {dropdownPreview()} <DownOutlined />
              </Button>
            </Dropdown>

            <Button
              type="primary"
              icon={<PoweroffOutlined />}
              onClick={handleMotorButtonClick}
            >
              {buttonText()}
            </Button>
          </Space>
        }
      >
        {controllerContent()}

        <DescriptionForm />
      </ContentPanel>
    </>
  );
};

export default Description;
