import React, { useContext } from 'react'
import { Input, Button, Form, Typography, Row, Col } from 'antd'
import { CommunicationContext } from '../../../context/CommunicationContext';
import ContentPanel from '../../../components/ContentPanel';

const { Text } = Typography;

const SendCommandHome = () => {

    const {
        sendCommand,
      } = useContext(CommunicationContext);
   
    const sendToNS = (values) => {
        sendCommand("" + values.command);
      };
    
      const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
      };
   
    return(
        <ContentPanel>
        <Text strong>Send Command</Text>
        <Form
        layout="horizontal"
        name="send command"
        initialValues={{ remember: true }}
        onFinish={sendToNS}
        onFinishFailed={onFinishFailed}
      >
        <Row gutter={[24,8]}>
          <Col span={16}>
          <Form.Item label="" name="command">
          <Input />
        </Form.Item>
          </Col>
          <Col span={8}>
          <Form.Item>
          <Button type="primary" htmlType="submit">
            Send
          </Button>
        </Form.Item>
          </Col>
        </Row>



      </Form>
      </ContentPanel>
    )
}

export default SendCommandHome