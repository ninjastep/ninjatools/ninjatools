import React from "react";
import CircularSlider from "./CircularSlider/CircularSlider"

/*
  The circular angle slider is used as a tool to direct control the position of the Ninja Step motor
*/

const CircularAngleSlider = () => {
  return (
    <div>
      <CircularSlider />
    </div>
  );
};

export default CircularAngleSlider;
