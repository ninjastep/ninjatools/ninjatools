import React, { useState, useEffect, useRef, useContext } from 'react'
import './styles/CircularSlider.css'
import { pathGenerator } from './helpers/path-generator'
import { useSelector } from 'react-redux'
import useEventListener from './helpers/useEventListener'
import { CommunicationContext } from '../../../../context/CommunicationContext'

const CircularSlider = () => {
  const [color, setColor] = useState('#DDDEFB')
  const radius = useRef(150)
  const size = useRef(radius.current * 2)
  const [pos, setPos] = useState({ x: size.current, y: size.current / 2 })
  const [isDragging, setIsDragging] = useState(false)
  const [degrees, setDegrees] = useState(0)
  const position = useSelector((state) => state.position)
  const [radians, setRadians] = useState(position || 0)
  const [path, setPath] = useState('')
  const [prev, setPrev] = useState(0)
  const [rotations, setRotations] = useState(0)
  const circularSlider = useRef(null)
  const SLIDER_EVENT = {
    DOWN: 'mousedown',
    UP: 'mouseup',
    MOVE: 'mousemove',
  }

  const {
    sendCommand,
    degreesToRadians,
  } = useContext(CommunicationContext)

  const handleMouseDown = () => {
    setIsDragging(true)
  }

  const handleMouseUp = () => {
    setIsDragging(false)
  }

  const onMouseMove = (event: any) => {
    event.preventDefault()

    const offsetRelativeToDocument = (ref: any) => {
      const rect = ref.current.getBoundingClientRect()
      const scrollLeft = document?.documentElement?.scrollLeft ?? 0
      const scrollTop = document?.documentElement?.scrollTop ?? 0
      return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }

    const mouseXFromCenter =
      event.pageX - (offsetRelativeToDocument(circularSlider).left + radius.current)
    const mouseYFromCenter =
      event.pageY - (offsetRelativeToDocument(circularSlider).top + radius.current)

    isDragging && setRadians(Math.atan2(mouseYFromCenter, mouseXFromCenter))
  }

  useEffect(() => {
    setKnobPos(Math.floor(radiansToDegrees(radians)))
  }, [radians])

  useEffect(() => {
    !isDragging && setRadians(position) 
  }, [position])

  const setKnobPos = (degrees: number) => {
    let temp = degrees
    if (degrees > 360 || degrees < -360) {
      setRotations(Math.floor(degrees / 360))
      let tempRotations = degrees / 360
      temp = 360 * (tempRotations < 0 ? tempRotations + Math.ceil(tempRotations) : tempRotations - Math.floor(tempRotations))
      temp = temp < 0 ? negativeToPositive(temp) : temp;
    }
    const radians = (temp * Math.PI) / 180

    setDegrees(temp)
    setPos({
      x: radius.current * Math.cos(radians) + radius.current,
      y: radius.current * Math.sin(radians) + radius.current,
    })
    generatePath()
  }

  const negativeToPositive = (degrees: number) => {
    let temp = degrees;
    while(temp < 0){
      temp += 360;
    }
    return temp;
  }

  const radiansToDegrees = (radians: number) => {
    let degrees = radians * (180 / Math.PI)
    return degrees < 0 ? negativeToPositive(degrees) : degrees
  }

  const generatePath = () => {
    let path = pathGenerator(radius.current, radius.current, radius.current, 0, degrees)
    setPath(path)
  }

  useEffect(() => {
    let rotate = 0;
    rotate = degrees < 90 && prev > 270 ? 1 : rotate 
    rotate = degrees > 270 && prev < 90 ? -1 : rotate
    setPrev(degrees)
    setRotations(rotations + rotate);

    let fixedDegrees = degrees + (360 * (rotations + rotate))
    isDragging && sendCommand('set_pos ' + degreesToRadians(fixedDegrees))
  }, [degrees])

  useEventListener(SLIDER_EVENT.MOVE, onMouseMove)
  useEventListener(SLIDER_EVENT.UP, handleMouseUp)

  return (
    <div className="container">
      <svg
        className="svg"
        height={size.current}
        width={size.current}
        ref={circularSlider}
        style={{ position: 'relative', zIndex: 100 }}
      >
        <g>
          <text
            x={size.current / 2}
            y={size.current / 2 - 60}
            textAnchor="middle"
            alignmentBaseline="middle"
            className="label"
          >
            Angle
          </text>
          <text
            x={size.current / 2}
            y={size.current / 2}
            textAnchor="middle"
            alignmentBaseline="middle"
            className="valueText"
          >
            {Math.floor(degrees + (360 * rotations)) + '°'}
          </text>
          <circle
            cx={size.current / 2}
            cy={size.current / 2}
            r={radius.current}
            pathLength={Math.abs(degrees)}
            stroke={color}
            strokeWidth="10"
            fill="none"
          />
          <path
            style={{ strokeLinecap: 'round' }}
            d={path}
            stroke="#80C3F3"
            strokeWidth="10"
            fill="none"
          />
          <circle
            cx={pos.x}
            cy={pos.y}
            r={18}
            fill="#4e63ea"
            fillOpacity="0.2"
            stroke="none"
          />
          <circle
            onMouseDown={handleMouseDown}
            cx={pos.x}
            cy={pos.y}
            r={15}
            fill="#4e63ea"
          />
        </g>
      </svg>
      <svg
        width="200"
        height="150"
        style={{
          position: 'absolute',
          top: '0',
          left: '50%',
          transform: 'translateX(-50%)',
          userSelect: 'none',
        }}
      >
        <text
          x="100"
          y="30"
          textAnchor="middle"
          alignmentBaseline="middle"
          fontSize="20px"
        >
          - +
        </text>
        <text
          x={140}
          y={30}
          fontSize="20"
          fill={'#95EB9D'}
          stroke={'#6FE27A'}
          textAnchor="middle"
          alignmentBaseline="middle"
        >
          {rotations > 0 && rotations}
        </text>
        <text
          x={60}
          y={30}
          fontSize="20"
          fill={'#E88F8F'}
          stroke={'#E56C6C'}
          textAnchor="middle"
          alignmentBaseline="middle"
        >
          {rotations + 1 < 0 && -1 * rotations -1}
        </text>
      </svg>
    </div>
  )
}

export default CircularSlider
