import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Row, Col, Divider, Spin } from "antd";
import { CommunicationContext, MotorContext, ConnectionContext } from "../../context/index";
import { Description, Performance, SendCommandHome } from "./components";
import RequirementsModal from "../../components/RequirementsModal";

const Home = () => {
  //  These variables and functions are exported from the CommunicationContext
  const history = useHistory()
  const { sendCommand, isLoading, demoMode } = useContext(CommunicationContext);
  const { connected } = useContext(ConnectionContext)

  const { running } = useContext(MotorContext)

  // Starts heartbeat when connected and on home, stops heartbeat on disconnect or leaving home
  useEffect(() => {
    if (running) {
      // start_hb_stream is a decimal off in its calculations so start_hb_stream 1 sets frequency to 10hz, ten times a second
      sendCommand("start_hb_stream 5");
    }

    // return () => {
    //   sendCommand("stop_hb_stream")
    // };
  }, []);

  const mainContent = () => {
    console.log("Connected: " + connected)
    if (isLoading) {
      return <Spin size="large" />;
    } else {
      if (connected) {
        return (
          <>
            <Row gutter={16}>
              <Col span={12}>
                <Description />
                <Divider />
              </Col>
              <Col span={12}>
                <Row>
                  <Performance />
                </Row>
                <Divider />
                <Row>
                  <Col span={16}>
                    <SendCommandHome />
                  </Col>
                </Row>
              </Col>
            </Row>
          </>
        );
      } else {
        return;
      }
    }
  };

  return (
    <>
      <div>
        <RequirementsModal />
        {mainContent()}
      </div>
    </>
  );
};

export default Home;
