import React, { useContext, useEffect } from "react";
import { Row, Col, Button } from "antd";
import { XTermTerminal } from "./components";
import ContentPanel from "../../components/ContentPanel";
import { CommunicationContext, MotorContext, ConnectionContext } from '../../context/index';
import { useHistory } from "react-router-dom";

const Terminal: React.FC<any> = () => {

  const {
    sendCommand,
  } = useContext(CommunicationContext);

  const {
    connected,
    hardReset,
    running
  } = useContext(ConnectionContext);

  const { setRunning } = useContext(MotorContext)

  const history = useHistory();

  useEffect(() => {
    if (running)
      sendCommand("stop_hb_stream") // For demoMode
  }, [])


  useEffect(() => {
    if (connected) {
      sendCommand("cli")
      setRunning(false)
    }
    return () => {
      if (connected) {
        sendCommand("exit")
        setTimeout(() => {
          sendCommand("knock_knock")
        }, 2000);
      }
    }
  }, [])

  useEffect(() => {
    if (!connected) {
      history.push("/")
    }
  }, [connected])

  const hardRs = () => {
    hardReset();
    console.log("hard reset activated")
  }

  return (
    <>
      <Row>
        <Col className="gutter-row" span={16} offset={4}>
          <ContentPanel>
            <XTermTerminal />
          </ContentPanel>
        </Col>
      </Row>
    </>
  );
};

export default Terminal;
