import React, { useRef, useEffect, useContext } from "react";
import XTerm, { Terminal } from "react-xterm";
import "xterm/css/xterm.css";
import { CommunicationContext } from "../../../context/CommunicationContext";
import { useSelector } from "react-redux";

const XTermTerminal = () => {
  const {
    sendCommand,
    // terminalOutput
  } = useContext(CommunicationContext);

  const terminalOutput = useSelector((state) => state.terminal);

  const inputRef = useRef<XTerm>();

  useEffect(() => {
    runFakeTerminal(inputRef.current);
    return () => {
      inputRef.current?.componentWillUnmount();
    };
  }, [inputRef]);

  useEffect(() => {
    inputRef.current.writeln("Welcome to NinjaStep Terminal");
    inputRef.current.writeln('type "cli" to start');
    inputRef.current.writeln('Type "exit" to stop"');
    inputRef.current.writeln("");
  }, []);

  useEffect(() => {
    console.log("terminalOutput: " + terminalOutput);
    // inputRef.current.writeln('Welcome to NinjaStep Terminal');
    // inputRef.current.writeln('type "cli" to start');
    // inputRef.current.writeln('Type "exit" to stop"');
    // inputRef.current.writeln('');
    inputRef.current.writeln(terminalOutput);
    // inputRef.current.write('\r\n' + '');
  }, [terminalOutput]);

  // const prompt = () => {
  //     xterm.write('\r\n' + shellprompt);
  // }

  function runFakeTerminal(xterm: XTerm) {
    var myBuffer = [] as any;
    const term: Terminal = xterm.getTerminal();
    var shellprompt = "";

    function prompt() {
      xterm.write("\r\n" + shellprompt);
    }
    // xterm.writeln('Welcome to xterm.js');
    // xterm.writeln('This is a local terminal emulation, without a real terminal in the back-end.');
    // xterm.writeln('Type some keys and commands to play around.');
    // xterm.writeln('');
    // prompt();

    term.on("key", function (key, ev) {
      //Från template
      var printable = !ev!!.altKey && !ev!!.ctrlKey && !ev!!.metaKey;

      // curr_line += key;
      // xterm.write(key);

      if (ev!!.keyCode === 13) {
        let keysEntered = myBuffer.join(""); //buffer to string
        console.log("buffer: " + keysEntered);
        sendCommand("" + keysEntered);
        myBuffer = []; //clear buffer
        prompt();
        // } else if (ev.keyCode == 8) {
        //   // Do not delete the prompt
        //   if (term['x'] > 2) {
        //     xterm.write('\b \b');
        //   }
      } else if (!(ev!!.keyCode < 32 || ev!!.keyCode === 127)) {
        xterm.write(key);
        myBuffer.push(key);
      } else if (ev!!.keyCode === 8) {
        console.log("delete button");
        myBuffer.splice(-1, 1);
        xterm.write("\b \b");
      } else if (printable) {
        //Den här if-satsen reachas inte
        xterm.write(key);

        myBuffer.push(key);
        console.log("printable: " + printable + " key: " + key);
      }
    });

    term.on("paste", function (data) {
      //vad gör denna funktionen?
      xterm.write(data);
    });
  }

  return (
    <div>
      <XTerm
        ref={inputRef}
        addons={["fit", "fullscreen", "search"]}
        options={{ cursorBlink: "true" }}
        style={{
          overflow: "hidden",
          position: "relative",
          width: "100%",
          height: "100%",
        }}
      />
    </div>
  );
};

export default XTermTerminal;
