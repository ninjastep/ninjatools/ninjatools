import React, { useContext, useState, useEffect, useRef } from "react";
import { CommunicationContext, MotorContext, ConnectionContext } from "../../../context/index";
import {
  PageHeader,
  Input,
  Button,
  Form,
  Spin,
  Row,
  Col,
  Progress,
} from "antd";
import ContentPanel from "../../../components/ContentPanel";

const CalibrateAngle = () => {
  const {
    resistance,
    maxCurrent,
    calibrated,
    running,
    calibrationStage,
    setCalibrationStage,
  } = useContext(MotorContext);

  const { sendCommand } = useContext(CommunicationContext)
  const { softReset } = useContext(ConnectionContext)

  const [form] = Form.useForm();

  const [runningVoltage, setRunningVoltage] = useState(0);
  const [isCalibrating, setIsCalibrating] = useState(false);
  // Added this because without it, default value became undefined when submitted
  const [revolutions, setRevolutions] = useState(5);

  useEffect(() => {
    form.setFieldsValue({
      runningvoltage: (maxCurrent * resistance).toFixed(2),
      revolutions,
    });
  }, [resistance, maxCurrent]);

  useEffect(() => {
    if (calibrated === "OK" || calibrated == "ERR") {
      setIsCalibrating(false);
    }
  }, [calibrated]);

  const calibrateAngle = async () => {
    running && (await softReset());
    const runningvolt = form.getFieldValue("runningvoltage");
    const revol = form.getFieldValue("revolutions");
    console.log(runningvolt);
    if (revol === "null") {
      sendCommand("start_cal " + runningvolt);
    } else {
      sendCommand("start_cal " + runningvolt + " " + revol);
    }
    setCalibrationStage(0);
    setIsCalibrating(true);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const refRunVol = useRef(null);
  const refRev = useRef(null);
  const refCalButton = useRef(null);

  const switchFocus = (e) => {
    console.log(e.target.id);
    switch (e.target.id) {
      case "calibrate motor_runningvoltage":
        refRev.current.focus();
        break;
      case "calibrate motor_revolutions":
        refCalButton.current.focus();
        break;
    }
  };

  return (
    <ContentPanel title="Calibrate Angle">
      <Form
        name="calibrate motor"
        initialValues={{ remember: true }}
        onFinish={calibrateAngle}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Row>
          <Col span={20}>
            <Form.Item
              name="runningvoltage"
              rules={[
                {
                  required: true,
                  message: "Please input running voltage!",
                },
              ]}
              validateStatus={
                runningVoltage / resistance > 0.5 ? "warning" : "validating"
              }
              help={
                runningVoltage / resistance > 0.5 &&
                "This running voltage can damage your computer"
              }
            >
              <Input
                type="number"
                addonBefore="Running Voltage"
                addonAfter="V"
                placeholder={
                  (0.5 * resistance * 0.9).toFixed(2) + " - Suggested"
                }
                onChange={(e) => {
                  setRunningVoltage(+e.target.value);
                }}
                min={0}
                max={50}
                step={0.01}
                ref={refRunVol}
                onPressEnter={(e) => {
                  switchFocus(e);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={20} style={{ height: "35px", marginBottom: "5px" }}>
            <Form.Item name="revolutions">
              <Input
                type="number"
                addonBefore="Revolutions"
                defaultValue={revolutions}
                onChange={(e) => setRevolutions(+e.target.value)}
                min={0}
                max={50}
                step={1}
                ref={refRev}
                onPressEnter={(e) => {
                  switchFocus(e);
                }}
              />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={[20, 6]}>
          {isCalibrating && (
            <Col span={20}>
              <Progress
                percent={
                  calibrationStage === 0
                    ? 0
                    : Math.floor((calibrationStage / 3) * 100)
                }
              />
            </Col>
          )}
          <Col span={20}>
            <Button
              type="primary"
              onClick={calibrateAngle}
              style={{ marginTop: "5px" }}
              ref={refCalButton}
            >
              Calibrate
            </Button>
          </Col>
        </Row>
      </Form>
    </ContentPanel>
  );
};

export default CalibrateAngle;
