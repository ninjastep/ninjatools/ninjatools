import React, { useContext, useEffect, useState, useRef } from 'react'
import { Tag, Row, Col, Button, Form, Input, Dropdown, Menu } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import { CommunicationContext, MotorContext, ConnectionContext } from '../../../context/index'
import ContentPanel from '../../../components/ContentPanel'
import inputStep from '../../../components/helpers/inputStep'
import CustomInput from '../../../components/CustomInput'

const ConfigurateMotor = () => {
  const [form] = Form.useForm()

  const [selectedKeys, setSelectedKeys] = useState(['rad/s'])
  const [calibratedTag, setCalibratedTag] = useState(
    <Tag color="error">Not Calibrated</Tag>,
  )

  const {
    resistance,
    inductance,
    motorConstant,
    poleNumber,
    calibrated,
    maxCurrent,
    maxSpeed,
    maxTorque,
  } = useContext(MotorContext)
  
  const {
    sendCommand,
    chainCommands,
  } = useContext(CommunicationContext)

  const { connected } = useContext(ConnectionContext)

  const getMotorSettings = () => {
    chainCommands([
      'get_res',
      'get_ind',
      'get_km',
      'get_n',
      'get_imax',
      'get_spd_max',
      'get_trq_max',
      'is_cal',
    ])
  }

  useEffect(() => {
    if (connected) {
      getMotorSettings()
    }
  }, [])

  useEffect(() => {
    changeCalibratedTag()
  }, [calibrated])

  const changeCalibratedTag = () => {
    if (calibrated === 'YES' || calibrated === 'OK') {
      setCalibratedTag(<Tag color="success">Calibrated</Tag>)
    } else {
      setCalibratedTag(<Tag color="error">Not Calibrated</Tag>)
    }
  }

  const sendCommandAndUpdate = (set, value, get) => {
    sendCommand(set + ' ' + value)
    setTimeout(() => {
      sendCommand(get)
    }, 500)
  }

  const onRefresh = () => {
    getMotorSettings()
  }

  const handleMenuClick = (e) => {
    // Selects unit and handles conversion
    let tempSpeed = form.getFieldValue('maxSpeed')
    setSelectedKeys((prev) => {
      if (prev[0] !== e.key) {
        if (prev[0] === 'rad/s') {
          tempSpeed *= 9.5493
        } else {
          tempSpeed /= 9.5493
        }
      }

      return [e.key]
    })
    form.setFieldsValue({ maxSpeed: tempSpeed.toFixed(2) })
  }

  const menu = (
    <Menu onClick={handleMenuClick} selectedKeys={selectedKeys}>
      <Menu.Item key="rad/s">rad/s</Menu.Item>
      <Menu.Item key="rpm">rpm</Menu.Item>
    </Menu>
  )

  const refRes = useRef(null)
  const refMotConst = useRef(null)
  const refMcurr = useRef(null)
  const refMtrq = useRef(null)
  const refInd = useRef(null)
  const refMspd = useRef(null)
  const refPolnum = useRef(null)

  const switchFocus = (e) => {
    // Using IIFE and Object to choose what to focus
    ;({
      motorconf_resistance: refMotConst,
      motorconf_motorConstant: refMcurr,
      motorconf_maxCurrent: refMtrq,
      motorconf_maxTorque: refInd,
      motorconf_inductance: refPolnum,
      motorconf_poleNumber: refMspd,
      motorconf_maxSpeed: refRes,
    }[e.target.id].current.focus())
  }

  return (
    <ContentPanel
      title="Configurate Motor"
      extra={<Button onClick={onRefresh}>Refresh</Button>}
    >
      <Form name="motorconf" form={form}>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item name="resistance">
              <CustomInput
                type="number"
                min={0}
                max={100}
                addonBefore="Resistance"
                addonAfter="Ohm"
                refs={refRes}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate('set_res', e.target.value, 'get_res')
                  switchFocus(e)
                }}
                form={form}
                state={{ resistance }}
                formatter={(value) => value.toFixed(2)}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="motorConstant">
              <CustomInput
                type="number"
                min={0}
                max={50}
                addonBefore="Motor constant"
                addonAfter="Ncm/A"
                refs={refMotConst}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate('set_km', e.target.value / 100, 'get_km')
                  switchFocus(e)
                }}
                form={form}
                state={{ motorConstant }}
                formatter={(value) => Math.round(value * 100)}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="maxCurrent">
              <CustomInput
                type="number"
                min={0}
                max={100}
                addonBefore="Max Current"
                addonAfter="A"
                refs={refMcurr}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate('set_imax', e.target.value, 'get_imax')
                  switchFocus(e)
                }}
                form={form}
                state={{ maxCurrent }}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="maxTorque">
              <CustomInput
                type="number"
                min={0}
                max={100}
                addonBefore="Max Torque"
                addonAfter="Ncm"
                refs={refMtrq}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate(
                    'set_trq_max',
                    e.target.value / 100,
                    'get_trq_max',
                  )
                  switchFocus(e)
                }}
                form={form}
                state={{ maxTorque }}
                formatter={(value) => value * 100}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="inductance">
              <CustomInput
                type="number"
                min={0}
                max={10}
                addonBefore="Inductance"
                addonAfter="mH"
                refs={refInd}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate(
                    'set_ind',
                    e.target.value / 1000,
                    'get_ind',
                  )
                  switchFocus(e)
                }}
                form={form}
                state={{ inductance }}
                formatter={(value) => +(value * 1000).toFixed(2)}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="poleNumber">
              <CustomInput
                type="number"
                min={0}
                max={100}
                addonBefore="Number of Poles"
                refs={refPolnum}
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate('set_n', e.target.value, 'get_n')
                  switchFocus(e)
                }}
                form={form}
                state={{ poleNumber }}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="maxSpeed">
              <CustomInput
                type="number"
                min={0}
                max={selectedKeys[0] === 'rad/s' ? 100 : 100 * 9.5493}
                addonBefore="Max Speed"
                refs={refMspd}
                addonAfter={
                  <Dropdown overlay={menu} placement={'bottomCenter'}>
                    <span>
                      {selectedKeys} {<DownOutlined />}
                    </span>
                  </Dropdown>
                }
                onPressEnter={(e: any) => {
                  sendCommandAndUpdate(
                    'set_spd_max',
                    selectedKeys[0] === 'rad/s'
                      ? e.target.value
                      : e.target.value / 9.5493,
                    'get_spd_max',
                  )
                  switchFocus(e)
                }}
                form={form}
                state={{ maxSpeed }}
                formatter={(value) =>
                  selectedKeys[0] === 'rad/s'
                    ? value
                    : +(value * 9.5493).toFixed(2)
                }
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Input
              type="number"
              min={0}
              max={50}
              step={0.1}
              addonBefore="Motor Calibrated"
              prefix={calibratedTag}
              disabled={true}
            />
          </Col>
        </Row>
      </Form>
    </ContentPanel>
  )
}

export default ConfigurateMotor
