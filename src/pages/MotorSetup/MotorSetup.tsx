import React, { useEffect, useContext } from "react";
import { Row, Col, Divider } from "antd";
import { ConfigurateMotor, CalibrateAngle } from "./components";
import { ConnectionContext, CommunicationContext } from "../../context/index";
import { useHistory } from "react-router-dom";

const MotorSetup: React.FC<any> = () => {


  const history = useHistory();
  const {
    connected,
    running
  } = useContext(ConnectionContext);

  const {
    sendCommand,
  } = useContext(CommunicationContext)

  useEffect(() => {
    if (!connected) {
      history.push("/")
    }
  }, [connected])

  useEffect(() => {
    if (running)
      sendCommand("stop_hb_stream") // For demoMode
  }, [])

  return (
    <>
      <Row gutter={16}>
        <Col className="gutter-row" span={12}>
          <ConfigurateMotor />
          <Divider />
        </Col>
        <Col span={8}>
          <CalibrateAngle />
          <Divider />
        </Col>
      </Row>
    </>
  );
};

export default MotorSetup;
