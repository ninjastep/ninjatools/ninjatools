import React, { useEffect, useContext } from "react";
import { StepDirInterface } from "./components";
import { Col } from "antd";
import { useHistory } from "react-router-dom";
import { ConnectionContext, CommunicationContext } from "../../context/index";

const InterfaceSetup: React.FC<any> = () => {

  const history = useHistory();
  const {
    connected,
    running
  } = useContext(ConnectionContext);

  const {
    sendCommand,
  } = useContext(CommunicationContext);

  useEffect(() => {
    if (!connected) {
      history.push("/")
    }
  }, [connected])

  useEffect(() => {
    if (running)
      sendCommand("stop_hb_stream") // For demoMode
  }, [])

  return (
    <>
      <Col span={18} offset={3}>
        <StepDirInterface />
      </Col>
    </>
  );
};

export default InterfaceSetup;
