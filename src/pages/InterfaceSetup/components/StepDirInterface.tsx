import React, { useState } from "react";
import {
  Row,
  Col,
  Typography,
  Divider,
  Select,
  Button,
  Alert,
  Switch,
  Form,
} from "antd";
import ContentPanel from "../../../components/ContentPanel";

const { Text } = Typography;

//JSON Object containing all current info about STEP/DIR Interface
const defaultParams = {
  step_GPIO: "27",
  step_LEVEL: "activeHigh",
  step_PULL: "no-pull",
  dir_GPIO: "26",
  dir_LEVEL: "activeHigh",
  dir_PULL: "no-pull",
  enable_GPIO: "none",
  enable_LEVEL: "",
  enable_PULL: "no-pull",
  fault_GPIO: "none",
  fault_LEVEL: "",
  fault_PULL: "no-pull",
  step_LENGTH: "1/8",
  active: true,
};

const { Option } = Select;

const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

// This send the Interface-settings to Ninja Step
const stepdirFormFinish = (values) => {
  console.log("Success:", defaultParams);
};

const StepDirInterface = () => {
  // These state variables are used to check if the same pin have been selected for two inputs
  const [stepPin, setStepPin] = useState("27");
  const [dirPin, setDirPin] = useState("26");
  const [enablePin, setEnablePin] = useState("None");
  const [faultPin, setFaultPin] = useState("None");

  //Check if the same pin has been selected twice
  const validateInput = () => {
    // Check pin 1 with 2, 3, 4
    if (
      stepPin === dirPin ||
      stepPin === enablePin ||
      stepPin === faultPin

      // defaultParams.step_GPIO === defaultParams.dir_GPIO ||
      // defaultParams.step_GPIO === defaultParams.enable_GPIO ||
      // defaultParams.step_GPIO === defaultParams.fault_GPIO
    ) {
      return false;
    }
    // Check pin 2 with 3, 4
    if (
      dirPin === enablePin ||
      dirPin === faultPin

      // defaultParams.dir_GPIO === defaultParams.enable_GPIO ||
      // defaultParams.dir_GPIO === defaultParams.fault_GPIO
    ) {
      return false;
    }
    // Check pin 3 with 4 (if they are none its no problem)
    if (
      enablePin !== "None" &&
      enablePin === faultPin
      // defaultParams.enable_GPIO === defaultParams.fault_GPIO
    ) {
      return false;
    }

    return true;
  };

  const showButton = () => {
    if (validateInput()) {
      return (
        <Button type="primary" htmlType="submit">
          Save
        </Button>
      );
    } else
      return (
        <Button type="primary" htmlType="submit" disabled>
          Save
        </Button>
      );
  };

  const showInputText = () => {
    if (validateInput()) {
      return null;
    } else {
      return (
        <Alert
          message="You cant select the same pin for two inputs"
          type="error"
        />
      );
    }
  };

  return (
    <>
      <ContentPanel title="STEP / DIR">
            <Text strong>Pins:</Text>
            <Divider />
            <Form
              name="stepdirForm"
              initialValues={{ remember: true }}
              onFinish={stepdirFormFinish}
              onFinishFailed={onFinishFailed}
            >
              <Row gutter={[16, 16]}>
                <Col span={6}> </Col>
                <Col span={6}>
                  {" "}
                  <Text strong>GPIO</Text>
                </Col>
                <Col span={6}>
                  <Text strong>Level</Text>
                </Col>
                <Col span={6}>
                  <Text strong>Pull</Text>
                </Col>
              </Row>

              <Row gutter={[16, 16]}>
                <Col span={6}>
                  <Text strong>STEP</Text>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue={stepPin}
                    dropdownMatchSelectWidth={false}
                    onChange={(v) => {
                      setStepPin(v);
                      defaultParams.step_GPIO = v;
                    }}
                  >
                    <Option value="24">24</Option>
                    <Option value="25">25</Option>
                    <Option value="26">26</Option>
                    <Option value="27">27</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue="activeHigh"
                    dropdownMatchSelectWidth={false}
                    onChange={(v) => {
                      defaultParams.step_LEVEL = v;
                    }}
                  >
                    <Option value="activeHigh">Active high</Option>
                    <Option value="activeLow">Active low</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    dropdownMatchSelectWidth={false}
                    defaultValue="no-pull"
                    onChange={(v) => (defaultParams.step_PULL = v)}
                  >
                    <Option value="pull-up">Pull-up</Option>
                    <Option value="pull-down">Pull-down</Option>
                    <Option value="no-pull">No-pull</Option>
                  </Select>
                </Col>
              </Row>

              <Row gutter={[16, 16]}>
                <Col span={6}>
                  <Text strong>DIR</Text>
                </Col>
                <Col span={6}>
                  {" "}
                  <Select
                    defaultValue={dirPin}
                    dropdownMatchSelectWidth={false}
                    onChange={(v) => {
                      setDirPin(v);
                      defaultParams.dir_GPIO = v;
                    }}
                  >
                    <Option value="24">24</Option>
                    <Option value="25">25</Option>
                    <Option value="26">26</Option>
                    <Option value="27">27</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  {" "}
                  <Select
                    defaultValue="activeHigh"
                    onChange={(v) => (defaultParams.dir_LEVEL = v)}
                  >
                    <Option value="activeHigh">Active high</Option>
                    <Option value="activeLow">Active low</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue="no-pull"
                    onChange={(v) => (defaultParams.dir_PULL = v)}
                  >
                    <Option value="pull-up">Pull-up</Option>
                    <Option value="pull-down">Pull-down</Option>
                    <Option value="no-pull">No-pull</Option>
                  </Select>
                </Col>
              </Row>

              <Row gutter={[16, 16]}>
                <Col span={6}>
                  <Text strong>Enable</Text>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue={enablePin}
                    onChange={(v) => {
                      setEnablePin(v);
                      defaultParams.enable_GPIO = v;
                    }}
                    dropdownMatchSelectWidth={false}
                  >
                    <Option value="24">24</Option>
                    <Option value="25">25</Option>
                    <Option value="26">26</Option>
                    <Option value="27">27</Option>
                    <Option value="None">None</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue="activeHigh"
                    onChange={(v) => (defaultParams.enable_LEVEL = v)}
                  >
                    <Option value="activeHigh">Active high</Option>
                    <Option value="activeLow">Active low</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue="no-pull"
                    dropdownMatchSelectWidth={false}
                    onChange={(v) => (defaultParams.enable_PULL = v)}
                  >
                    <Option value="pull-up">Pull-up</Option>
                    <Option value="pull-down">Pull-down</Option>
                    <Option value="no-pull">No-pull</Option>
                  </Select>
                </Col>
              </Row>

              <Row gutter={[16, 16]}>
                <Col span={6}>
                  <Text strong>Fault</Text>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue={faultPin}
                    onChange={(v) => {
                      setFaultPin(v);
                      defaultParams.fault_GPIO = v;
                    }}
                    dropdownMatchSelectWidth={false}
                  >
                    <Option value="24">24</Option>
                    <Option value="25">25</Option>
                    <Option value="26">26</Option>
                    <Option value="27">27</Option>
                    <Option value="None">None</Option>
                  </Select>
                </Col>
                <Col span={6}>
                  <Select
                    defaultValue="activeHigh"
                    onChange={(v) => (defaultParams.fault_LEVEL = v)}
                  >
                    <Option value="activeHigh">Active high</Option>
                    <Option value="activeLow">Active low</Option>
                  </Select>
                </Col>
                <Col span={6}> </Col>
              </Row>

              <Row gutter={[16, 16]}>
                <Col span={6}> </Col>
                <Col span={18}>
                  <Select
                    defaultValue="1/8"
                    onChange={(v) => (defaultParams.step_LENGTH = v)}
                  >
                    <Option value="1">1</Option>
                    <Option value="1/2">1/2</Option>
                    <Option value="1/4">1/4</Option>
                    <Option value="1/8">1/8</Option>
                    <Option value="1/16">1/16</Option>
                    <Option value="1/32">1/32</Option>
                    <Option value="1/64">1/64</Option>
                    <Option value="1/128">1/128</Option>
                    <Option value="1/256">1/256</Option>
                    <Option value="1/512">1/512</Option>
                  </Select>{" "}
                  Step Length
                </Col>
              </Row>

              <Row gutter={[24, 24]}>
                <Col span={6}> </Col>
                <Col span={9}>
                  <Switch
                    checkedChildren="ON"
                    unCheckedChildren="OFF"
                    defaultChecked
                    onChange={(v) => (defaultParams.active = v)}
                  />{" "}
                  Active
                </Col>
                <Col span={9}>
                  <Form.Item style={{ textAlign: "right" }}>
                    {showInputText()}
                    {showButton()}
                  </Form.Item>
                </Col>
              </Row>
            </Form>
      </ContentPanel>
    </>
  );
};

export default StepDirInterface;
