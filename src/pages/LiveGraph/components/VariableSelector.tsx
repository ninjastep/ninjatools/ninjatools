import React, { useContext } from "react";
import { Row, Select, Space, Col, Button } from "antd";
import { CommunicationContext } from "../../../context/CommunicationContext";
import ContentPanel from "../../../components/ContentPanel";

const { Option } = Select;

const children = [
  // @ts-ignore
  <Option key={"position"}>Cascade position</Option>,
  // @ts-ignore
  <Option key={"speed"}>Speed</Option>,
  // @ts-ignore
  <Option key={"torque"}>Position</Option>,
];

const VariableSelector = () => {
  const {
    setLiveTrack,
    setLiveTrackPos,
    setLiveTrackSpd,
    setLiveTrackTrq,
    sendCommand,
  } = useContext(CommunicationContext);

  const startTracking = () => {
    setLiveTrack(true);
    sendCommand("start_hb_stream 1");
  };

  const stopTracking = () => {
    setLiveTrack(false);
    sendCommand("stop_hb_stream");
  };

  const handleChange = (value) => {
    if (value.includes("position")) {
      console.log("tracking position");
      setLiveTrackPos(true);
    } else {
      console.log("not tracking position");
      setLiveTrackPos(false);
    }
    if (value.includes("speed")) {
      console.log("tracking speed");
      setLiveTrackSpd(true);
    } else {
      console.log("not tracking speed");
      setLiveTrackSpd(false);
    }
    if (value.includes("torque")) {
      console.log("tracking torque");
      setLiveTrackTrq(true);
    } else {
      console.log("not tracking torque");
      setLiveTrackTrq(false);
    }
  };

  return (
    <>
      <ContentPanel title="Select Variables to track">
        <Row gutter={[16, 16]}>
          <Col span={12}>

          
          <Select
            mode="multiple"
            style={{ width: "100%" }}
            placeholder="Select a variable to track"
            defaultValue={[]}
            onChange={handleChange}
          >
            {children}
          </Select>
          </Col>
        </Row>

        <Row gutter={[16, 16]}>
          <Space>
            <Col>
              <Button type="primary" onClick={() => startTracking()}>
                Start Tracking
              </Button>
            </Col>
            <Col>
              <Button danger onClick={() => stopTracking()}>
                Stop Tracking
              </Button>
            </Col>
          </Space>
        </Row>
      </ContentPanel>
    </>
  );
};

export default VariableSelector;
