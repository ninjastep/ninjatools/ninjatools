import React, { useState, useContext, useRef } from "react";
import { useSelector } from "react-redux";
import { Line } from "react-chartjs-2";
import "chartjs-plugin-streaming";
import "chartjs-plugin-zoom";
import { CommunicationContext } from "../../../context/CommunicationContext";
import { Button, Space } from "antd";

const data = {
  datasets: [
    {
      label: "Position",
      borderColor: "#f5222d",
      lineTension: 0,
      fill: false,
      data: [],
    },
  ],
};


const LivePositionChartJS = () => {

  const myRef = useRef(null);

  const {liveTrackPos, liveTrack } = useContext(
    CommunicationContext
  );

  const position = useSelector((state) => state.position);
  const [pause, setPause] = useState(false);
  

  const pauseButtonText = () => {
    if(pause) {
      return "Resume";
    }
    else {
      return "Pause";
    }
  }

  const options = {
    responsive: true,
    tooltips: {
      mode: "point",
      intersect: false,
    },
    hover: {
      mode: "point",
      intersect: false,
    },
    scales: {
      xAxes: [
        {
          type: "realtime",
          realtime: {
            onRefresh: function (chart) {
              chart.data.datasets[0].data.push({ x: Date.now(), y: position });
            },
            duration: 20000,
            refresh: 500,
            delay: 2000,
            pause: pause
          },
        },
      ],
      yAxes: [
        {
          type: 'linear',
          display: true,
          // ticks: {
          //   maxTicksLimit: 5,
          //   precision: 2,
          // }
          // scaleLabel: {
          //   display: true,
          //   labelString: 'value'
          // }
        }
      ]
    },
    pan: {
      enabled: true,
      mode: "xy",
      rangeMin: {
        x: 0,
      },
      rangeMax: {
        x: 4000,
      },
    },
    zoom: {
      enabled: true,
      mode: "xy",
      rangeMax: {
        x: 20000,
      },
      rangeMin: {
        x: 1000,
      },
    },
  };
  if (liveTrackPos && liveTrack) {
    return (
      <div>
      <Line ref={myRef} data={data} options={options} />
        <Space>
        <Button onClick={() => setPause(!pause)}>{pauseButtonText()}</Button>
        <Button onClick={() => (myRef as any).current.chartInstance.resetZoom() }>Reset Zoom</Button>
        </Space>
        
      </div>
    );
  } 
  else {
    return null;
  }
};

export default LivePositionChartJS;
