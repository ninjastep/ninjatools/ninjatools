import React, { useState, useEffect, useContext } from "react";
import { Line } from "@ant-design/charts";
import { useSelector } from "react-redux";
import { CommunicationContext } from "../../../../context/CommunicationContext";

let i = 0;

const LivePositionChartAnt = () => {
  const position = useSelector((state) => state.position);

  const { liveTrack, liveTrackPos } = useContext(
    CommunicationContext
  );

  const [data, setData] = useState([
    { type: "position", tick: i.toString(), value: "0" },
  ]);

  useEffect(() => {

    setData([
      ...data,
      {type:"position", tick: i.toString(), value: liveTrackPos ? position : null },
    ]);

    console.log(data)

    i += 1;
  }, [position]);

  const config = {
    onlyChangeData: liveTrack,
    padding: "auto",
    forceFit: true,
    data,
    xField: "tick",
    yField: "value",
    seriesField: "type",
    color: "#1979C9",
    responsive: true,

    interactions: [
      {
        type: liveTrack ? "" : "slider",
        // cfg: {
        //   start: 0.1,
        //   end: 0.2,
        //   minLimit: 1,
        //   maxLimit: 1,
        // },
      },
    ],
  };

  return (
    <div>
      <Line {...config} />
    </div>
  );
};
export default LivePositionChartAnt;
