import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { LineChart, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer, ReferenceArea, Brush } from "recharts";
import { CommunicationContext } from "../../../../context/CommunicationContext";

const LiveTorqueChartRecharts = () => {
  const [torques, setTorques] = useState([0]);

  const torque = useSelector((state) => state.torque);

  const { liveTrack, liveTrackTrq } = useContext(
    CommunicationContext
  );

  useEffect(() => {
      if(liveTrackTrq){
        setTorques([...torques, torque]);
      }  
  }, [torque]);

  const data = torques.map((t, i) => {
    return {
      name: i,
      torque: t,
    };
  });

  return (
    <ResponsiveContainer width="100%" height={300}>
    
    <LineChart  data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      
      <Legend />
      <Line type="monotone" dataKey="torque" stroke="#cf1322" dot={{r:1}} activeDot={{r:3}} isAnimationActive={false} />
      <Brush dataKey='name' height={30} stroke="#cf1322"/>
      <Tooltip />
      
    </LineChart>
    </ResponsiveContainer>
  );
};

export default LiveTorqueChartRecharts;
