import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { LineChart, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer, ReferenceArea, Brush } from "recharts";
import { CommunicationContext } from "../../../../context/CommunicationContext";

const LiveSpeedChartRecharts = () => {
  const [speeds, setSpeeds] = useState([0]);

  const speed = useSelector((state) => state.speed);

  const { liveTrack, liveTrackSpd } = useContext(
    CommunicationContext
  );

  useEffect(() => {
      if(liveTrackSpd){
        setSpeeds([...speeds, speed]);
      }  
  }, [speed]);

  const data = speeds.map((s, i) => {
    return {
      name: i,
      speed: s,
    };
  });

  return (
    <ResponsiveContainer width="100%" height={300}>
    
    <LineChart  data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      
      <Legend />
      <Line type="monotone" dataKey="speed" stroke="#389e0d" dot={{r:1}} activeDot={{r:3}} isAnimationActive={false} />
      <Brush dataKey='name' height={30} stroke="#389e0d"/>
      <Tooltip />
      
    </LineChart>
    </ResponsiveContainer>
  );
};

export default LiveSpeedChartRecharts;
