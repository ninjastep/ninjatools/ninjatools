import React, { useState, useEffect, useContext } from "react";
import { Line } from "@ant-design/charts";
import { useSelector } from "react-redux";
import { CommunicationContext } from "../../../../context/CommunicationContext";

let i = 0;

const LiveSpeedChartAnt = () => {
  const speed = useSelector((state) => state.speed);

  const { liveTrack, liveTrackSpd } = useContext(
    CommunicationContext
  );

  const [data, setData] = useState([
    {type:"speed", tick: i.toString(), value: "0" },
  ]);

  useEffect(() => {

    setData([
      ...data,
      { type:"speed", tick: i.toString(), value: liveTrackSpd ? speed : null },
    ]);

    console.log(data)

    i += 1;
  }, [speed]); 

  const config = {
    onlyChangeData: liveTrack,
    padding: "auto",
    forceFit: true,
    data,
    xField: "tick",
    yField: "value",
    seriesField: "type",
    color: "#D62A0D",
    responsive: true,

    interactions: [
      {
        type: liveTrack ? "" : "slider",
        // cfg: {
        //   start: 0.1,
        //   end: 0.2,
        //   minLimit: 1,
        //   maxLimit: 1,
        // },
      },
    ],
  };

  return (
    <div>
      <Line {...config} />
    </div>
  );
};
export default LiveSpeedChartAnt;
