import React, { useState, useEffect, useContext } from "react";
import { Line } from "@ant-design/charts";
import { useSelector } from "react-redux";
import { CommunicationContext } from "../../../../context/CommunicationContext";

let i = 0;

const LiveTorqueChartAnt = () => {
  const torque = useSelector((state) => state.torque);

  const { liveTrack, liveTrackPos, liveTrackSpd, liveTrackTrq } = useContext(
    CommunicationContext
  );

  const [data, setData] = useState([
    { type: "torque", tick: i.toString(), value: "0" },
  ]);

  useEffect(() => {

    setData([
      ...data,
      {type:"torque", tick: i.toString(), value: liveTrackTrq ? torque : null }
    ]);

    console.log(data)

    i += 1;
  }, [torque]);

  const config = {
    onlyChangeData: liveTrack,
    padding: "auto",
    forceFit: true,
    data,
    xField: "tick",
    yField: "value",
    seriesField: "type",
    color: "#FAA219",
    responsive: true,

    interactions: [
      {
        type: liveTrack ? "" : "slider",
        // cfg: {
        //   start: 0.1,
        //   end: 0.2,
        //   minLimit: 1,
        //   maxLimit: 1,
        // },
      },
    ],
  };

  return (
    <div>
      <Line {...config} />
    </div>
  );
};
export default LiveTorqueChartAnt;
