import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { LineChart, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer, ReferenceArea, Brush } from "recharts";
import { CommunicationContext } from "../../../../context/CommunicationContext";

const LivePositionChartRecharts = () => {
  const [positions, setPositions] = useState([0]);

  const { liveTrack, liveTrackPos } = useContext(
    CommunicationContext
  );

  const { pidRef } = useContext(CommunicationContext);

  const position = useSelector((state) => state.position);

  useEffect(() => {
      if(liveTrackPos){
        setPositions([...positions, position]);
      } 
  }, [position]);

  const data = positions.map((p, i) => {
    return {
      name: i,
      pos: p,
    };
  });

//   const trackPos = () => {
//       if(liveTrackPos){
//           return(
// data
//           )
//       } else {
//           return null
//       }
//   }

  return (
    <ResponsiveContainer width="100%" height={300}>

    
    <LineChart  data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      
      <Legend />
      <Line type="monotone" dataKey="pos" stroke="#8884d8" dot={{r:1}} activeDot={{r:3}} isAnimationActive={false} />
      <Brush dataKey='name' height={30} stroke="#8884d8"/>
      <Tooltip />
      
    </LineChart>
    </ResponsiveContainer>
  );
};

export default LivePositionChartRecharts;
