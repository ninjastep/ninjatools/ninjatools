import React, { useState, useEffect } from 'react';
import { Line } from '@ant-design/charts';

const LiveGraphLineChart: React.FC<any> = () => {
    
    const [data, setData] = useState([]);
    useEffect(() => {
      asyncFetch();
    }, []);

    const asyncFetch = () => {
      fetch('https://gw.alipayobjects.com/os/antfincdn/qRZUAgaEYC/sales.json')
        .then((response) => response.json())
        .then((json) => {
            setData(json);
            console.log(json);}) //too see data
        .catch((error) => {
          console.log('fetch data failed', error);
        });
    };
    

    const config = {
        forceFit: true,
        padding: 'auto',
        data,
        xField: '城市',
        xAxis: {
          visible: true,
          label: { autoHide: true },
        },
        yField: '销售额',
       // yAxis: { label: { formatter: (v) => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, (s) => `${s},`) } },
        interactions: [
          {
            type: 'slider',
            cfg: {
              start: 0.1,
              end: 0.2,
            },
          },
        ],
      };
   


    return (
        <>
            <Line {...config} />;
        </>
    );
};
export default LiveGraphLineChart;  

/*   

    const [volt, setVolt] = useState([]);
    const [ticks, setTicks] = useState([]);
    let tick = 0;


    useEffect(() => {
        const interval = setInterval(() => {
            const min = 1;
            const max = 10;
            const randVolt = Math.floor(min + Math.random() * (max - min));
            tick++;

            setVolt(currentVolt => [...currentVolt, randVolt]);

            setTicks(currentTicks => [...currentTicks, tick]);

        }, 1000);     //1 Second interval
        return () => clearInterval(interval);
    }, []);
    */
