import React, { useContext } from "react";
import { Input, Button, Form, Col } from "antd";
import { CommunicationContext } from "../../../context/CommunicationContext";
import ContentPanel from "../../../components/ContentPanel";

const SetPos = () => {
  const { sendCommand } = useContext(CommunicationContext);

  const sendToNS = (values) => {
    sendCommand("set_pos " + values.pos);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <ContentPanel title="Set Position">
        <Form
          name="set position"
          initialValues={{ remember: true }}
          onFinish={sendToNS}
          onFinishFailed={onFinishFailed}
        >
          <Col span={12}>
            <Form.Item label="" name="pos">
              <Input />
            </Form.Item>
          </Col>
          <Form.Item>
            <Button htmlType="submit">Set Position</Button>
          </Form.Item>
        </Form>
      </ContentPanel>
    </>
  );
};

export default SetPos;
