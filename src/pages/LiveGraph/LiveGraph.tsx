import React, { useEffect, useContext } from "react";
import { Row, Col } from "antd";
import { CommunicationContext, ConnectionContext } from "../../context/index";
import {
  LivePositionChartJS,
  LiveSpeedChartJS,
  LiveTorqueChartJS,
  SetPos,
  VariableSelector,
} from "./components";
import ContentPanel from "../../components/ContentPanel";
import { useHistory } from "react-router-dom";

const LiveGraph: React.FC<any> = () => {

  const history = useHistory();

  const {
    sendCommand,
  } = useContext(CommunicationContext);

  const { connected, running } = useContext(ConnectionContext)

  useEffect(() => {
    return () => {
      if (connected) {
        sendCommand("stop_hb_stream")
      }
    };
  }, [])

  useEffect(() => {
    if (!connected) {
      history.push("/")
    }
  }, [connected])

  useEffect(() => {
    if (running)
      sendCommand("stop_hb_stream") // For demoMode
  }, [])


  return (
    <>
      <Row gutter={[16, 16]}>
        <Col span={16}>
          <VariableSelector />
        </Col>
        <Col span={8}>
          <SetPos />
        </Col>
      </Row>

      <Row gutter={[16, 16]}>
        <Col span={8}>
          <ContentPanel title="Cascade Position">
            <LivePositionChartJS />
          </ContentPanel>
        </Col>
        <Col span={8}>
          <ContentPanel title="Speed">
            <LiveSpeedChartJS />
          </ContentPanel>
        </Col>
        <Col span={8}>
          <ContentPanel title="Position">
            <LiveTorqueChartJS />
          </ContentPanel>
        </Col>
      </Row>
    </>
  );
};

export default LiveGraph;
