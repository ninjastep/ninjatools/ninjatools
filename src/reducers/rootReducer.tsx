const initialState = {
    position: 0.0,
    speed: 0.0,
    torque: 0.0,
    currentAlpha: 0.0,
    currentBeta: 0.0,
    voltage: 0.0,
    terminal: "",
}

/*
    This could be considered a compliment to the CommunicationContext and is using Redux technology. The reason for this compliment
    is that the structure works better for shared variables that are rapidly updated. Heartbeat, position and terminal are sometimes 
    updated many times per second and I found out that redux works better for variables that are rapidly updated. 
*/


function rootReducer(state = initialState, action){
    const updatedState = {...state}

    const conditionalUpdate = (property, value) => {
        if(!isNaN(value)){
            updatedState[property] = value;
        }
    }

    switch(action.type){
        case 'UPDATE_HEARTBEAT':
            conditionalUpdate("position", parseFloat(action.value[0]));
            conditionalUpdate("speed", parseFloat(action.value[1]));
            conditionalUpdate("torque", parseFloat(action.value[2]));
            conditionalUpdate("currentAlpha", parseFloat(action.value[3]));
            conditionalUpdate("currentBeta", parseFloat(action.value[4]));
            conditionalUpdate("voltage", parseFloat(action.value[5]));
            return updatedState;
        case 'UPDATE_TERMINAL':
            updatedState["terminal"] = action.value
            return updatedState;
        case 'UPDATE_POSITION':
            conditionalUpdate("position", parseFloat(action.value))
            return updatedState;
        case 'UPDATE_SPEED':
            conditionalUpdate("speed", parseFloat(action.value))
            return updatedState
        default:
            return state;
    }
}

export default rootReducer;