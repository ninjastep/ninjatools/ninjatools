export class MockSerialPort {
    buffer = []
    // Different mock responses for different command, can easily be customized
    commandResponse = { 
        "knock_knock": () => '<NINJA>',
        "get_res": () => `<r:0>`,
        "set_res": (arg) => `<r-${arg}>`,
        "get_ind": () => `<L:0>`,
        "set_ind": (arg) => `<L-${arg}>`,
        "get_km": () => `<km:0>`,
        "set_km": (arg) => `<km-${arg}>`,
        "get_n": () => `<n:0>`,
        "set_n": (arg) => `<n-${arg}>`,
        "get_imax": () => `<i:0>`,
        "set_imax": (arg) => `<i-${arg}>`,
        "get_spd_p": () => `<spd_p:0>`,
        "set_spd_p": (arg) => `<spd_p-${arg}>`,
        "get_spd_i": () => `<spd_i:0>`,
        "set_spd_i": (arg) => `<spd_i-${arg}>`,
        "get_spd_d": () => `<spd_d:0>`,
        "set_spd_d": (arg) => `<spd_d-${arg}>`,
        "get_pos_p": () => `<pos_p:0>`,
        "set_pos_p": (arg) => `<pos_p-${arg}>`,
        "get_pos_i": () => `<pos_i:0>`,
        "set_pod_i": (arg) => `<pos_i-${arg}>`,
        "get_pos_d": () => `<pos_d:0>`,
        "set_pos_d": (arg) => `<pos_d-${arg}>`,
        "get_trq_p": () => `<trq_p:0>`,
        "set_trq_p": (arg) => `<trq_p-${arg}>`,
        "get_trq_i": () => `<trq_i:0>`,
        "set_trq_i": (arg) => `<trq_i-${arg}>`,
        "get_trq_d": () => `<trq_d:0>`,
        "set_trq_d": (arg) => `<trq_d-${arg}>`,
        "get_spd_max": () => `<sm:0>`,
        "set_spd_max": (arg) => `<sm-${arg}>`,
        "get_trq_max": () => `<mt:0>`,
        "set_trq_max": (arg) => `<tm-${arg}>`,
        "get_vdq": () => `<vdq:0>`,
        "set_vdq": (arg) => `<vdq-${arg}>`,
        "get_spd": () => `<s:0>`,
        "set_spd": (arg) => `<s-${arg}>`,
        "get_trq": () => `<t:0>`,
        "set_trq": (arg) => `<t-${arg}>`,
        "get_pos": () => `<p:0>`,
        "set_pos": (arg) => `<p-${arg}>`,
        "get_vin": () => `<v:0>`,
        "start_cal": () => `<cal:OK>`,
        "is_cal": () => `<cal:YES>`,
        "check_cal": () => `<ccal:0>`,
        "set_ctrl_mode": (arg) => `<ctrl_mode-${arg}>`,
        "get_ctrl_mode": () => `<ctrl_mode:0>`,
        "get_hb": () => `<hb:0 0 0 0 0>`,
        "start_hb_stream": () => `<hb:OK>`,
        "stop_hb_stream": () => `<hb:STOP>`,
        "move": () => `<p-0>`,
        "stop": () => `<p-0>`,
        "init_mot": () => `<motor:OK>`,
        "start_mot": () => `<motor:START>`,
        "stop_mot": () => `<motor-STOP>`,
        "get_es": () => `<es:0 0 0  0 0>`,
        "start_pos_stream": () => `<p:OK>`,
        "start_spd_stream": () => `<s:OK>`,
        "start_trq_stream": () => `<t:OK>`,
        "start_iab_stream": () => `<iab:OK>`,
        "start_vin_stream": () => `<v:OK>`,
        "stop_pos_stream": () => `<p:STOP>`,
        "stop_spd_stream": () => `<s:STOP>`,
        "stop_trq_stream": () => `<t:STOP>`,
        "stop_iab_stream": () => `<i:STOP>`,
        "stop_vin_stream": () => `<v:STOP>`,
    }

    read = () => this.buffer.shift() // Reads and removes the first item in "buffer"

    // Gets response from commandResponse with command and stores it in "buffer"
    write = (command) => {
        let splitCommand = command.split(' ')
        let args = splitCommand.length > 1 ? splitCommand.splice(1) : []
        if (args)
            this.buffer.push(this.commandResponse[splitCommand[0]](args[0]))
        else 
            this.buffer.push(this.commandResponse[splitCommand[0]]())
            
    }
}