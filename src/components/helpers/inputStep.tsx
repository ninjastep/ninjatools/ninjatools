/*
  Finds next biggest place value 
  returns 10ⁿ.
  Example: if original value is between 10 and 100, step will be 1. 
  If original value is less between 1 and 10, step will be 0.1.
*/

const inputStep = (value) => {
  let dividedValue = value / 10
  if (dividedValue >= 1) 
    return handleInt(dividedValue)
  else 
    return handleDecimal(dividedValue)
}

const handleDecimal = (number) => {
  let referenceValue = String(number)
  for (let i = 0; i < referenceValue.length; i++) {
    if (referenceValue[i] == '0' || referenceValue[i] == '.') {
      continue
    }
    return referenceValue.slice(0, i) + '1'
  }
}

const handleInt = (number) => {
  let newStr = '1'
  for (let i = 1; i < String(Math.floor(number)).length; i++) {
    newStr += '0'
  }
  return newStr
}

export default inputStep
