import React, { useContext, useState } from "react";
import { Button, Space, Select, Tag, Switch } from "antd";
import {
  SyncOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
  UsbOutlined,
  WifiOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { ConnectionContext, CompatabilityContext, MotorContext, SerialPortContext, CommunicationContext } from "../context/index";
import { FirmwareUpdate } from '../pages/Home/components/FirmwareUpdate/index';

const TopBar = () => {
  const {
    createConnection,
    closeConnection,
    connectionType,
    connected,
  } = useContext(ConnectionContext);

  const {
    setDemoMode
  } = useContext(CommunicationContext);

  const {
    baudrateState,
    setBaudrateState,
  } = useContext(SerialPortContext)

  const { running } = useContext(MotorContext)

  const { checkBrowser } = useContext(CompatabilityContext)

  const [lightMode, setLightMode] = useState(0);
  const [colorHue, setColorHue] = useState(0);

  const [modalVisible, setModalVisible] = useState(false);

  const buttonText = () => {
    if (connected) {
      return "Disconnect";
    } else {
      return "Connect";
    }
  };

  const connectionIcon = () => {
    if (connectionType === "serial") {
      return <UsbOutlined />;
    } else {
      return <WifiOutlined />;
    }
  };

  const connectionTag = () => {
    if (connected) {
      if (running) {
        return (
          <Tag icon={<SyncOutlined spin />} color="processing">
            running
          </Tag>
        );
      } else {
        return (
          <Tag icon={<CheckCircleOutlined />} color="success">
            connected
          </Tag>
        );
      }
    } else {
      return (
        <Tag icon={<CloseCircleOutlined />} color="error">
          not connected
        </Tag>
      );
    }
  };

  const handleConnectButtonClick = () => {
    if (connected) {
      closeConnection(); //COMMUNICATION
    } else {
      if((checkBrowser())){
        createConnection(); //COMMUNICATION
      }
    }
  };

  const handleUploadButtonClick = () => setModalVisible(true)

  return (
    <>
      {/* DARKMODE */}
      <div
        style={{
          width: "100vw",
          height: "100vh",
          zIndex: 10000,
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          backdropFilter: `invert(${lightMode}) hue-rotate(${colorHue}deg)`,
          pointerEvents: "none",
          // opacity: lightMode ? 0 : 1,
          // transition: "0.5s",
        }}
      />

      {/*Color Rotations */}
      <div
        style={{
          width: "100%",
          backgroundColor: "#dee1e5",
          height: "68px", //"57px",
          borderBottom: "1px solid #d9d9d9",
          marginBottom: "24px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          paddingLeft: "24px",
        }}
      >
        {" "}
        <Space>
          <Switch
            checkedChildren="Light"
            unCheckedChildren="Dark"
            defaultChecked
            onChange={() => setLightMode((lightMode + 1) % 2)}
          />
          <Tag
            color="#1890ff"
            onClick={() => setColorHue(0)}
            style={{ filter: `hue-rotate(${-colorHue}deg)` }}
          >
            1
          </Tag>
          <Tag
            color="#1890ff"
            onClick={() => setColorHue(90)}
            style={{ filter: `hue-rotate(${-colorHue + 90}deg)` }}
          >
            2
          </Tag>
          <Tag
            color="#1890ff"
            onClick={() => setColorHue(180)}
            style={{ filter: `hue-rotate(${-colorHue + 180}deg)` }}
          >
            3
          </Tag>
          <Tag
            color="#1890ff"
            onClick={() => setColorHue(270)}
            style={{ filter: `hue-rotate(${-colorHue + 270}deg)` }}
          >
            4
          </Tag>
        </Space>
        <Space
          style={{
            marginRight: "12px",
            display: "flex",
            justifyContent: "flex-end",
            flexDirection: "row",
            alignItems: "flex-end",
            paddingBottom: "8px",
          }}
        >
          <Switch
            style={{ marginBottom: "7px" }}
            checkedChildren="Demo"
            unCheckedChildren="Demo"
            onChange={(e) => setDemoMode(e)}
          />
          <div style={{ marginBottom: "7px" }}>{connectionTag()}</div>

          {/*<div
            className="connectionType-wrapper"
            style={{ display: "flex", flexDirection: "column" }}
          >
             <div>Connection</div>
            <Select
              defaultValue={connectionType}
              style={{ width: 120 }}
              onChange={(v) => setConnectionType(v)}
            >
              <Select.Option value="serial">Serial</Select.Option>
              <Select.Option value="wifi">Wifi</Select.Option>
            </Select>
          </div> */}

          <div
            className="Baudrate-wrapper"
            style={{ display: "flex", flexDirection: "column" }}
          >
            <div>Baudrate</div>
            <Select
              defaultValue={baudrateState}
              style={{ width: 120 }}
              onChange={(v) => setBaudrateState(v)}
            >
              <Select.Option value="9600">9600</Select.Option>
              <Select.Option value="19200">19200</Select.Option>
              <Select.Option value="115200">115200</Select.Option>
            </Select>
          </div>
          <div style={{ marginBottom: "1px" }}>
            <Button
              disabled={connected}
              icon={<UploadOutlined />}
              onClick={handleUploadButtonClick}
            >
              Update
            </Button>
          </div>
          <div style={{ marginBottom: "1px" }}>
            <Button
              type="primary"
              icon={connectionIcon()}
              onClick={handleConnectButtonClick}
            >
              {buttonText()}
            </Button>
          </div>
        </Space>
      </div>
      <FirmwareUpdate modalVisible={modalVisible} setModalVisible={setModalVisible} />
    </>
  );
};

export default TopBar;
