import React, { useContext } from "react";
import { Menu, Layout } from "antd";
import { withRouter, useHistory } from "react-router-dom";
import NSLogo from "../assets/NS-logo.png";

import {
  SettingOutlined,
  HomeOutlined,
  ControlOutlined,
  ApiOutlined,
  AreaChartOutlined,
  CodeOutlined,
} from "@ant-design/icons";
import { ConnectionContext } from "../context/index";

const { Sider } = Layout;



const SideBar: React.FunctionComponent<any> = () => {
  const history = useHistory();
  const {
    connected
  } = useContext(ConnectionContext)

  const disableItem = () => {
    if (connected){
      return false;
    } else {
      return true;
    }
  }

  const getSelectedKey = (currentLocation) => {
    return {
      '/': '1',
      '/motor-setup': '2',
      '/controller-setup': '3',
      '/interface-setup': '4',
      '/live-graph': '5',
      '/terminal': '6',
    }[currentLocation]
  }

  return (
    <Sider width={200} className="site-layout-background elevation">
      <Menu
        mode="inline"
        //defaultSelectedKeys={["1"]}
        style={{ height: "100%", borderRight: 0 }}
        selectedKeys={[getSelectedKey(history.location.pathname)]}
      >
        <div className="SidebarLogo">
          <img className="SidebarLogo-image" src={NSLogo} alt="Ninja Step Logo" />
          Ninja Tools
        </div>
        <Menu.Divider />

        <Menu.Item
          icon={<HomeOutlined />}
          key="1"
          disabled ={disableItem()}
          onClick={() => history.push("/")}
        >
          Home
        </Menu.Item>
        <Menu.Item
          icon={<SettingOutlined />}
          key="2"
          disabled ={disableItem()}
          onClick={() => history.push("/motor-setup")}
        >
          Motor Setup
        </Menu.Item>
        <Menu.Item
          icon={<ControlOutlined />}
          key="3"
          disabled ={disableItem()}
          onClick={() => history.push("/controller-setup")}
        >
          Controller Setup
        </Menu.Item>
        <Menu.Item
          icon={<ApiOutlined />}
          key="4"
          disabled ={disableItem()}
          onClick={() => history.push("/interface-setup")}
        >
          Interface Setup
        </Menu.Item>
        <Menu.Item
          icon={<AreaChartOutlined />}
          key="5"
          disabled ={disableItem()}
          onClick={() => history.push("/live-graph")}
        >
          Live Graph
        </Menu.Item>

        <Menu.Item
          icon={<CodeOutlined />}
          key="6"
          disabled ={disableItem()}
          onClick={() => history.push("/terminal")}
        >
          Terminal
        </Menu.Item>
      </Menu>
    </Sider>
  );
};

export default withRouter(SideBar);
