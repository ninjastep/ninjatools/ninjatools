import React, { useContext } from "react";
import { Layout, PageHeader, Tag } from "antd";
import { ConnectionContext, MotorContext } from "../context/index";
import {
  SyncOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
} from "@ant-design/icons";

const { Content } = Layout;

const ContentPanel = ({
  children,
  title = "",
  showConnected = false,
  extra = <React.Fragment />,
}) => {
  const {
    connected,
  } = useContext(ConnectionContext);

  const { running } = useContext(MotorContext)

  return (
    <Content className="panel">
      {title !== "" && (
        <PageHeader
          tags={
            showConnected &&
            (connected ? (
              running ? (
                <Tag icon={<SyncOutlined spin />} color="processing">
                  running
                </Tag>
              ) : (
                <Tag icon={<CheckCircleOutlined />} color="success">
                  connected
                </Tag>
              )
            ) : (
              <Tag icon={<CloseCircleOutlined />} color="error">
                not connected
              </Tag>
            ))
          }
          title={title}
          extra={extra}
        />
      )}
      {children}
    </Content>
  );
};

export default ContentPanel;
