import React, { useEffect } from 'react'
import { Input } from 'antd'
import inputStep from './helpers/inputStep'

const CustomInput = (props) => {
  /*
    state must be passed as an object 
    because the useEffect dynamically updates a fields value
    using the objects key.
  */

  const { form, state, refs, formatter, ...other } = props;

  useEffect(() => {
    let clonedState = {...state} // Formatting didn't work correctly when using original state
    clonedState[Object.keys(clonedState)[0]] = formatter ? formatter( getFirstValue(clonedState) ) : getFirstValue(clonedState);
    form.setFieldsValue(clonedState)
  }, [state])

  const getFirstValue = (obj: Object) => obj[Object.keys(obj)[0]];

  return (
    <Input
      {...other}
      defaultValue={getFirstValue(state)}
      step={inputStep(formatter ? formatter( getFirstValue(state) ) : getFirstValue(state))}
      ref={refs}
    />
  )
}

export default CustomInput
