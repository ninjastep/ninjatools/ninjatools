import React, { useState, useContext } from 'react'
import { Modal } from 'antd'
import {
  CheckCircleTwoTone,
  CloseCircleTwoTone,
  InfoCircleTwoTone,
} from '@ant-design/icons'
import { ConnectionContext, CompatabilityContext } from '../context/index'

const RequirementsModal = () => {
  const { connected } = useContext(ConnectionContext)
  const { checkBrowser, checkOS } = useContext(CompatabilityContext)

  const [modalVisible, setModalVisible] = useState(!(checkBrowser() && !checkOS()) && !connected)
  
  const handleModalClick = () => {
    setModalVisible(false)
  }

  return (
    <Modal
      title="Requirements for Ninja Tools"
      visible={modalVisible}
      onOk={() => handleModalClick()}
      onCancel={() => handleModalClick()}
    >
      <div>
        Please use latest versions of Chrome of Edge as your browser:
        <br />
        {checkBrowser() ? (
          <div>
            <CheckCircleTwoTone twoToneColor="#52c41a" /> Done!{' '}
          </div>
        ) : (
          <div>
            {' '}
            <CloseCircleTwoTone twoToneColor="#f5222d" /> Download latest
            version of Chrome or Edge
          </div>
        )}
        {checkOS() && (
          <div>
            <br />
            <InfoCircleTwoTone color="#62B1FF" /> Linux devices may have to
            change permissions to usb-device with: {<br />} sudo chmod 0777
            /dev/
            {'[port]'}{' '}
          </div>
        )}
      </div>
    </Modal>
  )
}

export default RequirementsModal
